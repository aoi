#!/bin/sh

valgrind \
  --leak-check=yes \
  --gen-suppressions=yes \
  --suppressions=valgrind.suppressions \
./aoi
