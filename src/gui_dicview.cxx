/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstdio>
#include <cmath>
#include <FL/Fl_Menu_Item.H>
#include "gui_dicview.hxx"
#include "utils.hxx"
#include "logger.hxx"
#include <stdexcept>

using utils::split_string;
using utils::TextTag;

namespace aoi_ui {

DicView::DicView ( int x, int y, int w, int h, const char *l ): Fl_Table_Row(x,y,w,h,l)
{
  when( FL_WHEN_RELEASE|FL_WHEN_CHANGED );
  table_box( FL_NO_BOX );
  type(SELECT_SINGLE);
  cb_leftclick(   scb_general, nullptr );
  cb_rightclick(  scb_general, nullptr );
  cb_doubleclick( scb_general, nullptr );
  cb_select(      scb_general, nullptr );
  register_tag("default", TextStyle());;
  register_tag("sep",     TextStyle(TextStyle::SEPARATOR_SQUARE) ); 
  register_tag("br",      TextStyle(TextStyle::NEWLINE_AFTER));
  cols(3);
  headers_ = {"", "", ""};
  col_header(1);
  col_resize(1);
  col_resize_min(4);
}


void DicView::set_data ( const vector<string> &d, const vector<int> &cell_ids )
{
  // prevents division by zero
  if ( cols() == 0 )
    cols(1);
  // clear view
  data_.clear();
  ids_.clear();
  redraw();
  // set new data
  int c = d.size()/cols();
  rows( (c>1) ? c:1 );
  cell_heights_.clear();
  cell_heights_.resize(d.size());
  data_ = d;
  ids_ = cell_ids;
  if ( cell_ids.empty() )
    ids_ = vector<int>(data_.size(),-1);
  // recalc cols and rows and redraw 
  draw_cell( CONTEXT_RC_RESIZE, 0, 0, 0, 0 );
  redraw();
}


int DicView::draw_single_cell ( int R, int C, int X, int Y, int W, int H )
{
  int cell = R*cols()+C;

  if ( cell >= int(data_.size()) ){
		fl_push_clip(X, Y, W, H);
		{
      // background
		  fl_color( Fl::get_color( FL_BACKGROUND2_COLOR ) );
			fl_rectf(X, Y, W, H);
      // border
      fl_color(FL_LIGHT2);
      fl_rect(X, Y, W, H);
    }
		fl_pop_clip();
    return -1; 
  }

  int fy = 0;

	fl_push_clip(X, Y, W, H);
	{
	  // BG COLOR
	  fl_color( row_selected(R) ? FL_LIGHT2:FL_BACKGROUND2_COLOR );
		fl_rectf(X, Y, W, H);
  
		// TEXT
    // parse data_[cell]
    auto p = utils::parse_markup(data_.at(cell).c_str());
    string raw_string = p.first;
    auto tags = p.second; 
    std::sort( tags.begin(), tags.end() );
    std::multimap<int,TextTag> tmap;

    // fill gaps in tag ranges with default tag
    for ( size_t i=0; i<tags.size(); i++ ) {
      int prev_end = (i>0) ? tags[i-1].pos + tags[i-1].len:0;
      if ( prev_end < tags[i].pos )
        tmap.insert( { prev_end, {"default", prev_end, tags[i].pos-prev_end, 
          static_cast<int>(tmap.size()+1)} } );
      tmap.insert( {tags[i].pos, tags[i]} );
    }


    // apply default tag on whole string (if no other tags were defined)
    int raw_len = strlen(raw_string.c_str());
    if ( tmap.empty() )
      tmap.insert( { 0,{ "default", 0, raw_len, -1 } } );
    // create last default tag if neccessary
    else {
      int pos = tags.back().pos + tags.back().len;
      if ( pos < raw_len )
        tmap.insert( { pos,{ "default", pos, raw_len-pos, 
          static_cast<int>(tmap.size()+1) }} );
    }

    for ( auto mi = tmap.begin(); mi != tmap.end(); ++mi ){
      auto t = mi->second;
      // remove unneded default
      if ( t.tag == "default" ){
        auto bounds = tmap.equal_range( mi->second.pos );
        for( auto j=bounds.first; j!=bounds.second; j++ ){
          // XXX: ? nemuze se stat, ze bude 2x stejny default
          if ( j->second.pos == t.pos && j->second.len == t.len 
                && j->second.tag  != "default" )
            tmap.erase( mi );
        }
      }
    }

    tags.clear();
    for ( auto t: tmap )
      tags.push_back( t.second );

    // detect the biggest font on the first line
    // for the first line is better use font size instead of fl_height
    // because the bigger font is, the bigger fl_height which leads to too
    // big gaps 
    // (e.g. size=14 : fl_height=16, size=50 : fl_height=61)
    int total_w = 0;
    auto mi = tmap.begin();
    int fh = 0;
    while ( total_w < W ){
      if ( mi == tmap.end() )
        break;
      TextStyle *style = get_style(mi->second.tag);
      fl_font( style->font, int(font_size_*style->relsize) );
      int ffh = style->relsize*font_size_;
      if ( ffh > fh )
        fh = ffh;
      total_w += fl_width("m")*mi->second.len;
      mi++;
    }

    // set values needed for calculations
    int fx = X + cell_padding_x();
    fy = Y + cell_padding_y() + fh;

    for ( auto t: tags ){
      TextStyle *style = get_style(t.tag); 
      fl_font( style->font, int(font_size_*style->relsize) );
	    fl_color( style->color );
      fh = fl_height();
      int space_width = fl_width(" ");
      int fy_correction = 0;  // interline spacing when using subscript
    
      // separator
      // inserts: [SPACE]SQUARE[SPACE]
      if ( style->separator != TextStyle::SEPARATOR_NONE ){
        Fl_Color prev_color = fl_color();
        fl_color( style->color );
        int size = ceil(fh*0.4);
        fl_rectf( fx + space_width, fy-size, size, size );
        fx += size + 2*space_width;
        fl_color( prev_color );
        continue;
      };
      
      // interline space when <sub>
      if ( style->offset_y > fy_correction )
        fy_correction = style->offset_y;
    

      // draw words
      for ( auto &ss: split_string( raw_string.substr(t.pos,t.len), " " ) ) {
        int swidth = fl_width(ss.c_str());
        // break line
        if ( fx+swidth-X >  W-2*cell_padding_x() ){
          fx = X + cell_padding_x();
          fy += fh + fy_correction;
          fy_correction = 0;
        }
	      fl_draw( ss.c_str(), fx, fy + style->offset_y );
        fx += swidth + space_width;
      }
    
      if ( style->newline == TextStyle::NEWLINE_AFTER ){
        fx = X + cell_padding_x();
        fy += fh + 2*fl_descent() + fy_correction;
        fy_correction = 0;
      }
    } // END: for tag in tags
    // END: TEXT

    fy += cell_padding_y_; // empty space
   
	  // draw a line between rows (draw line above current cell)
    if ( R > 0 ){
	    fl_color( Fl::get_color( FL_FOREGROUND_COLOR ) ); 
      fl_line(X,Y,X+col_width(C),Y);
    }

  }
	fl_pop_clip();

  int this_height = fy-Y+cell_padding_y();
  cell_heights_[R*C+C] = this_height;
  
  if ( C == cols()-1 ){
    int max_height = 0;
    for ( int c=0; c<=C; c++ ){
      int ch = cell_heights_[R*c+c];
      if ( ch > max_height )
        max_height = ch;
    }
    if ( max_height != H ){
      redraw_row(R,max_height);
    }
  }

  return this_height;
}


void DicView::redraw_row ( int R, int H )
{
  row_height(R,H);
  for ( int c=0; c<cols(); c++ ){
    int X = 0;
    int Y = 0;
    int W = 0;
    int H = 0;
    find_cell( CONTEXT_CELL, R, c, X, Y, W, H);
    draw_single_cell(R,c,X,Y,W,H);
  }
}



void DicView::draw_cell ( TableContext context, int R, int C, int X, int Y, int W, int H )
{
  switch ( context )
  {
	  case CONTEXT_STARTPAGE:
	    fl_font( FL_HELVETICA, font_size());
	    return;

	  case CONTEXT_COL_HEADER:
	    fl_push_clip(X, Y, W, H);
	    {
		    fl_draw_box(FL_THIN_UP_BOX, X, Y, W, H, col_header_color());
	      fl_font( FL_HELVETICA, 0.7*col_header_height() );
        fl_color( FL_BLACK );
        if ( !headers_.at(C).empty() )  
          fl_draw( headers_.at(C).c_str(), X+5, Y+fl_height()  );
	    }
	    fl_pop_clip();
	    return;


	  case CONTEXT_CELL:
    {
      draw_single_cell(R,C,X,Y,W,H);
      return;
    }


		case CONTEXT_RC_RESIZE:
    {
      // adjust last column (sense) width
      int sum = 0;
      for ( int c=0; c<cols()-1; c++ )
        sum+=col_width(c);
      int scroll = 1.2*vscrollbar->w();
      if ( !vscrollbar->visible() )
        scroll = 5;
      col_width(cols()-1, w() - sum - scroll );
      return;
    }

	  case CONTEXT_ROW_HEADER:
		case CONTEXT_ENDPAGE:
		case CONTEXT_TABLE:
		case CONTEXT_NONE:
    default:
      return;
  }
}

int DicView::handle ( int event )
{
  switch ( event ) {
    case FL_FOCUS:
    {
      if ( selected_row() == -1 )
        select_row(0);
      return 1;
    }
    case FL_PUSH:
    {
      int row = row_at_y(Fl::event_y()-y());
      take_focus();
      select_row(row);
      // Fl::belowmouse() ensures that scrollbar will be still usable 
      if ( Fl::event_button() == FL_LEFT_MOUSE && Fl::belowmouse()==this ){
        if ( Fl::event_clicks() > 1 )
          cb_doubleclick(this);
        else
          cb_leftclick(this);
      }
      else if ( Fl::event_button() == FL_RIGHT_MOUSE ){
        cb_rightclick(this);
      }
    }
    case FL_RELEASE:
    {
      if ( Fl::event_button() == FL_RIGHT_MOUSE )
        return 1;
    }
    case FL_KEYDOWN:
    {
      int row = selected_row();
      int r1 = 0;
      int r2 = 0;
      int c1 = 0;
      int c2 = 0;
      visible_cells(r1,r2,c1,c2);
      if ( row >= r2 )
        top_row(r1+1);  
      else if ( row <= r1 && r1 > 0 )
        top_row(r1-1);
  
      switch ( Fl::event_key() ){
        case FL_Down:
        {
          if ( row < rows() )
            select_row(row+1);
          return 1;
        }
        case FL_Up:
        {
          if ( row > 0 )
            select_row(row-1);
          return 1;
        }
        // Page Up/Down are handled by the vertical scrollbar
        case ' ':
        case FL_Enter:
          cb_rightclick(this);
          return 1;
      };
    }
  }
  return Fl_Table_Row::handle(event);
}


} // namespace
