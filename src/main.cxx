/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "aoi.hxx"
#include "utils.hxx"
#include "parsers.hxx"
#include "sqlite3.hxx"
#include "config.hxx"

#include <exception>
#include <string>

using aoi::ElementKanji;
using aoi::ElementReading;
using aoi::ElementSense;
using aoi::DicWord;
using aoi::App;
using std::string;
using utils::to_string;

const char *TEMP_JMDICT   = "gztemp.jmdict";
const char *TEMP_KANJIDIC = "gztemp.kanjidic";

const char *DBSCRIPT_LICENSE = 
"--This file is based on the JMDICT and KANJIDIC dictionary files. These files \
are the property of the Electronic Dictionary Research and Development \
Group, and are used in conformance with the Group's licence.\
--http://www.csse.monash.edu.au/~jwb/jmdict.html\n\
--http://www.csse.monash.edu.au/~jwb/kanjidic2\n\
--http://www.edrdg.org/edrdg/licence.html\n\
--Decompositions of the kanji to radicals are taken from the KRADFILE-U \
- Copyright is held by Jim Rose. (http://www.kanjicafe.com/kradfile_license.htm)\n\
--The SKIP (System of Kanji Indexing by Patterns) system for ordering kanji \
used in this program was developed by Jack Halpern (Kanji Dictionary \
Publishing Society at http://www.kanji.org/, and is used with his \
permission.\n\
--See http://aoi.souko.cz for more details.\n";

/*!
* Parses JMDict. File may be GZipped.
* \see parsers::JmdictParser
* \exception utils::ParsingError
* \param fname path to JMDict file
*/
void parse_jmdict ( const char *fname )
{
  printf("Loading JMdict file '%s'.\n", fname);
  std::stringstream ss;
  ss << DBSCRIPT_LICENSE;
  ss << "BEGIN TRANSACTION;\n";

  // create parser
  try {
    printf("Decompressing file: %s -> %s\n", fname, TEMP_JMDICT);
    utils::gzip_decompress_file( fname, TEMP_JMDICT);
    parsers::JmdictParser jmp(TEMP_JMDICT);
    const char *SEP = aoi::SEPARATOR_SQL;

    // tables
    for ( auto &mi: aoi_config::db_tables.at("main") ){
      if ( strncmp( mi.first, "d_",2 ) !=0 && strcmp( mi.first, "aoi" )!=0 )
        continue;
      ss << "DROP TABLE IF EXISTS " << mi.first << ";\n"
      << "CREATE TABLE " << mi.first << " ( "; 
      for ( size_t i=0; i<mi.second.size(); i++ )
        ss << mi.second[i].name << " " << mi.second[i].type 
          << ((i==mi.second.size()-1) ? "":",");
      ss << ");\n";
    }

    // version
    ss << "INSERT INTO aoi (key,val) VALUES ('jmdict_version', '" 
      << jmp.get_version() << "');\n";
    printf("jmdict_version: %s\n", jmp.get_version().c_str());
    
    // get entities
    for ( std::pair<string,string> elt: jmp.get_entities() ){
      ss << "INSERT INTO d_entities (abbr,desc) VALUES ('" << SQLite3::escape(elt.first) 
        << "','" << SQLite3::escape(elt.second) << "');\n";
    } 
    
    int n_entries = 1;
    DicWord entry = jmp.get_entry();
    while ( entry.did() != -1 ) {
      for ( ElementReading &rele: entry.r_ele() )
        ss << rele.sql(entry.did(),SEP);
      for ( ElementKanji &kele: entry.k_ele() )
        ss << kele.sql(entry.did(),SEP);
      for ( ElementSense &sele: entry.s_ele() )
        ss << sele.sql(entry.did(),SEP);
      // tbl: header
      n_entries++;
      entry = jmp.get_entry();
    } // while entry
    printf("%d entries processed.\n", n_entries);
  }   
  catch ( utils::ParsingError &e ){
    std::string msg = "App::parse_jmdict(): ParsingError: ";
    msg += e.what();
    printf("parse_jmdict(): ParsingError: %s\n", e.what() );
    return;
  }

  ss << "END TRANSACTION;\n";
  printf("Writing file 'script.jmdict.sql'...\n");
  std::ofstream f ("script.jmdict.sql");
  f << ss.str();
  f.close();

  remove(TEMP_JMDICT);
}


/*!
* Parses kanjidic2. Works in the same way as parse_jmdict().
* \see parsers::KanjidicParser
*/
void parse_kanjidic ( const char *fname )
{
  printf("Loading kanjidic file: %s\n", fname);
  const char *SEP = aoi::SEPARATOR_SQL;

  int n_kanji = 0;
  try {
    printf("Decompressing file: %s -> %s\n", fname, TEMP_KANJIDIC);
    utils::gzip_decompress_file( fname, TEMP_KANJIDIC);
    parsers::KanjidicParser p(TEMP_KANJIDIC);
    auto kanji = p.get_entry();

    std::stringstream ss;
    ss << "BEGIN TRANSACTION;\n";

    // tables
    for ( auto &mi: aoi_config::db_tables.at("main") ){
      if ( strncmp( mi.first, "k_",2 ) !=0 )
        continue;
      ss << "DROP TABLE IF EXISTS " << mi.first << ";\n"
      << "CREATE TABLE " << mi.first << " ( "; 
      for ( size_t i=0; i<mi.second.size(); i++ )
        ss << mi.second[i].name << " " << mi.second[i].type 
          << ((i==mi.second.size()-1) ? "":",");
      ss << ");\n";
    }

    // version
    ss << "REPLACE INTO aoi (key,val) VALUES ('kanjidic_version','" 
      << p.get_version() << "');\n";

    while ( kanji.kanji() != "" ){
      n_kanji++;
      ss << "INSERT INTO k_kanji "
        << "(kanji,ucs,onyomi,kunyomi,meaning,nanori,flags,jlpt,grade,freq,strokes,"
        << "rad_classic,rad_nelson,components)"
        << " VALUES('"
        << kanji.kanji() << "','"
        << kanji.ucs() << "','"
        << to_string(kanji.onyomi(),SEP) << "','"
        << to_string(kanji.kunyomi(),SEP) << "','"
        << SQLite3::escape(to_string(kanji.meaning(),SEP)) << "','"
        << to_string(kanji.nanori(),SEP) << "','"
        << to_string(kanji.flags(),SEP) << "',"
        << kanji.jlpt() << ","
        << kanji.grade() << ","
        << kanji.freq() << ","
        << kanji.strokes() << ","
        << kanji.rad_classic() << ","
        << kanji.rad_nelson() << ","
        << "''" 
        << ");\n";
      for ( aoi::SKIP &s: kanji.skip() ) {
        ss << "INSERT INTO k_skip (kanji,skip1,skip2,skip3,misclass) VALUES('"
          << kanji.kanji() << "'," << s.s1 << "," << s.s2 << "," << s.s3 
          << ",'" << s.misclass << "');\n";
      }
      kanji = p.get_entry();
    }
    ss << "END TRANSACTION\n;";

    printf("Writing file 'script.kanjidic.sql'...\n");
    std::ofstream f ("script.kanjidic.sql");
    f << ss.str();
    f.close();
  }
  catch ( utils::ParsingError &e ){ 
    printf("parse_kanjidic(): ParsingError: %s\n", e.what());
    return;
  } 
  remove( TEMP_KANJIDIC );
}



void usage ()
{
  printf("USAGE: aoi [OPTIONS]\n");
  printf("OPTIONS:\n");
  printf("  -geometry W*H+X+Y         \n");
  printf("  -scheme none|GTK+|plastic \n");
  printf("  -config                   config string\n");
  printf("  -parse jmdict|kanjidic    parse either jmdict or kanjidic\n");
}


//////////////////////////////////////////////////////////////////////////


int main ( int argc, char **argv )
{
  int ret = 0;
  int fltk_argc = 1;
  char **fltk_argv = &argv[0];

  // Commandline options
  try {
    for ( int i=1; i < argc; i++ ){
      ///////////////////////////////////////////////////////
      // CONFIG
      if ( !strcmp( argv[i], "-config") ){
        string opts = argv[++i];
        if ( opts == "help" ){
          for ( auto mi: App::get()->get_config_map() )
            printf("%s [%s]\n   %s\n\n", 
              mi.first.c_str(), mi.second.val.c_str(), mi.second.desc.c_str() ); 
          continue;
        }
        for ( string &s: utils::split_string( opts, ":" ) ){
          vector<string> kv = utils::split_string(s, "=");
          App::get()->log("Config override: " + kv[0] + "=" + kv[1]);
          try {
            App::get()->config_override( kv[0], kv[1] ); }
          catch ( std::runtime_error &e ) {
            App::get()->log_w("Unknown option: " + kv[0] ); }
        }
        App::get()->apply_config();
      } // -config
      ///////////////////////////////////////////////////////
      // Parse source files
      else if ( !strcmp( argv[i], "-parse" ) ) {
        if ( i == argc-1 ){
          printf("-parse: Missing parameter: either kanjidic or jmdict\n");
          return 0;
        }
        i++;
        if ( !strcmp( argv[i], "jmdict") ){
          if ( utils::file_exists("JMdict_e.gz") )
            parse_jmdict( "JMdict_e.gz" );
          else if ( utils::file_exists("JMdict_e") )
            parse_jmdict( "JMdict_e" );
          else {
            printf("File not found: JMdict_e or JMdict_e.gz\n");
            return 0;
          }
        }
        else if ( !strcmp( argv[i], "kanjidic") ){
          if ( utils::file_exists("kanjidic2.xml.gz") )
            parse_kanjidic( "kanjidic2.xml.gz" );
          else if ( utils::file_exists("kanjidic2.xml") )
            parse_kanjidic( "kanjidic2.xml" );
          else {
            printf("File not found: kanjidic2.xml or kanjidic2.xml.gz\n");
            return 0;
          }
        }
        else {
          printf("-parse: wrong parameter '%s'.\n", argv[i] );
          printf("Possible parameters: kanjidic or jmdict\n");
        }
        return 1;
      }
      ///////////////////////////////////////////////////////
      // load database script
      else if ( !strncmp( argv[i], "-loaddb", 7) ){
        if ( i==argc-1 ) {
          printf("Missing file: -loaddb FILE\n");
          return 0;
        }
        if ( !utils::file_exists(argv[i+1]) ){
          printf("File does not exist: %s\n", argv[i+1]);
          return 0;
        }
        App::get()->load_dbscript( argv[i+1] );
        return 1;
      }
      ///////////////////////////////////////////////////////
      // argv[i] not recognized 
      else {
        printf("argv[%d] : %s -> FLTK\n", i, argv[i]);
        // pass unparsed arguments to FLTK
        fltk_argv[fltk_argc++] = argv[i];
      }
    }
    ret = App::get()->run(fltk_argc,fltk_argv);
  }
  ///////////////////////////////////////////////////////
  // EXCEPTIONS
  catch ( std::exception &e ){
    // This should never happen ...
    string msg = std::string("Something went wrong...\n") 
                + std::string(typeid(e).name()) 
                + std::string(": ") + std::string(e.what());
    App::get()->alert(msg);
  }

  delete App::get();
  return ret;
}
