/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "utils.hxx"
#include <cstring>
#include <sstream>
#include <cstdio>
#include <FL/fl_utf8.h>
#include <stdexcept>
#include "logger.hxx"

namespace utils {

void check_zlib_error ( int err )
{
  std::string msg;
  switch (err){
    case Z_STREAM_ERROR:
      msg = "File is not valid.";
      break;
    case Z_ERRNO:
      msg = "File operation error.";
      break;
    case Z_MEM_ERROR:
      msg = "Out of memory.";
      break;
    case Z_BUF_ERROR:
      msg = "LAst read ended in the middle of gzip stream.";
    case Z_OK:
      return;
    default:
      msg = "This should not happen (gzclose() returned" + std::to_string(err) + ")";
  }
  throw std::runtime_error(msg.c_str());
}


void gzip_decompress_file( const char *infilename, const char *outfilename )
{
  gzFile infile = gzopen(infilename, "rb");
  FILE *outfile = fopen(outfilename, "wb");

  if (!infile) 
    throw std::runtime_error("Can't open file");
  if (!outfile) 
    throw std::runtime_error("Can't write file");

  char buffer[128];
  int num_read = 0;
  while ((num_read = gzread(infile, buffer, sizeof(buffer))) > 0)
    fwrite(buffer, 1, num_read, outfile);

  int ret = gzclose(infile);
  fclose(outfile);

  check_zlib_error( ret );
}


std::pair<string,vector<TextTag>> parse_markup ( const char *s )
{
  size_t i    = 0;
  int raw_pos = 0;
  int len     = 0;
  int n       = 0;
  char name[MAX_TAGNAME_LENGTH];
  vector<TextTag> tags;
  std::stringstream raw;

  size_t slen = strlen(s);
  
  while ( i < slen && s[i] != '\0' ){
    // tag starts
    if ( s[i] == '<' ){
      // << (i.e. escaped < )
      if ( s[i+1] == '<' ){
        i++;
        continue;
      }
      // </tag>
      if ( s[i+1] == '/' )
        tags.push_back( { name, raw_pos-len, len, n++ } );

      size_t j = 0;
      while ( s[i++] != '>' ){
        name[j++] = s[i];
      }
      // <tag/>
      if ( s[i-2] == '/' ) {
        name[j-2] = '\0';
        tags.push_back( { name, raw_pos, 0, n++ } );
      }
      name[j-1] = '\0';
      len = 0;
      if ( i < slen && s[i+1] != '<' )
        continue;
    } // closing '>' reached
    raw_pos++;
    len++;
    raw << s[i++];
  } // while ( s[i] != '/0' )
  return { raw.str(), tags };
}


vector<string> split_string ( const string &s, const char *delimiters )
{
  vector<string> v;
  char tmp[strlen(s.c_str())+1];
  strcpy( tmp, s.c_str() );
  char *ss = strtok( tmp, delimiters ); // strtok destroys tmp
  while ( ss != nullptr ){
    v.push_back( string(ss) );
    ss = strtok( nullptr, delimiters );
  }
  return v;
}


vector<int> split_string_int ( const string &s, const char *delimiters )
{
  vector<int> v;
  char tmp[strlen(s.c_str())+1];
  strcpy( tmp, s.c_str() );
  char *ss = strtok( tmp, delimiters ); // strtok destroys tmp
  while ( ss != nullptr ){
    v.push_back( atoi(ss) );
    ss = strtok( nullptr, delimiters );
  }
  return v;
}


vector<string> str_to_chars ( const char *s )
{
  vector<string> v;
  while ( *s != '\0' ){
    int len = fl_utf8len(*s);
    std::stringstream ss;
    if (len!=-1){
      for ( int i=0; i<len; i++ ) ss << *s++;
      v.push_back(ss.str());
    }
  }
  return v;
}


vector<vector<string>> separate_groups ( vector<string> &v )
{
  std::sort( v.begin(), v.end() );
  vector<vector<string>> groups;
  vector<string> group;
  for ( size_t i=0; i < v.size(); ++i ){
    // similar names
    if ( i > 0 ){
      const char *prev = v[i-1].c_str();  
      const char *curr = v[i].c_str();
      size_t pos = find_first_digit( curr );
      if ( !strncmp( prev, curr, pos ) )
        group.push_back( v[i] );
    }
    else{
      if ( !group.empty() )
        groups.push_back( group );
      group = { v[i] };
    }
  }
  // last element
  groups.push_back( group );
  return groups;
}
} // namespace utils
