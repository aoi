/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstdio>
#include <cmath>
#include <FL/Fl_Menu_Item.H>
#include "gui_kanjiview.hxx"
#include "utils.hxx"
#include "logger.hxx"
#include <stdexcept>

using utils::split_string;

namespace aoi_ui {

KanjiView::KanjiView ( int x, int y, int w, int h, const char *l ): Fl_Table(x,y,w,h,l)
{
  when( FL_WHEN_RELEASE|FL_WHEN_CHANGED );
  table_box( FL_NO_BOX );
  cb_leftclick(   scb_general, nullptr );
  cb_rightclick(  scb_general, nullptr );
  cb_select(      scb_general, nullptr );
  cols(1);
  end();
}


int KanjiView::handle ( int event )
{
  switch ( event ) {
    case FL_PUSH:
    {
      take_focus();
      if ( Fl::event_button() == FL_LEFT_MOUSE && Fl::belowmouse() == this ){
        Cell *c = cell_at_xy(Fl::event_x()-x(), Fl::event_y()-y());
        if ( c ){
          if ( c->selected )
            c->selected = false;
          else {
            select_cell(c);
            cb_leftclick(this);
          }
          redraw();
          return 1;
        }
      }
      else if ( Fl::event_button() == FL_RIGHT_MOUSE ) {
        Cell *c = cell_at_xy(Fl::event_x()-x(), Fl::event_y()-y());
        Fl_Menu_Item menu[] = {
          { "Copy",   0, scb_copy,  (void*)c->str.c_str() },
          // TODO: copy all kanjidata
          { 0 }
        };
        const Fl_Menu_Item *m = menu->popup(Fl::event_x(), Fl::event_y(), 0, 0, 0);
        if ( m ) 
          m->do_callback(0, m->user_data());
        return 1;
      }
    }
    case FL_RELEASE:
      if ( Fl::event_button() == FL_RIGHT_MOUSE )
        return 1;
    case FL_KEYUP:
    {
      int cell = selected_id();
      switch ( Fl::event_key() ){
        case FL_Left:
        {
          if ( cell > 0 )
            select_cell(cell-1);
          return 1;
        }
        case FL_Right:
        {
          size_t id = cell+1;
          if ( id < data_.size() )
            select_cell(id);
          return 1;
        }
        case FL_Down:
        {
          size_t id = cell+cols();
          if ( id < data_.size() )
            select_cell(id);
          return 1;
        }
        case FL_Up:
        {
          int id = cell-cols();
          if ( id >= 0 )
            select_cell(id);
          return 1;
        }
        case FL_Enter:
        case ' ':
        {
          cb_select(this);
          return 1;
        }
      }
    }
    default:
      return Fl_Table::handle(event);
  }
}

void KanjiView::font_size ( int s )
{
  font_size_ = s;
  fl_font( font_, font_size_ );
  int x = fl_height() + 2*cell_padding_;
  cell_size(x);
}

void KanjiView::set_data ( const vector<Cell> &d )
{
  // prevents division by zero
  if ( cols() == 0 )
    cols(1);
  // clear view
  data_.clear();
  // set new data
  data_ = d;
  int ncols = d.size()/cols();
  rows( (ncols>1) ? ncols:1 );
  row_height_all( cell_size_ );
  col_width_all( cell_size_ );
  draw_cell( CONTEXT_RC_RESIZE, 0, 0, 0, 0 );
  redraw();
}


void KanjiView::draw_single_cell ( int R, int C, int X, int Y, int W, int H )
{
  int cell = R*cols()+C;

  // empty cells at the end of the last row
  if ( cell >= int(data_.size()) ){
		fl_push_clip(X, Y, W, H);
		{
      // background
		  fl_color( Fl::get_color( FL_BACKGROUND2_COLOR ) );
			fl_rectf(X, Y, W, H);
      // border
      fl_color(FL_LIGHT2);
      fl_rect(X, Y, W, H);
    }
		fl_pop_clip();
    return; 
  }

  Cell c = data_.at(cell);
	fl_push_clip(X, Y, W, H);
	{
	  // BG COLOR
	  fl_color(c.selected ? selection_color_:c.bgcolor );
		fl_rectf(X, Y, W, H);
  
		// TEXT
    fl_font( font_, font_size_ );
	  fl_color( c.fgcolor );
	  fl_draw( c.str.c_str(), X+cell_padding_, Y+font_size_ );
   
    // BORDER
    fl_color(FL_LIGHT2);
    fl_rect(X, Y, W, H);
  }
	fl_pop_clip();
}


void KanjiView::draw_cell ( TableContext context, int R, int C, int X, int Y, int W, int H )
{
  switch ( context )
  {
	  case CONTEXT_STARTPAGE:
	    fl_font( FL_HELVETICA, font_size());
	    return;

	  case CONTEXT_COL_HEADER:
	    fl_push_clip(X, Y, W, H);
	    {
		    fl_draw_box(FL_THIN_UP_BOX, X, Y, W, H, col_header_color());
	    }
	    fl_pop_clip();
	    return;


	  case CONTEXT_CELL:
    {
      draw_single_cell(R,C,X,Y,W,H);
      return;
    }


		case CONTEXT_RC_RESIZE:
    {
      // append column
      int n_cols = (w()-vscrollbar->w())/cell_size_;
      int n_rows = ceil((float)data_.size()/n_cols);
      if ( n_cols != cols() )
        cols( n_cols );  
      if ( n_rows != rows() ){
        rows(n_rows);
        row_height_all(cell_size_);
      }
      return;
    }

    default:
      return;
  }
}

} // namespace
