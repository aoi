/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CONFIG_HXX
#define _CONFIG_HXX

#include <string>
#include <sstream>
#include <map>
#include <stdexcept>
#include <vector>
#include <cstring>
#include <cstdio>

using std::string;
using std::vector;

namespace aoi_config {


/*!
* One column in database.
* \sa db_tables_main
* \sa db_tables_user
*/
struct DBTableColumn
{
  const char *name;
  const char *type;
  bool index;
  const char *sort;
};

/*!
* List of the tables in the database.
* \sa App::check_tables()
* \sa App::check_indexes()
*/
static const std::map<const char*, std::map<const char*,vector<DBTableColumn>>> db_tables =
{
  // main database
  { "main", {
    { "aoi", {
        { "key",      "TEXT",   false , "ASC" },
        { "val",      "TEXT",   false,  "ASC" }
      }
    },
    { "d_entities", { 
        { "abbr",     "TEXT",   false,  "ASC" },
        { "desc",     "TEXT",   false,  "ASC" }
      }
    },
    { "indices", { 
        { "sid",      "INT",    false,  "ASC" },
        { "mid",      "INT",    false,  "ASC" },
        { "headword", "TEXT",   true,  "ASC" },
        { "good_example", "INT",false,  "ASC" },
        { "reading",  "TEXT",   false,  "ASC" },
        { "form",     "TEXT",   false,  "ASC" },
        { "sense_no", "INT",    false,  "ASC" }
      }
    },
    { "sentences", { 
        { "id",       "INT",   true,  "ASC" },
        { "lang",     "TEXT",  false,  "ASC" },
        { "text",     "TEXT",  false,  "ASC" }
      }
    },
    { "d_reading", {
        { "rid",      "INT",    true,   "ASC" },
        { "did",      "INT",    true,   "ASC" },
        { "reading",  "TEXT",   true,   "ASC" },
        { "inf",      "TEXT",   false,  "ASC" },
        { "nokanji",  "INT",    false,  "ASC" },
        { "freq",     "INT",    false,  "ASC" }
      }
    },
    { "d_kanji", {
        { "kid",      "INT",    true,   "ASC" },
        { "did",      "INT",    true,   "ASC" },
        { "kanji",    "TEXT",   true,   "ASC" },
        { "inf",      "TEXT",   false,  "ASC" },
        { "freq",     "INT",    false,  "ASC" }
      }
    },
    { "d_sense", {
        { "sid",      "INT",    true,   "ASC" },
        { "did",      "INT",    true,   "ASC" },
        { "gloss",    "TEXT",   false,  "ASC" },
        { "xref",     "TEXT",   false,  "ASC" },
        { "ant" ,     "TEXT",   false,  "ASC" },
        { "inf",      "TEXT",   false,  "ASC" },
        { "pos",      "TEXT",   false,  "ASC" },
        { "field",    "TEXT",   false,  "ASC" },
        { "misc",     "TEXT",   false,  "ASC" },
        { "dial",     "TEXT",   false,  "ASC" }
      }
    },
    { "d_re_restr", { 
        { "rid",      "INT",    false,  "ASC" },
        { "kid",      "INT",    false,  "ASC" }
      }
    },
    { "d_stagk", { 
        { "sid",      "INT",    false,  "ASC" },
        { "kid",      "INT",    false,  "ASC" }
      }
    },
    { "d_stagr", { 
        { "sid",      "INT",    false,  "ASC" },
        { "rid",      "INT",    false,  "ASC" }
      }
    },
    { "k_kanji", {
        { "kanji",      "TEXT",   true,   "ASC" },
        { "ucs",        "TEXT",   false,  "ASC" },
        { "onyomi",     "TEXT",   false,  "ASC" },
        { "kunyomi",    "TEXT",   false,  "ASC" },
        { "meaning",    "TEXT",   false,  "ASC" },
        { "nanori",     "TEXT",   false,  "ASC" },
        { "flags",      "TEXT",   false,  "ASC" },
        { "jlpt",       "INT",    false,  "ASC" },
        { "grade",      "INT",    false,  "ASC" },
        { "freq",       "INT",    false,  "ASC" },
        { "strokes",    "INT",    false,  "ASC" },
        { "rad_classic","INT",    false,  "ASC" },
        { "rad_nelson", "INT",    false,  "ASC" },
        { "components", "TEXT",   false,  "ASC" }
      }
    },
    { "components", {
        { "component",  "TEXT",   false,  "ASC" },
        { "strokes",    "INT",    false,  "ASC" }
      }
    },
    { "k_skip", {
        { "kanji",      "TEXT",   true,   "ASC" },
        { "skip1",      "INT",    false,  "ASC" },
        { "skip2",      "INT",    false,  "ASC" },
        { "skip3",      "INT",    false,  "ASC" },
        { "misclass",   "TEXT",   false,  "ASC" },
      }
    }
  } // main database tables
  }, // main database
  // user database
  { "user", {
    { "config", {
        { "key",        "TEXT",   false,  "ASC" },
        { "val",        "TEXT",   false,  "ASC" }
      }
    }
  } // user db tables
  } // user database
}; // db_tables;



/*!
* Contains current and default (compile time) configuration.
* Colors are always saved as in hexadecimal form as strings 0xRRGGBB00.
* Does not acces to database - this is a responsibility of App.
* \sa App::save_config()
* \sa App::load_config()
*/
class Config 
{
  public:
    enum ValueType { STRING, INT, BOOL, COLOR, FONT };
    struct Value {
      string val;
      string desc;
      ValueType type;
    };
  private:
    std::map<string,Value> data_;       //!<  current configuration (used in app)
    std::map<string,Value> default_;    //!<  default config (as defined in config.cxx)
    std::map<string,Value> overrides_;  //!<  cmdline overrides

  public:
    Config();
    //! Destructor. Empty.
    ~Config(){};

    //! Returns current configuration.
    inline std::map<string,Value> get_map () const { return data_; };
    //! Return default (compile time configuration.
    inline std::map<string,Value> get_default_map () const { return default_; };


    /*!
    * Returns a copy of the config variable <i>var</i> (for int, bool).
    * \exception std::runtime_error When key <i>var</i> does not exist or when val can not be found.
    */
    template<class T>
    inline T get ( const string &var )
    {
      Value *val = nullptr;
      if ( overrides_.find(var) != overrides_.end() )
        val = &overrides_[var];
      else if ( data_.find(var) == data_.end())
        throw std::runtime_error("Config::get(\""+var+"\"): unknown variable");
      else
        val = &data_[var];
      if ( !val )
        throw std::runtime_error("Config::get(\""+var+"\"): no value found");
      std::stringstream ss;
      T v;
      ss << val->val;
      ss >> v;
      return v; 
    }


    /*!
    * Sets a config variable (int, bool) <i>var</i> to <i>val</i>.
    * Colors are saved as strings.
    * \exception std::runtime_error When key <i>var</i> does not exist.
    * \note C++: implicit conversion bool->int
    */
    inline void set ( const string &var, int val ) {
      if ( data_.find(var) == data_.end())
        throw std::runtime_error("Config::get(\""+var+"\"): unknown variable");
      Value *v = &data_.at(var);
      if ( v->type == COLOR ){
        char buff[16];
        snprintf( buff, 15, "0x%08x", val);
        v->val = buff;
      }
      else
        v->val = std::to_string(val); 
    }
    /*!
    * Sets a config variable to <i>val</i>.
    * \exception std::runtime_error When key <i>var</i> does not exist.
    * \note C++: implicit conversion bool->int
    */
    inline void set ( const string &var, const string &val ) { 
      if ( data_.find(var) == data_.end())
        throw std::runtime_error("Config::get(\""+var+"\"): unknown variable");
      Value *v = &data_.at(var);
      v->val = val; 
      v->val = val; 
    }
    /*!
    * Overrides a config variable. Overriden value is used but not saved.
    * \exception std::out_of_range whet var does not exist ( map.at(var) )
    * \sa set()
    */
    inline void set_override ( const string &var, const string &val ){
      Value v = data_.at(var);
      v.val = val;
      overrides_[var] = v;
    }
};


} // namespace aoi_config
#endif // _CONFIG_HXX
