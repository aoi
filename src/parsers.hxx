/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _PARSERS_HXX
#define _PARSERS_HXX

/*! \file parsers.hxx
* Parsers for JMdict and kanjidic2.
*/


#include <vector>
#include <string>
#include <map>
#include <fstream>
#include "3rdparty/rapidxml.hpp"
#include "utils.hxx"
#include "datatypes.hxx"

using aoi::SEPARATOR_SQL;
using aoi::ElementKanji;
using aoi::ElementReading;
using aoi::ElementSense;
using aoi::DicWord;
using aoi::Kanji;
using std::vector;
using std::string;
using std::map;
using rapidxml::xml_node;
using rapidxml::xml_document;

namespace parsers {


/*!
* Base XML parser class. Loads XML file and build XML tree. Must be subclassed.
* \todo Parser should escape aoi::SEPARATOR_SQL character right after loading 
* file into memory (i.e. before parsing).
*/
class BaseParser 
{
  private:
    char *buffer_;

  protected:
    xml_document<> doc_;
    std::ifstream file_;

    /*!
    * Get values of all the elements of the type <i>element</i> in <i>node</i>.
    * For example XML code:
    \verbatim
    <person>
      <name>John Doe</name>
      <phone>1232456789</phone>
      <phone>987654321</phone>
    </person>
    \endverbatim
    * get_elements( node, phone ) returns "{ "123456789", "987654321" }"
    * \param parent       parent node
    * \param element      what element to get
    * \param unreference  if true: remove '&' and ';' from the string borders
    * \return values of all the elements <i>element</i> or empty vector 
    */
    static vector<string> get_elements ( xml_node<> *parent, const char *element, 
                                         bool unreference=false );

  public:
    BaseParser ( const char *filename );
    virtual ~BaseParser ();

    /*!
    * Scans first node of the document for the entities (<!ENTITY).
    * \return map in format entity_name:entity_description 
    */
    map<string,string> get_entities ();
};


//! Parser for JMDict_e XML file.
class JmdictParser : public BaseParser
{
  private:
    int n_entries_ = 0;
    int n_reading_ = 0;
    int n_kanji_   = 0;
    int n_gloss_   = 0;
    int n_sense_   = 0;
    xml_node<> *entry_ = nullptr;

  public:
    JmdictParser( const char *filename ) : BaseParser(filename) 
      { entry_ = doc_.first_node("JMdict")->first_node("entry"); };
    ~JmdictParser() {};

    /*!
    * Gets one entry from JMdict. Caller should call this function until 
    * Dicword.did() != -1
    \verbatim
    JmdictParser p("file.xml");
    DicWord w = p.get_entry();
    while ( w.did() != -1 ){
      printf("Word ID: %d\n", w.did());
      w = p.get_entry();
    }
    \endverbatim
    * \return DicWord on succes, empty DicWord (did()=-1) otherwise
    */
    DicWord get_entry ();

    //! Returns JMDict version.
    string get_version ();
    
};


//! Parser for kanjidic2 XML file.
class KanjidicParser : public BaseParser
{
  private:
    int n_entries_ = 0;
    xml_node<> *entry_ = nullptr;

  public:
    KanjidicParser( const char *filename ): BaseParser(filename)
      { entry_ = doc_.first_node("kanjidic2")->first_node("character"); };
    ~KanjidicParser(){};

    /*!
    * Gets one entry from kanjidic2. Caller should call this function until
    * Kanji.kanji() != ""
    \verbatim
    KanjidicParser p("file.xml");
    Kanji k = p.get_entry();
    while ( !k.kanji().empty() ){
      printf("Kanji: %s\n", k.kanji().c_str());
      k = p.get_entry();
    }
    \endverbatim
    * \return Kanji on success, empty Kanji (kanji()=="") otherwise
    */
    Kanji get_entry ();

    //! Returns kanjidic2 version in format: "version (date)"
    string get_version ();
};


} // namespace parsers
#endif // _PARSERS_HXX
