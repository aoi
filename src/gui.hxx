/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _GUI_HXX
#define _GUI_HXX

#include <cmath>
#include <vector>
#include <string>
#include <map>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Button.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Help_Dialog.H>
#include <FL/Fl_Widget.H>

#include "aoi.hxx"
#include "gui_settings.hxx"
#include "gui_widgets.hxx"
#include "gui_dialogs.hxx"
#include "gui_dicview.hxx"
#include "gui_kanjiview.hxx"

/*! \file gui.hxx
* Contains class GUI, which manages the main window and all the dialogs.
* \todo Separate all the functions working as interface for the App.
*/


namespace aoi { 
  class App; 
}


using std::vector;
using std::string;
using aoi::App;


namespace aoi_ui {

class GUI;

/*! 
* Closes all windows and quits FLTK. Used as callback in menu and MainWindow.
*/
static void quit_fltk ( Fl_Widget *w, void *p )
{
  Fl_Window *mainwindow = (Fl_Window*)p;
  // hide all other windows
  Fl_Window *win = Fl::next_window( mainwindow );
  while ( win ){
    win->hide();
    win = Fl::next_window( mainwindow );
  }
  // hide main window and thus exit program
  mainwindow->hide();
}


/*!
* Centers the window w on the first visible window or on its parent if the 
* parent is not nullptr. 
* If the w is smaller than parent then positions w on top left corner od parent.
* Works only on the hidden w.
* \param ontop if true then w is brought to the front
* \todo does not work after start
* \todo check whether ontop works (in creating dictionary)
*/
static void center_window ( Fl_Window *w, Fl_Window *parent=nullptr, bool ontop=true)
{
  if ( w->visible() )
    return;
  if ( !parent )
    parent = Fl::first_window();
  if ( !parent )
    return;
  // new position
  int nx = (parent->w() - w->w())/2;
  int ny = (parent->h() - w->h())/2;
  if ( nx < 10 ) nx=10;
  if ( ny < 10 ) ny=10;
  w->position( parent->x_root()+nx, parent->y_root()+ny );
  // should ensure, that w will be on top ...
  if (ontop){
    w->hide();
    w->show();
  }
}


/*! 
* Dialog for the testing of romanization (both hiragana and katakana).
* Shows the input and output field.
*/
class DialogRomanizationTest : public Fl_Double_Window
{
  private:
    Fl_Input  *input_   = nullptr;
    Fl_Input  *output_  = nullptr;
    Fl_Choice *choice_  = nullptr;
    Fl_Button *b_close_ = nullptr;
    string buff_;
  public:
    DialogRomanizationTest( int w=300, int h=180 );
    ~DialogRomanizationTest(){};
    void cb_input ();
    inline static void scb_input( Fl_Widget *w, void *p )
      { ((DialogRomanizationTest*)p)->cb_input();   }
    inline static void scb_close ( Fl_Widget *w, void *p )
      { ((DialogRomanizationTest*)p)->hide(); }
};


/*! Container for all MainWindow related widgets.
* Handles global shortcuts
* ALL THE CONFIGURATIONS OF ITS WIDGETS SHOULD BE DONE IN GUI::GUI()
*/
class MainWindow : public Fl_Double_Window
{
  private:
    GUI   *parent_ = nullptr;
  public:
    Fl_Menu_Button *b_menu_   = nullptr;
    Fl_Button *b_toggle_view_ = nullptr; // switch between dictionary and kanjiview

    Fl_Group *grp_dictionary_ = nullptr;
    Fl_Group *grp_kanjidic_   = nullptr;

    // grp_dictionary_
    DicView *dic_view_        = nullptr;
    Entry *dic_input_         = nullptr; 
    Fl_Check_Button *cb_verb_ = nullptr;
    Fl_Check_Button *cb_adj_  = nullptr;
    Fl_Check_Button *cb_noun_ = nullptr;
    Fl_Check_Button *cb_expr_ = nullptr;

    // grp_kanjidic_
    Label *l_knj_results      = nullptr;
    Entry *e_skip             = nullptr;
    Entry *e_strokes          = nullptr;
    Entry *e_jlpt             = nullptr;
    Entry *e_grade            = nullptr;
    KanjiView *knj_view_      = nullptr;
    Fl_Choice *knj_orderby_   = nullptr;
    ComponentView *compo_view_  = nullptr;
    Fl_Button *b_knj_clear_     = nullptr;
    Fl_Button *b_knj_search_    = nullptr;
    Fl_Check_Button *cb_knj_jis208 = nullptr;
    // toggle sort components by strokes / by freq
    Fl_Check_Button *cb_knj_components = nullptr;


    MainWindow ( GUI *parent, int w, int h );
    ~MainWindow() {};

    int handle ( int event );
};


/*!
* Encapsulates the MainWindow and all dialogs.
* Provides interface for class App.
* \sa App
*/
class GUI
{
  private:
    aoi::App *parent_;

    int font_base_size_             = 12;
    string fontname_kanji_          = "";
    string help_file_               = "";

    MainWindow *main_window_        = nullptr;

    DialogFontTest *dlg_fonts_      = nullptr;
    DialogDownload *dlg_download_   = nullptr;
    DialogProgress *dlg_progress_   = nullptr;
    KanjiPopupWindow *popup_        = nullptr;
    Fl_Help_Dialog *dlg_help_       = nullptr;
    DialogEditWord *dlg_edit_word_  = nullptr;
    ManageDBDialog *dlg_manage_db_  = nullptr;

    //! Prevents closing MainWindow when ESC pressed.
    inline static void scb_main_window_ ( Fl_Widget *w, void *f )
    {
      if ( Fl::event() == FL_SHORTCUT ){
        switch ( Fl::event_key() ){
          case FL_Escape:   // dont close main window on ESC
          case 'm':         
            return;
        }
      }
      quit_fltk( w, (void*)w );
    };


    void change_cursor ( Fl_Cursor c = FL_CURSOR_DEFAULT );
    

  public:

    GUI ( aoi::App *parent );
    ~GUI ();

    /*!
    * Sets FONT_KANJI a FL_SYMBOL (needed in Fl_Help_Dialog).
    * Initialize fonts in all relevant widgets and dialogs. 
    */
    void init_fonts();

    /*!
    * Sets FL_BACKGROUND_COLOR, FL_BACKGROUND2_COLOR, FL_FOREGROUND_COLOR
    * and FL_SELECTION_COLOR
    */
    void init_colors();

    /*!
    * Displays simple window with the text <i>s</i> and 2 buttons 
    * (default: <b>Yes</b> and </b>No</b>). String <i>s</i> may contain
    * newlines ('\n').
    * \return  pressed button number (0 for the first, 1 for the second)
    */
    inline int choice ( const string &s, const string &btn1="Yes", 
                        const string &btn2="No" ){
      return fl_choice( s.c_str(), btn1.c_str(), btn2.c_str(), 0);
    }


    //! Shows MainWindow and returns Fl::run()
    int run ( int argc, char **argv );

    //! Reset checkboxes for filtering pos in dictionary (noun,verb, adj, exp)
    void reset_filters ();

    /*!
    * Shows and sets modal dialog with progress.
    */
    inline void progress ( float percent, const string &message ){
      if ( ! dlg_progress_->visible() )
        dlg_progress_->show();
      center_window( dlg_progress_ );
      dlg_progress_->progress( percent, message );
    }
    //! Hides modal progress dialog.
    inline void progress_hide (){
      dlg_progress_->init();
      dlg_progress_->hide();
      Fl::check();
    }

    /*!
    * Shows simple textview with HTML 2.0 support.
    */
    inline void show_html ( const string &s ){
      DialogHtmlView *d = new DialogHtmlView(600,400);
      d->set(s);
      d->show();
    }

    inline void alert ( const char *msg ) { fl_alert("%s",msg); };
    inline void alert ( const string &s ) { alert(s.c_str()); };

    /*! 
    * Shows download dialog for given url.
    * \return name(path) of the downloaded file
    */
    inline string download_dialog ( const string &url ){
      string fname = utils::guess_fname_from_url(url);
      dlg_download_->set_names ( url, fname );
      center_window( dlg_download_ );
      dlg_download_->run();
      return fname;
    }

    //! Shows no. of found dictionary results in DicView header.
    inline void set_dic_results ( const string &s ) 
      { main_window_->dic_view_->headers( {"","",s} ); };

    // XXX: does not work
    //! Shows no. of found kanji results in label.
    inline void set_kanji_results ( const string &s ) { 
      main_window_->l_knj_results->show(); 
      main_window_->l_knj_results->set(s); 
    };

    //! Shows <i>Manage database</i> dialog.
    inline ManageDBDialog *dlg_manage_db (){
      center_window( dlg_manage_db_, main_window_);
      return dlg_manage_db_;
    };

    /*! 
    * Copy userdata to clipboard and selection buffer (middle button on Linux).
    */
    static void scb_copy ( Fl_Widget *w, void *userdata ) {
      const char *d = (const char*)userdata;
      int len = strlen(d);
      Fl::copy( d, len, 0);
      Fl::copy( d, len, 1);
    }


    //! Change the cursor to WAIT (hourglass).
    inline void cursor_wait () { change_cursor(FL_CURSOR_WAIT); };
    //! Change the cursor to default (arrow).
    inline void cursor_default () { change_cursor(); }

    //! Returns the status of checkbox above the list of the components
    inline bool sort_components_by_strokes () const {
      return main_window_->cb_knj_components->value();
    }

    //! Shows <i>Font</i> dialog.
    int select_font () {
      center_window( dlg_fonts_ );
      return dlg_fonts_->run();
    }

    //! Shows <i>Romanization test</i> dialog.
    void show_romanization_test ()
    {   
      DialogRomanizationTest *d = new DialogRomanizationTest();
      d->show();
    }

    //! Shows dialog <i>Settings</i>.
    void show_settings ();

    /*! 
    * Popups the (context) menu in DicView.
    * \param did  JMdict id of the word.
    * \param things List of kanji in the word.
    * \param notes If false then item <i>Notes</i> will be grayed out.
    * \param examples If false then item <i>Examples</i> will be grayed out.
    */
    void dicview_menu ( int did, const vector<string> &things, 
                        bool notes=false, bool examples=false );

    /*! Shows the <i>Edit word</i> dialog.
    */
    void edit_word ( const aoi::DicWord &w );

    //! Popups KanjiPopupWindow in KanjiView.
    void popup_kanji ( const Kanji &k );
    /*!
    * Tells App to find kanji <i>k</a> in the database 
    * and call popup_kanji( const Kanji &k ).
    */
    void popup_kanji ( const string &k );

    //! Sets the file serving as index for the help.
    inline void help_file ( const string &s ) { help_file_ = s; };
    /*! Sets the name of the font that will be used for non ASCII characters.
    * \note the font should be able to display JIS X 208/212/213
    */
    inline void fontname_kanji ( const string &s ) 
      { fontname_kanji_=s; init_fonts(); };
    //! The base size of the fonts. All other sized are derived from this value.
    inline void font_base_size ( int s ) 
      { font_base_size_=s; init_fonts(); };
    //! Returns the content of the input field "SKIP"
    inline string skip () const { return main_window_->e_skip->value(); };
    //! Return the selected sort mode (in kanjiview)
    inline int sort_mode () const { return main_window_->knj_orderby_->value(); };
    //! Returns the content of the input field "Strokes" (#)
    inline const char *strokes () const { return main_window_->e_strokes->value(); };
    //! Returns the content of the input field "JLPT"
    inline const char *jlpt () const { return main_window_->e_jlpt->value(); };
    //! Returns the content of the input field "Grade" (Grd.)
    inline const char *grade () const { return main_window_->e_grade->value(); };
    //! Returns all the components selected by the leftclick.
    inline vector<string> components_include () const 
      { return main_window_->compo_view_->selected1(); };
    //! Returns all the components selected by the rightclick.
    inline vector<string> components_exclude () const 
      { return main_window_->compo_view_->selected2(); };
    //! Highlights all the components that are the part of the currently selected kanji.
    inline void highlight_components ( const string &comps )
      { main_window_->compo_view_->set_highlight(comps); }
    
    /*! Displays the fileselection dialog. On the Windows it should be the native
    * dialog, on the Linux the FLTK's. 
    * \returns path or ""
    */
    string ask_file (){
      Fl_Native_File_Chooser fnfc;
      fnfc.title("Pick a file");
      fnfc.type(Fl_Native_File_Chooser::BROWSE_FILE);
      switch ( fnfc.show() ) {
        case -1: 
          throw std::runtime_error( fnfc.errmsg() );    
          break;
        case  1: 
          return "";
      }
      return fnfc.filename();
    }

    //! Returns selected fitlers for listview as string ( "noun", "verb", "adj", "expr")
    vector<string> listview_filters ();

    inline int dicview_selected_rowid () const
      { return main_window_->dic_view_->selected_row_id(); };

    inline void set_components ( const vector<ComponentView::Cell> &v ) 
      { main_window_->compo_view_->set_data(v);  };

    inline void set_listview ( const vector<string> &d, const vector<int> &r={} ) 
      { main_window_->dic_view_->set_data(d,r);  };
    inline void register_tag_dicview ( const string &tag, const TextStyle &style )
      { main_window_->dic_view_->register_tag(tag,style); };

    inline void set_kanjiview ( const vector<KanjiView::Cell> &d ) 
      { main_window_->knj_view_->set_data(d);  };

    inline const char *get_dic_input () const 
      { return main_window_->dic_input_->value(); };

    void cb_toggle_group ();
    void cb_knj_clear ();
    void cb_show_help (){
      dlg_help_->load(help_file_.c_str());
      dlg_help_->show();
    };
    void cb_jis208_toggle ();

    inline static void scb_menu_show_kanji ( Fl_Widget *w, void *p ){  
      char name[64];  // the last picked menu item
      if ( ((Fl_Menu_*)w)->item_pathname(name, sizeof(name)-1) == 0 )
        // menu item is in the form: "Kanji/Show"
        ((GUI*)p)->popup_kanji( strtok( name, "/" ) );
    }
    inline static void scb_knj_clear ( Fl_Widget *w, void *f )
      {((GUI *)f)->cb_knj_clear(); }
    inline static void scb_toggle_group(Fl_Widget *w, void *f) 
      {((GUI *)f)->cb_toggle_group();}
    inline static void scb_show_help(Fl_Widget *w, void *f) 
      {((GUI *)f)->cb_show_help();}
    inline static void scb_jis208_toggle(Fl_Widget *w, void *f) 
      {((GUI *)f)->cb_jis208_toggle();}
    inline static void scb_test_romanization ( Fl_Widget *w, void *p )
      {((GUI*)p)->show_romanization_test(); }
    inline static void scb_test_fonts ( Fl_Widget *w, void *p )
      {((GUI*)p)->select_font(); }
    inline static void scb_show_settings ( Fl_Widget *w, void *p )
      {((GUI*)p)->show_settings(); }
};


} // namespace

#endif // _GUI_HXX
