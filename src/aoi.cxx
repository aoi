/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cmath>
#include <set>
#include <FL/fl_ask.H>
#include "aoi.hxx"
#include "utils.hxx"

namespace aoi {

using std::set;
using aoi_ui::TextStyle;
using utils::to_string;

//! See App::load_dbscript() and App::App()
const char *DB_BACKUP_FILE            = "db.backup";
const char *LOG_FILE                  = "aoi.log";
const char SENSE_SEARCH_CHAR          = ':';

App* App::instance_ = nullptr;

App::App ()
{
  remove(LOG_FILE);
  logger_.filename(LOG_FILE);
  logger_.loglevel(Logger::MSG_DEBUG);

  db_   = nullptr;  // NECESSARY ( if db_ undefined then SEGFAULT in open_database() )
  cfg_  = new aoi_config::Config();
  ui_   = new aoi_ui::GUI(this);
  rmn_  = new Romanization();

  if ( utils::file_exists( DB_BACKUP_FILE ) ){
    log_w("Backup copy of the database found.");
    std::stringstream ss;
    ss << "Backup copy of the database found. Delete it?\n"
      << "WARNING: You should check whether the current database\n" 
      << "works before deleting the backup.";
    if ( ui_->choice(ss.str(), "Delete", "No") == 0 ){
      log("Deleting the backup copy of the database.");
      remove(DB_BACKUP_FILE);
    }   
  }

  ui_->progress(0,"Opening database...");
  open_database();

  ui_->progress( 30, "Checking tables..." );
  check_tables();
  ui_->progress( 60, "Checking indexes..." );
  check_indexes();

  load_config();
  init_dicview();

  #ifdef DEBUG
//    ui_->progress(90, "DEBUG query..." );
    parse_dic_input("ana*");
//    cb_kanji_search();
//    ui_->cb_toggle_group(nullptr);
  #endif

  ui_->progress( 98, "Initializing fonts..." );
  ui_->fontname_kanji( get_config("font/kanji") );
  ui_->help_file( get_config("sources/help_index") );

  ui_->progress_hide();


  auto q = query("select val from aoi where key='jmdict_version'");
  string jmdict_version = (q.empty()) ? "NONE":q[0];
  q = query("select val from aoi where key='kanjidic_version'");
  string kanjidic_version = q.empty() ? "NONE":q[0];
  if ( jmdict_version == "NONE" || kanjidic_version == "NONE" )
    cb_manage_db();


  #ifdef DEBUG
    logger_.loglevel(Logger::MSG_DEBUG);
  #endif

  instance_ = this;
}


App::~App () 
{
  delete db_;
  delete rmn_;
  delete ui_;
  delete cfg_;
}


void App::init_dicview ()
{
  // initialize textstyles
  TextStyle style_default(FL_HELVETICA,1.2);
  TextStyle style_reading(aoi_ui::FONT_KANJI,1.5);
  TextStyle style_reading_freq = style_reading;
  style_reading_freq.color = get_color("frequent");
  TextStyle style_kanji(aoi_ui::FONT_KANJI,1.7);
  TextStyle style_kanji_freq = style_kanji;
  style_kanji_freq.color = get_color("frequent"); 
  TextStyle style_inf(FL_HELVETICA_ITALIC,0.8,get_color("pos"));
  style_inf.offset_y = 3;
  TextStyle style_pos = style_inf;
  TextStyle style_misc = style_pos;
  style_misc.color = get_color("misc");
  TextStyle style_field = style_pos;
  style_field.color = get_color("field");
  TextStyle style_dial = style_pos;

  // register TextStyles
  ui_->register_tag_dicview( "default",       style_default );
  ui_->register_tag_dicview( "reading",       style_reading );
  ui_->register_tag_dicview( "reading_freq",  style_reading_freq );
  ui_->register_tag_dicview( "kanji",         style_kanji );
  ui_->register_tag_dicview( "kanji_freq",    style_kanji_freq );
  ui_->register_tag_dicview( "kinf",          style_inf ); 
  ui_->register_tag_dicview( "rinf",          style_inf ); 
  ui_->register_tag_dicview( "pos",           style_pos ); 
  ui_->register_tag_dicview( "misc",          style_misc ); 
  ui_->register_tag_dicview( "field",         style_field ); 
  ui_->register_tag_dicview( "dial",          style_dial ); 
}


vector<string> App::query ( const char *q, bool log_query, bool replace_separator )
{
  if ( !db_ ){
    log_e("App::query(): Database does not exist.");
    return {};
  }
  ui_->cursor_wait();
  vector<string> result;
  try {
    if ( log_query )
      log_d(string(q));
    result = db_->query(q);
  }
  catch (SQLite3::DatabaseError &e){
    log_e("App: DatabaseError:: " + string(e.what()) + string("\nQuery: ") 
      + string(e.query()) );
    ui_->cursor_default();
  }
  string msg = std::to_string(db_->result_rows()) + " results";
  log( "App::query(): " + msg);
  if ( replace_separator )
    for ( string &s: result )
      utils::replace_all(s, SEPARATOR_SQL, ", ");
  ui_->cursor_default();
  return result;
}


void App::open_database ()
{
  try {
    if ( !db_ )
      db_ = new SQLite3::SQLite3( get_config("db/file_main").c_str() );
    else{
      db_->close();
      db_->open(get_config("db/file_main").c_str());
    }
  }
  catch (SQLite3::CantOpenDatabase &e){
    log_e( "Aoi: Can't open database '"  + string(e.what()) + "'.");
    return;
  }
  std::stringstream ss;
    ss << "ATTACH DATABASE '" << get_config("db/file_user") << "'as user;";
  db_->query(ss.str().c_str());
}


void App::cb_set_components ()
{
  if ( curr_components_.empty() )
    return;

  using aoi_ui::ComponentView;

  // prepare cells
  vector<string> included = ui_->components_include();
  vector<string> excluded = ui_->components_exclude();

  vector<ComponentView::Cell> v;
  for ( string &s: included )
    v.push_back( ComponentView::Cell(s,ComponentView::CELL_SELECTED_1) );
  for ( string &s: excluded )
    v.push_back( ComponentView::Cell(s,ComponentView::CELL_SELECTED_2) );

  // sort by occurences
  if ( !ui_->sort_components_by_strokes() ){
    for ( auto mi = curr_components_.rbegin(); mi!=curr_components_.rend(); ++mi ){
      if ( !utils::is_in( included, mi->second ) 
            && !utils::is_in( excluded, mi->second ) )
        v.push_back( ComponentView::Cell(mi->second) );
    }
  }
  // sort by strokes
  else {
    vector<string> comps_by_strokes;
    for ( auto mi: curr_components_ ){
      if ( mi.first < get_config<int>("knj/min_compo_count") )
        continue;
      if ( !utils::is_in( included, mi.second ) 
            && !utils::is_in( excluded, mi.second ) )
        comps_by_strokes.push_back(mi.second);
    }
 
    struct SortByStrokes {
      map<string,int> comps;
      SortByStrokes( const map<string,int> &c ): comps(c){};
      bool operator() (const string &c1, const string &c2 ){ 
        return ( this->comps[c1] < this->comps[c2] );
      }
    } sbc(components_);
 
    std::sort( comps_by_strokes.begin(), comps_by_strokes.end(), sbc );

    int prev_strokes=0;
    for ( string &s: comps_by_strokes ){
      int curr_strokes = components_[s];
      if ( prev_strokes != curr_strokes )
        v.push_back( ComponentView::Cell( std::to_string(curr_strokes), 
                                          ComponentView::CELL_LABEL) );
      v.push_back( ComponentView::Cell(s) );
      prev_strokes = curr_strokes;
    }
  }

  ui_->set_components( v );
}


void App::cb_kanji_search ()
{

  if ( components_.empty() ){
    log("Loading components...");
    vector<string> res = query("select component, strokes from components");
    int skipped = 0;
    for ( size_t i=0; i<res.size(); i=i+2 ){
      int strokes = 0;
      if ( res[i+1].empty() )
        skipped++;
      else
        strokes = std::stoi(res[i+1]);
      components_[res[i]] = strokes;
    }
    if ( skipped>0 )
      log_d(std::to_string(skipped)+" components without strokes data.");
  }

  // strokes
  std::pair<int,int> strokes = utils::parse_range( utils::strip( ui_->strokes() ) );
  // jlpt
  std::pair<int,int> jlpt = utils::parse_range( utils::strip( ui_->jlpt() ) );
  // grade
  std::pair<int,int> grade = utils::parse_range( utils::strip( ui_->grade() ) );

  // skip
  vector<string> skip = utils::split_string( ui_->skip() ); 
  std::stringstream sskip;
  if ( skip.size() > 0 ){
    string skip1 = utils::strip(skip[0].c_str());
    sskip << " S.skip1=" << skip1; 
  }
  if ( skip.size() > 1 ){
    std::pair<int,int> skip2 = utils::parse_range( utils::strip(skip[1].c_str()) );
    sskip << " and S.skip2>=" << skip2.first << " and S.skip2<=" << skip2.second;
  }
  if ( skip.size() > 2 ){
    std::pair<int,int> skip3 = utils::parse_range( utils::strip(skip[2].c_str()) );
    sskip << " and S.skip3>=" << skip3.first << " and S.skip3<=" << skip3.second;
  }
  if ( !sskip.str().empty() )
    sskip << " and ";

  // ordering  
  string order_by;
  switch ( ui_->sort_mode() ){
    case SORT_FREQ_ASC:
      order_by = "(case freq when 0 then 9999 else freq end) asc";
      break;
    case SORT_FREQ_DESC:
      order_by = "(case freq when 0 then 9999 else freq end) desc";
      break;
    case SORT_STROKES_ASC:
      order_by = "strokes asc";
      break;
    case SORT_STROKES_DESC:
      order_by = "strokes desc";
      break;
  }

  vector<string> components_include = ui_->components_include();
  vector<string> components_exclude = ui_->components_exclude();
  std::stringstream comps;
  for ( auto c: components_include ) 
    comps << " and components like '%" << c << "%'";
  for ( auto c: components_exclude ) 
    comps << " and components not like '%" << c << "%'";
  printf("%s\n",comps.str().c_str());

  string jis208 = "";
  if ( get_config<bool>("knj/jis208_only") )
    jis208 = " flags glob '*jis208*' and ";

  // build query
  std::stringstream ss;
  ss  <<  "select distinct "
      <<    "K.kanji,freq,components,flags "
      <<  " from k_kanji as K"
      <<    (sskip.str().empty() ? " where ":", k_skip as S where K.kanji=S.kanji and")
      <<    jis208
      <<    sskip.str()
      <<    " strokes>=" << strokes.first << " and strokes<=" << strokes.second
      <<    " and jlpt>=" << jlpt.first << " and jlpt<=" << jlpt.second
      <<    " and grade>=" << grade.first << " and grade<=" << grade.second
      <<    comps.str()  
      <<  " order by " << order_by;
  

  // perform query
  vector<string> q = query( ss.str().c_str(), true, false );
  vector<aoi_ui::KanjiView::Cell> data;
  vector<string> components;
  set<string> flags;
  for ( size_t i=0; i<q.size(); i+=4 ){
    string kanji = q[i];
    int freq = std::stoi(q[i+1]);
    data.push_back( 
      aoi_ui::KanjiView::Cell( 
        kanji, 
        (freq>0)? get_color("frequent"):-1 
      ) 
    );
    components.push_back(q[i+2]);
    for ( string &s: utils::split_string( q[i+3], SEPARATOR_SQL) )
      flags.insert(s);
  }

  log_d("Groups: " + utils::to_string(flags));
  
  ui_->set_kanjiview( data );
  char b[32];
  sprintf( b, "%d results", db_->result_rows() );
  ui_->set_kanji_results( b );

  utils::Histogram<string> histogram;
  for ( string &s: components )
    histogram.add( utils::str_to_chars(s.c_str()) );
  curr_components_ = histogram.sorted();

  cb_set_components();
}


void App::alert ( const string &msg, const string &desc )
{
  std::stringstream ss;
  ss << msg;
  if ( !desc.empty() )
    ss << "\n\nDetails:\n" << desc;
  log_e(ss);
  ui_->alert(ss.str());
}


int App::run ( int argc, char **argv )
{
  return ui_->run(argc,argv);
}


void App::check_tables ()
{
  for ( auto &dbit: aoi_config::db_tables) {
    // no need for vacuum here, it will be done by check_indexes()
    log("Checking tables in database: " + string(dbit.first));
    std::stringstream q;
    q << "SELECT name FROM " << dbit.first << "." << "sqlite_master WHERE type='table'";
    vector<string> existing = query(q.str().c_str());
    // CREATE TABLE name ( column1 TYPE, column2 TYPE, ... )
    for ( auto &table: dbit.second ){
      if ( utils::is_in(existing, string(table.first)) )
        continue;
      log("Creating table " + string(table.first));
      vector<string> v;
      for ( auto &column: table.second ){
        std::stringstream sstr;
        sstr << column.name << " " << column.type;
        v.push_back(sstr.str()); 
      }
      std::stringstream ss;
      ss << "CREATE TABLE " << dbit.first << "." << table.first 
        << " (" << to_string(v,",") << ");\n";
      query(ss.str().c_str());
    }
    log("Check done.");
  }
}


void App::check_indexes ()
{
  bool do_vacuum = false;
  log("Checking indexes...");
  for ( auto &dbit: aoi_config::db_tables ) {
    std::stringstream q;
    q << "SELECT name FROM " << dbit.first << "." << "sqlite_master WHERE type='index'";
    vector<string> existing = query(q.str().c_str());
    // CREATE INDEX idx_table_column ON table ( column ASC )
    for ( auto &mi: dbit.second ){  // tables
      for ( auto &c: mi.second ){ // columns
        std::stringstream idx_name;
        idx_name << "idx_" << mi.first << "_" << c.name;
        if ( c.index && !utils::is_in( existing, idx_name.str() ) ){
          log(string("Creating index ") + idx_name.str());
          std::stringstream ss;
          ss << "CREATE INDEX " << idx_name.str() << " ON " << mi.first 
            << "(" << c.name << " " << c.sort << ")";
          query(ss.str().c_str());
          do_vacuum = true;
        }
      }
    }
  }
  if ( do_vacuum )
    query("VACUUM");
  log("Check done.");
}


void App::load_dbscript ( const char *fname )
{
  const char  *TEMP_DBSCRIPT  = "dbscript.temp";
  const char  *TEMP_DATABASE  = "temp.db";
  const int   LINE_BUFFER     = 4096;

  // hide manage db_dialog if visible
  auto *d = ui_->dlg_manage_db();
  if ( d && d->visible() )
    d->hide();

  log("Loading dbscript: "+string(fname));

  log("Decompressing file (if necessary)...");
  utils::gzip_decompress_file( fname, TEMP_DBSCRIPT);

  log("Opening temporary database.");
  remove(TEMP_DATABASE);
  SQLite3::SQLite3 db(TEMP_DATABASE);

  log("Loading...");
  std::ifstream f;
  char line[LINE_BUFFER];
  f.open(TEMP_DBSCRIPT);
  // n lines should be > 2e6 -> percent=n*2e6/100
  size_t n = 0;
  while ( f.good() ){
    if ( n % 5000 == 0 ){
      char b[64];
      snprintf( b, 64, "Loading line: %d", n);
      ui_->progress(n/float(2.5e4), b);
    }
    f.getline( line, LINE_BUFFER );
    db.query(line);
    n++;
  }
  f.close();

  log("Closing temporary database...");
  db.close();
  remove(TEMP_DBSCRIPT);

  log("Closing old database.");
  db_->close();
  
  string dbfile = get_config("db/file_main");

  log("Renaming old database to main.db.bckp.");
  rename( dbfile.c_str(), DB_BACKUP_FILE );

  log("Renaming new database.");
  rename( TEMP_DATABASE, dbfile.c_str() );

  log("Switching to the new database");
  open_database();

  ui_->progress(90, "Checking tables...");
  check_tables();
  ui_->progress(95, "Checking indexes...");
  check_indexes();

  ui_->progress_hide();
  // show informations about db
  cb_manage_db();
}


void App::cb_dic_input ()
{
  parse_dic_input( ui_->get_dic_input() );
  ui_->reset_filters();
}


//void App::on_dic_selected ( int id )
//{
//  log_d("App::on_dic_selected()");
//}


void  App::cb_download_db ()
{
  log_d("Download DB");
  string path = ui_->download_dialog( get_config("sources/url_database") );
  if ( !path.empty() && !utils::file_exists( path ) ){
    log_w("App::cb_download_db(): Not a file: "+path);
    return;
  }

  log("Closing old database.");
  db_->close();
  
  string dbfile = get_config("db/file_main");

  log("Renaming old database to main.db.bckp.");
  rename( dbfile.c_str(), DB_BACKUP_FILE );

  log("Decompressing downloaded file");
  utils::gzip_decompress_file( path.c_str(), dbfile.c_str() );

  log("Switching to the new database");
  open_database();

  ui_->progress(10, "Checking tables...");
  check_tables();
  ui_->progress(50, "Checking indexes...");
  check_indexes();

  remove( path.c_str() );
  ui_->progress_hide();
  // show informations about db
  cb_manage_db();
}


void App::cb_edit_word ()
{
  std::stringstream ss;
  int id = ui_->dicview_selected_rowid();
  ss << "App::edit_word( " << id << " )";
  log_d(ss);
  ui_->edit_word( db_get_word(id));
}


void App::cb_popup_kanji ( const string &kanji )
{
  std::stringstream ss;
  ss << "App::on_kanji_clicked()" << kanji;
  Kanji k = db_get_kanji(kanji);
  ui_->popup_kanji( k );
  // XXX: ? this should be somewhere else (it is not logical here)
  ui_->highlight_components( k.components() );
  log(ss);
}


void App::set_listview ( const vector<string> &v )
{
  if ( !listview_items_.empty() ) listview_items_.clear();
  vector<string> d;
  vector<int> cell_ids;
  size_t i = 0;
  while ( i < v.size() ) {
    int cell_id = std::stoi(v[i]);  // jmdict id
    cell_ids.push_back(cell_id);
    cell_ids.push_back(cell_id);
    cell_ids.push_back(cell_id);
    // pos
    set<string> pos;
    for ( string &elt: utils::split_string( v[i+1], ",") )
      if ( elt.size() > 0 )
        pos.insert(utils::strip(elt.c_str()));
    d.push_back( v[i+2] );            // reading
    d.push_back( v[i+3] );            // kanji
    d.push_back( v[i+4] );            // sense
    listview_items_.push_back( {cell_id, pos, v[i+2], v[i+3], v[i+4]} );
    i += 5;
  }
  char buff[32];
  sprintf( buff, "%d results", db_->result_rows() );
  ui_->set_dic_results( buff );
  ui_->set_listview(d,cell_ids);
}


void App::parse_dic_input ( const char *str )
{
  log_d(string("App::parse_dic_input: \"") +string(str) + string("\""));

  string stripped = utils::strip(str);

  if ( stripped.empty() )
    return;

  const char *s = stripped.c_str();

  string qq;
  if ( s[0] != SENSE_SEARCH_CHAR ){
    DictionaryInputParser p;
    qq = p.parse(s);
    
    if ( p.warning() ){
      int res = ui_->choice( 
        "Too broad search.\n"\
        "Your computer may become unresponsible for a long time.\n"\
        "Proceed?",
        "Proceed",
        "Cancel"
        );
        if ( res == 1 ) // Cancel
          return;
    }
 
    // append * at the end of the simple string (just text and nothing else)
    if ( !strchr(s,'*') && !strchr(s,'?') && !strchr(s,'[') && !strchr(s,'{') 
        && !strchr(s,'(') )
      qq += "*";
  }

  std::stringstream q; 
  if ( s[0] == SENSE_SEARCH_CHAR ){
    q << "select did,"
      << "group_concat(pos) as pos," 
      << q_reading("d_sense.did")
      << q_kanji("d_sense.did")
      << q_sense()
      << " from d_sense where gloss glob '*" << stripped.substr(1) << "*'"
      << " group by did";
  }
  else if ( rmn_->contains_kanji( qq.c_str() ) ) {
    q << "select d_kanji.did as did,"
      << "(select group_concat(pos) from d_sense where d_sense.did = d_kanji.did) as pos," 
      << q_kanji() 
      << q_reading("d_kanji.did")
      << q_sense("d_kanji.did")
      << "from d_kanji where "
        << "kanji glob '" << rmn_->romaji_to_hiragana(qq.c_str()) << "' "
        << " or kanji glob '" << rmn_->romaji_to_katakana(qq.c_str()) << "' "
      << " group by did order by d_kanji.freq desc, d_kanji.kanji asc";
  }
  else {
    q << "select d_reading.did as did,"
      << "(select group_concat(pos) from d_sense where d_sense.did = d_reading.did) as pos," 
      << q_reading()
      << q_kanji("d_reading.did") 
      << q_sense("d_reading.did")
      << "from d_reading where "
        << "reading glob '" << rmn_->romaji_to_hiragana(qq.c_str()) 
        << "' or reading glob '"<< rmn_->romaji_to_katakana(qq.c_str()) << "' "
      << " group by did order by d_reading.freq desc, d_reading.reading asc";
  }
  set_listview(query(q.str().c_str()));
}


void App::cb_examples ()
{
  log_d("cb_examples():" + std::to_string(ui_->dicview_selected_rowid()));
  DicWord w = db_get_word( ui_->dicview_selected_rowid() );
  if ( w.k_ele().empty() )
    return;
  std::stringstream q;
  // returns: japanese sentence, english sentence, string to be highlighted
  q << "select"
      << " (select text from sentences where id=sid) as jp,"
      << " (select text from sentences where id=mid) as en,"
      << " good_example,"
      << " form"
    << " from indices where headword='"  << w.k_ele()[0].kanji() 
    << "' order by good_example desc;";
  vector<string> res = query(q.str().c_str());
  std::stringstream ss;
  for ( size_t i=0; i<res.size(); i+=4 ){
    bool good_example =  std::stoi(res[i+2]);
    ss << "&nbsp;<br><font face=\"symbol\"";
    if ( good_example )
      ss << " size=\"5\" color=\"blue\"";
    ss << ">" << res[i] << "</font><br>";
    if ( !res[i+1].empty() )
      ss << res[i+1] << "<br>";
    ss << "&nbsp;<br>";
    // form = res[i+3]
  }
  ui_->show_html(ss.str());
}


DicWord App::db_get_word ( int id )
{
  DicWord w(id);
  std::stringstream ss;

  // kanji
  ss << "select kid,kanji,inf,freq from d_kanji where did=" << id <<";";
  vector<string> res = query( ss.str().c_str() );
  for ( size_t i=0; i<res.size(); i+=4 ){
    int kid             = std::stoi(res[i]);
    string kanji        = res[i+1];
    vector<string> inf  = utils::split_string(res[i+2],SEPARATOR_SQL);
    bool freq           = std::stoi(res[i+3]);
    w.k_ele( ElementKanji( kid, kanji, inf, freq ) );
  }

  // reading 
  ss.str("");
  ss.clear();
  ss << "select rid,reading,inf,nokanji,freq from d_reading where did=" << id << ";";
  res = query( ss.str().c_str() );
  for ( size_t i=0; i<res.size(); i+=5 ){
    int rid             = std::stoi(res[i]);
    string reading      = res[i+1];
    vector<string> inf  = utils::split_string(res[i+2],SEPARATOR_SQL);
    bool nokanji        = std::stoi(res[i+3]);
    bool freq           = std::stoi(res[i+4]);
    w.r_ele( ElementReading( rid, reading, nokanji, {/*restr*/}, inf, freq ) );
  }

  // sense
  ss.str("");
  ss.clear();
  ss << "select sid,gloss,xref,ant,inf,pos,field,misc,dial from d_sense where did=" << id << ";";
  res = query( ss.str().c_str() );
  for ( size_t i=0; i<res.size(); i+=9 ){
    int sid               = std::stoi(res[i]);
    vector<string> gloss  = utils::split_string(res[i+1],SEPARATOR_SQL);
    vector<string> xref   = utils::split_string(res[i+2],SEPARATOR_SQL);
    vector<string> ant    = utils::split_string(res[i+3],SEPARATOR_SQL);
    vector<string> inf    = utils::split_string(res[i+4],SEPARATOR_SQL);
    vector<string> pos    = utils::split_string(res[i+5],SEPARATOR_SQL);
    vector<string> field  = utils::split_string(res[i+6],SEPARATOR_SQL);
    vector<string> misc   = utils::split_string(res[i+7],SEPARATOR_SQL);
    vector<string> dial   = utils::split_string(res[i+8],SEPARATOR_SQL);
    w.s_ele( ElementSense(  sid, gloss, {/*stagk*/}, {/*stagr*/}, pos, xref,
                            ant, field, misc, dial, inf) );
  }

  return w;
}


Kanji App::db_get_kanji ( const string &kanji )
{
  log("App::db_get_kanji()");
  std::stringstream q;
  q << "select kanji,strokes,ucs, rad_classic, rad_nelson, "
          << "jlpt, grade, freq, onyomi, kunyomi, nanori, meaning,flags,components "
    << "from k_kanji where kanji='" << kanji << "';";

  vector<string> res = query(q.str().c_str());
  Kanji kk(res[0]);
  kk.strokes(std::stoi(res[1]));
  kk.ucs(res[2]);
  kk.rad_classic(std::stoi(res[3]));
  kk.rad_nelson( (res[4].empty()) ? -1:std::stoi(res[4]));
  kk.jlpt(std::stoi(res[5]));
  kk.grade(std::stoi(res[6]));
  kk.freq(std::stoi(res[7]));
  kk.onyomi( utils::split_string(res[8],SEPARATOR_SQL) );
  kk.kunyomi( utils::split_string(res[9],SEPARATOR_SQL) );
  kk.nanori( utils::split_string(res[10],SEPARATOR_SQL) );
  kk.meaning( utils::split_string(res[11],SEPARATOR_SQL) );
  kk.flags( utils::split_string(res[12],SEPARATOR_SQL) );
  kk.components( res[13] );

  string qq = "select skip1,skip2,skip3,misclass from k_skip where kanji='"
    + kanji + "';";
  vector<string> res2 = query(qq.c_str());
  if ( res2.size() % 4 != 0 ){
    std::stringstream ss;
    ss << "Wrong SKIP count. Kanji: " << kanji 
      << "Query result size: " << res2.size() 
      << " (should be 4,8 or 12).  SKIP not loaded.";
    log_e(ss);
    return kk;
  }
  for ( size_t i=0; i < res2.size(); i+=4 )
    kk.skip( res2[i], res2[i+1], res2[i+2], res2[i+3] );
  return kk;
}


void App::cb_filter_listview ()
{
  vector<string> pos = ui_->listview_filters();
  log_d("App::cb_filter_listview(): pos: " + utils::to_string(pos));
  bool filter_expr  = ( utils::is_in( pos, string("expr") ) );
  bool filter_noun  = ( utils::is_in( pos, string("noun") ) );
  bool filter_verb  = ( utils::is_in( pos, string("verb") ) );
  bool filter_adj   = ( utils::is_in( pos, string("adj")  ) );
  vector<string> data;
  vector<int> ids;
  size_t n = 0;
  for ( auto &elt: listview_items_ ){
    auto start = elt.pos.begin();
    auto end = elt.pos.end();
    if ( filter_expr && std::find( start, end, "exp") == end )
      continue;
    if ( filter_noun && std::find( start, end, "n" ) == end )
      continue;
    if ( filter_adj && std::find_if( start, end, 
      [](const string &s){ return strncmp(s.c_str(),"adj",3)==0;} ) == end )
      continue;
    if ( filter_verb && std::find_if( start, end, 
      [](const string &s){ return strncmp(s.c_str(),"v",1)==0;} ) == end )
      continue;
    ids.push_back( elt.did );
    ids.push_back( elt.did );
    ids.push_back( elt.did );
    data.push_back ( elt.reading );
    data.push_back ( elt.kanji );
    data.push_back ( elt.sense );
    n++;
  } 
  ui_->set_listview( data, ids );
  std::stringstream ss;
  ss << n << " results";
  if (  listview_items_.size() != n )
    ss << " (" <<  listview_items_.size()-n << " hidden)"; 
  log(ss.str());
  ui_->set_dic_results( ss.str() );
}
    

void App::cb_dicview_rightclick ( int did )
{
  string q = "select group_concat(kanji,'') from d_kanji where did="
    + std::to_string(did);
  vector<string> res = query( q.c_str() );
  set<string> kanji;
  for ( string &c: utils::str_to_chars(res[0].c_str()) )
    if ( App::get()->rmn()->is_kanji(c.c_str()) )
      kanji.insert(c);
  ui_->dicview_menu( did, vector<string>(kanji.begin(),kanji.end()), false, true );
}


void App::apply_config ()
{
  ui_->font_base_size( get_config<int>("font/base_size"));
  logger_.loglevel( get_config("log/level") );
  ui_->init_colors();
  Fl::check();
}


void App::cb_manage_db ()
{
  auto q = query("select key, val from aoi");

  std::map<string,string> mm;
  for ( size_t i=0; i<q.size(); i+=2 )
    mm[q[i]] = q[i+1];

  auto *d = ui_->dlg_manage_db();
  d->main_version(      mm["db_version"]);
  d->main_created(      mm["db_created"]);
  d->main_ver_jmdict(   mm["jmdict_version"]);
  d->main_ver_kanjidic( mm["kanjidic_version"]);
  d->main_ver_kradfile( mm["kradfile_version"]);
  d->main_ver_tatoeba(  mm["tatoeba_version"]);
  d->user_checked_against("UNKNOWN");
  d->cb_download( scb_download_db, (void*)this );
  d->show();
}


void App::load_config ()
{
  log("Loading config...");
  vector<string> res = query("select key,val from config;");
  for ( size_t i=0; i < res.size(); i+=2 ){
    set_config( res[i], res[i+1] );
  }
  apply_config();
}


void App::save_config ( const std::map<string,aoi_config::Config::Value> &newmap)
{
  log("Saving config...");

  typedef std::pair<string,aoi_config::Config::Value> cfg_pair;

  // merge new and old config
  for ( const cfg_pair &p: newmap )
    set_config( p.first, p.second.val );
  apply_config();

  // prepare SQL script
  std::stringstream ss;
  ss << "BEGIN TRANSACTION;\n";
  ss << "DROP TABLE IF EXISTS user.config;\n";
  ss << "CREATE TABLE user.config (key TEXT, val TEXT);\n";
  for ( const cfg_pair &p: get_config_map() )
    ss << "INSERT INTO user.config (key,val) VALUES('" 
      << p.first << "','" << p.second.val << "');\n";
  ss << "END TRANSACTION;\n";

  query(ss.str().c_str());

  // apply new fonts and colors
  init_dicview();  
}


//////////////////////////////////////////////////////////////////////////
// DictionaryInputParser

set<string> DictionaryInputParser::intersection ( const string &query, 
                                                  const set<string> &current )
{
  set<string> characters;
  // all characters in result
  for ( string &r: App::get()->query(query.c_str()) ) {
    for ( string &c: utils::str_to_chars(r.c_str()) ){
      if ( App::get()->rmn()->is_kanji(c.c_str()) )
        characters.insert(c);
    }
  }
  if ( current.empty() )
    return characters;
  // intersection
  vector<string> tmp(current.size());
  std::set_intersection( 
      current.begin(), current.end(), 
      characters.begin(), characters.end(), 
      tmp.begin() 
  );
  set<string> newset;
  newset.insert( tmp.begin(), tmp.end() );
  return newset;
}


string DictionaryInputParser::find_kanji ( const string &s, bool from_words )
{
  set<string> results;
  string q;
  for ( string &w: utils::split_string(s,",") ) {
    // search SKIP
    if ( w.size()==1 && utils::isint(w.c_str()) ){
      int i = std::stoi(w);
      if ( i > 0 && i < 5 )
        q = "select group_concat(kanji,'') from k_skip where skip1=" + w + ";";
    }
    else { 
      // search readings
      if ( from_words  ) {
        q = "select group_concat("\
          "(select group_concat(kanji,'') from d_kanji where d_reading.did=d_kanji.did)"\
          ",'') from d_reading where reading='" + App::get()->rmn()->romaji_to_hiragana(w) 
          + "';";
      }
      // search yomi 
      else{
        q = "select group_concat(kanji,'') from k_kanji where "\
          "kunyomi glob '*" + App::get()->rmn()->romaji_to_hiragana(w) + 
          "*' or onyomi glob '*" + App::get()->rmn()->romaji_to_katakana(w) + "*'";
      }
    }
    results = intersection( q, results);    
  }
  return utils::to_string(results,"");
}


void DictionaryInputParser::check_warning ( const string &s, const string &previous )
{
  size_t warning_limit = App::get()->get_config<size_t>("dic/input_parser_warning");
  size_t n = utils::str_to_chars(s.c_str()).size();
  if ( warning_ || n <= warning_limit )
    return;

  bool is_wildchar = true;
  for ( string &c: utils::str_to_chars(previous.c_str()) ){
    if ( c != "?" && c != "*" ){
      is_wildchar = false;
      break;
    }
  }
  warning_ = is_wildchar;
}


string DictionaryInputParser::parse ( const char *s )
{
  // initialize
  type_ = STRING;
  buffer_.str("");
  parts_.clear();
  warning_ = false;

  // scan string
  size_t i = 0;
  while ( i < strlen(s) ){
    switch( s[i] ){
      case '[':   add();    type_ = WORD;   break;
      case '(':   add();    type_ = KANJI;  break;
      case ']':
      case ')':   add();    type_ = STRING; break;
      default:
        buffer_ << s[i];
    }
    i++;
  }
  add();

  std::stringstream output;
  for ( size_t j=0; j < parts_.size(); ++j ){
    auto p = parts_[j];
    switch ( p.second ){
      case WORD:
      {
        string r = find_kanji(p.first,true);
        check_warning( r, (j==0) ? "":parts_[j-1].first );
        output << "[" << r << "]";
        break;
      }
      case KANJI:
      {
        string r = find_kanji(p.first,false);
        check_warning( r, (j==0) ? "":parts_[j-1].first );
        output << "[" << r << "]";
        break;
      }
      default:
        output << p.first;
    }
  }

  string ret = output.str();
  utils::replace_all(ret, "{", "[");
  utils::replace_all(ret, "}", "]");
  return ret;
}

} // namespace aoi
