#include "romanization.hxx"
#include <algorithm>
#include "utils.hxx"

using utils::replace_all;


Romanization::Romanization ()
{
  current_romaji_ = 0;
  table_ = { 
  // { {hepburn}, hiragana, katakana }
  // extended katakana
  { {"va"},    "",       "ヴァ" },    { {"va"},    "",       "ヷ"   },
  { {"tsa"},   "",       "ツァ" },    { {"fa"},    "",       "ファ" },
  { {"wi"},    "",       "ウィ" },    { {"vi"},    "",       "ヴィ" },
  { {"ti"},    "",       "ティ" },    { {"di"},    "",       "ディ" },
  { {"fi"},    "",       "フィ" },    { {"vu"},    "",       "ヴ"   },
  { {"tu"},    "",       "トゥ" },    { {"tyu"},   "",       "テュ" },
  { {"du"},    "",       "ドゥ" },    { {"dyu"},   "",       "デュ" },
  { {"fyu"},   "",       "フュ" },    { {"ye"},    "",       "イェ" },
  { {"we"},    "",       "ウェ" },    { {"ve"},    "",       "ヴェ" },
  { {"she"},   "",       "シェ" },    { {"je"},    "",       "ジェ" },
  { {"che"},   "",       "チェ" },    { {"tse"},   "",       "ツェ" },
  // hiragana wo is necessary (first occurence is used for replace)      
  { {"fe"},    "",       "フェ" },    { {"wo"},    "を",     "ウォ" },
  { {"vo"},    "",       "ヴォ" },    { {"vo"},    "",       "ヺ"   },
  { {"tso"},   "",       "ツォ" },    { {"fo"},    "",       "フォ" },
  { {"zi"},    "",       "ズィ" },    { {"tsi"},   "",       "ツィ" },
  { {"tse"},   "",       "ツェ" },    { {"tso"},   "",       "ツォ" },
  // extended katakana END                                               
	{ {"kya"},	  "きゃ",		"キャ" },		{ {"sha"},	  "しゃ",		"シャ" },
	{ {"cha"},	  "ちゃ",		"チャ" },		{ {"nya"},	  "にゃ",		"ニャ" },
	{ {"hya"},	  "ひゃ",		"ヒャ" },		{ {"mya"},	  "みゃ",		"ミャ" },
	{ {"rya"},	  "りゃ",		"リャ" },		{ {"gya"},	  "ぎゃ",		"ギャ" },
	{ {"ja"},	  "じゃ",		"ジャ" },		{ {"ja"},	  "ぢゃ",		"ヂャ" },
	{ {"bya"},	  "びゃ",		"ビャ" },		{ {"pya"},	  "ぴゃ",		"ピャ" },
	{ {"kyu"},	  "きゅ",		"キュ" },		{ {"shu"},	  "しゅ",		"シュ" },
	{ {"chu"},	  "ちゅ",		"チュ" },		{ {"nyu"},	  "にゅ",		"ニュ" },
	{ {"hyu"},	  "ひゅ",		"ヒュ" },		{ {"myu"},	  "みゅ",		"ミュ" },
	{ {"ryu"},	  "りゅ",		"リュ" },		{ {"gyu"},	  "ぎゅ",		"ギュ" },
	{ {"ju"},	  "じゅ",		"ジュ" },		{ {"ju"},	  "ぢゅ",		"ヂュ" },
	{ {"byu"},	  "びゅ",		"ビュ" },		{ {"pyu"},	  "ぴゅ",		"ピュ" },
	{ {"kyo"},	  "きょ",		"キョ" },		{ {"sho"},	  "しょ",		"ショ" },
	{ {"cho"},	  "ちょ",		"チョ" },		{ {"nyo"},	  "にょ",		"ニョ" },
	{ {"hyo"},	  "ひょ",		"ヒョ" },		{ {"myo"},	  "みょ",		"ミョ" },
	{ {"ryo"},	  "りょ",		"リョ" },		{ {"gyo"},	  "ぎょ",		"ギョ" },
	{ {"jo"},	  "じょ",		"ジョ" },		{ {"jo"},	  "ぢょ",		"ヂョ" },
	{ {"byo"},	  "びょ",		"ビョ" },		{ {"pyo"},	  "ぴょ",		"ピョ" },
	{ {"ka"},	  "か",			"カ" },	  	{ {"sa"},	  "さ",			"サ" },
	{ {"ta"},	  "た",			"タ" },			{ {"na"},	  "な",			"ナ" },
	{ {"ha"},	  "は",			"ハ" },			{ {"ma"},	  "ま",			"マ" },
	{ {"ya"},	  "や",			"ヤ" },			{ {"ra"},	  "ら",			"ラ" },
	{ {"wa"},	  "わ",			"ワ" },			{ {"ga"},	  "が",			"ガ" },
	{ {"za"},	  "ざ",			"ザ" },			{ {"da"},	  "だ",			"ダ" },
	{ {"ba"},	  "ば",			"バ" },			{ {"pa"},	  "ぱ",			"パ" },
	{ {"ki"},	  "き",			"キ" },			{ {"shi"},	  "し",			"シ" },
	{ {"chi"}, 	"ち",			"チ" },			{ {"ni"},	  "に",			"ニ" },
	{ {"hi"},	  "ひ",			"ヒ" },			{ {"mi"},	  "み",			"ミ" },
	{ {"ri"},	  "り",			"リ" },			{ {"gi"},	  "ぎ",			"ギ" },
	{ {"ji"},	  "じ",			"ジ" },			{ {"ji"},	  "ぢ",			"ヂ" },
	{ {"bi"},	  "び",			"ビ" },			{ {"pi"},	  "ぴ",			"ピ" },
	{ {"ku"},	  "く",			"ク" },			{ {"tsu"}, 	"つ",			"ツ" },
	{ {"nu"},	  "ぬ",			"ヌ" }, 		  { {"su"},	  "す",			"ス" },
  { {"fu"},	  "ふ",			"フ" },			{ {"mu"},	  "む",			"ム" },
	{ {"yu"},	  "ゆ",			"ユ" },			{ {"ru"},	  "る",			"ル" },
	{ {"gu"},	  "ぐ",			"グ" },			{ {"zu"},	  "ず",			"ズ" },
	{ {"zu"},	  "づ",			"ヅ" },			{ {"bu"},	  "ぶ",			"ブ" },
	{ {"pu"},	  "ぷ",			"プ" },			{ {"ke"},	  "け",			"ケ" },
	{ {"se"},	  "せ",			"セ" },			{ {"te"},		"て",			"テ" },
	{ {"ne"},		"ね",			"ネ" },			{ {"he"},		"へ",			"ヘ" },
	{ {"me"},		"め",			"メ" },			{ {"re"},		"れ",			"レ" },
	{ {"ge"},		"げ",			"ゲ" },			{ {"ze"},		"ぜ",			"ゼ" },
	{ {"de"},		"で",			"デ" },			{ {"be"},		"べ",			"ベ" },
	{ {"pe"},		"ぺ",			"ペ" },			{ {"ko"},		"こ",			"コ" },
	{ {"so"},		"そ",			"ソ" },			{ {"to"},		"と",			"ト" },
	{ {"no"},		"の",			"ノ" },			{ {"ho"},		"ほ",			"ホ" },
	{ {"mo"},		"も",			"モ" },			{ {"yo"},		"よ",			"ヨ" },
	{ {"ro"},		"ろ",			"ロ" },			{ {"go"},		"ご",			"ゴ" },
	{ {"zo"},		"ぞ",			"ゾ" },			{ {"do"},		"ど",			"ド" },
	{ {"bo"},		"ぼ",			"ボ" },			{ {"po"},		"ぽ",			"ポ" },
  { {"wo"},    "を",     "ヲ" },                                     
	{ {"n"},	  	"ん",			"ン" },			{ {"a"},		  "あ",			"ア" },
	{ {"i"},		  "い",			"イ" },			{ {"u"},	  	"う",			"ウ" },
	{ {"e"},		  "え",			"エ" },      { {"o"},     "お",     "オ" },
  { {"m"},		  "ん",			"ン" },        
  // special symbols                      
  { {"-"},     "ー",     "ー" },        
  /// must be at the end. Needed when parsing JMdict
  { {"we"},    "ゑ",     "ヱ" },      { {"wi"},    "ゐ",     "ヰ" },
  { {"L"},     "Ｌ",     "Ｌ" },      { {"A"},     "Ａ",     "Ａ" },
  { {"N"},     "Ｎ",     "Ｎ" },      { {"S"},     "Ｓ",     "Ｓ" },
  { {"I"},     "Ｉ",     "Ｉ" },      { {"M"},     "Ｍ",     "Ｍ" },
  { {"E"},     "Ｅ",     "Ｅ" },                                     
  { {"0"},     "０",     "０" },      { {"1"},     "１",     "１" },
  { {"2"},     "２",     "２" },      { {"8"},     "８",     "８" }
  }; // table_
  tsus_  = {"k","s","t","n","h","m","r","w","g","d","z","b","p","c"};
  tsu_k_ = "ッ"; 
  tsu_h_ = "っ";
}


string Romanization::hiragana_to_katakana ( const string &s )
{
  string str = s;
  for ( auto &elt: table_ )
    replace_all( str, elt.hiragana, elt.katakana );
  return str;
}


string Romanization::romaji_to_kana ( const string &rmj, bool to_hiragana )
{
  string  str = rmj;
  std::transform( str.begin(), str.end(), str.begin(), ::tolower);
  string  tsu = (to_hiragana) ? tsu_h_:tsu_k_;


  for ( auto &elt: table_ )
    replace_all( str, elt.romaji[current_romaji_], (to_hiragana) ? elt.hiragana:elt.katakana);

  // replace double consonants with 'tsu'
  // kocchi ->こっち (no こcち)
  for ( auto &t: tsus_ )
    replace_all ( str, t, tsu );

  // protector
  // han'i -> はんい
  // hani  -> はに
  replace_all( str, "'", "" );

  // remove small tsu from strings end
  size_t  s = str.size(), 
          t = tsu.size();
  if ( s > 2*t && str.substr( s - 2*t , s ) == tsu+tsu )
      return str.substr( 0, str.size()-1);
  return str;
}


string Romanization::kana_to_romaji ( const string &k )
{
  string str = k;

  for ( auto &elt: table_ ){
    if ( elt.hiragana != "" )
      replace_all( str, elt.hiragana, elt.romaji[current_romaji_] );
    if ( elt.katakana != "" )
      replace_all( str, elt.katakana, elt.romaji[current_romaji_]);
  }

  // remove 'tsu' from the end of the string
  if ( str.size() > 1 ){
    string last_char = str.substr(str.size()-1, str.size() );
    while( (last_char == tsu_k_) || (last_char == tsu_h_) )
      str.erase(str.size()-1);  // remove last character ( i.e. from (n-1) to the end )
  }

  // remove katakana 'tsu' ('tsu' is replaced by the next character)
  size_t pos = 0;
  while ( (pos = str.find(tsu_k_, pos)) != string::npos )
    str.replace(pos, tsu_k_.size(), 1, str.c_str()[pos+tsu_k_.size()]);

  // remove hiragana 'tsu'
  pos = 0;
  while ( (pos = str.find(tsu_h_, pos)) != string::npos )
    str.replace(pos, tsu_h_.size(), 1, str.c_str()[pos+tsu_h_.size()]);

  return str;
}
