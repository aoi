/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _GUI_SETTINGS_HXX
#define _GUI_SETTINGS_HXX

/*! \file gui_settings.hxx
* Defines the OS dependant usleep() macro and main window title.
*/

// function usleep()
#ifdef WINDOWS
	#include <windows.h>
  // Sleep(v) causes too slow GUI redraw
	#define usleep(v) Sleep(v/100) 
#else
	#include <unistd.h>
#endif

#define WIN_TITLE "Aoi (alpha.03, Build: " __DATE__ ")"


namespace aoi_ui {
  const int WIN_WIDTH       = 800;
  const int WIN_HEIGHT      = 600;
  const int FONT_KANJI      = 250;
//  const int FONT_KANJI_ONLY = 71;

}
#endif // _GUI_SETTINGS_HXX
