/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _GUI_WIDGETS_HXX
#define _GUI_WIDGETS_HXX

#include <cmath>
#include <FL/Fl.H>
#include <FL/Fl_Group.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Menu_Item.H>

#include <vector>
#include <string>

#include "datatypes.hxx"
#include "gui_settings.hxx"

/*! \file gui_widgets.hxx
* Custom widgets. Not dialos.
* \see gui_dialogs.hxx
*
*/

using std::vector;
using std::string;
using aoi::Kanji;

namespace aoi_ui{

//! A replacement for Fl_Widget::measure_label()
inline int text_height ( const char *s, int wrap_width )
{
  return ceil(fl_width(s)/wrap_width)*fl_height();
}


/*!
* Text label which stores its string.
* \todo color, font
*/
class Label : public Fl_Box
{
  private:
    string label_;    

  public:
    Label ( int x, int y, int w, int h, const char *l=0 ): Fl_Box(x,y,w,h,l){};

    inline void set ( const string &s ) {
      label_ = s;
      label(label_.c_str());
    };

    inline string get () const { return label_; };

};


/*!
* Entry (Fl_Input) which displays its label inside (if empty) and 
* ignores Ctrl-L and Ctrl-M.
* \todo better focus handling
*/
class Entry : public Fl_Input
{
  private:
    bool innerlabel_visible_ = true;
    const char *innerlabel_;
  public:
    Entry ( int x, int y, int w, int h, const char *l="Label")
      : Fl_Input(x,y,w,h,0), innerlabel_(l) 
    {
      when(FL_WHEN_ENTER_KEY_ALWAYS);
    };
    ~Entry(){};

    void draw ()
    {
      if (input_type() == FL_HIDDEN_INPUT) return;
      Fl_Boxtype b = box();
      if (damage() & FL_DAMAGE_ALL) 
        draw_box(b, color());
      if ( strlen(value()) == 0 && innerlabel_visible_ ) {
        // clear background
        fl_color(color());
        fl_rectf( x()+Fl::box_dx(b), y()+Fl::box_dy(b), 
                  w()-Fl::box_dw(b), h()-Fl::box_dh(b) );
        // draw grayed text
        fl_font(textfont(), textsize());
        fl_color(FL_GRAY);
        fl_draw( innerlabel_,
          x()+Fl::box_dx(b)+3,
          y()+h()-Fl::box_dy(b)-Fl::box_dh(b)-fl_descent() 
        );
      }
      else
        Fl_Input_::drawtext(x()+Fl::box_dx(b), y()+Fl::box_dy(b), 
                            w()-Fl::box_dw(b), h()-Fl::box_dh(b) );
    }

    //! copy content into clipboard and selection buffer
    static void scb_copy (Fl_Widget*, void *userdata) {
        Entry *in = (Entry*)userdata;
        in->copy(0);    // text selection clipboard
        in->copy(1);    // copy/paste clipboard
    }

    //! paste from selection buffer  (middle click, 0)
    static void scb_paste0 (Fl_Widget*, void *userdata) {
        Entry *in = (Entry*)userdata;
        Fl::paste(*in, 0);
    }

    //! paste from clipboard  (1)
    static void scb_paste1 (Fl_Widget*, void *userdata) {
        Entry *in = (Entry*)userdata;
        Fl::paste(*in, 1);
    }

    /*
    * Overriden handle() with context menu (copy,paste). Ignores Ctrl-M and Ctrl-L.
    */
    int handle ( int event )
    {
      unsigned int mods = Fl::event_state() & FL_CTRL;
      auto key = Fl::event_key();
      switch ( event )
      {
        case FL_PUSH:
        {
          innerlabel_visible_ = false;
          redraw();
          if ( Fl::event_button() == FL_RIGHT_MOUSE ) {
            Fl_Menu_Item menu[] = {
              { "Copy",   0, scb_copy,  (void*)this },
              { "Paste (clipboard)",  0, scb_paste1, (void*)this },
              { "Paste (selection)",  0, scb_paste0, (void*)this },
              { 0 }
            };
            const Fl_Menu_Item *m = menu->popup(Fl::event_x(), Fl::event_y(),0,0,0);
            if ( m ) 
              m->do_callback(0, m->user_data());
            return 1;
          }
          break;
        }
        case FL_KEYBOARD:
          if ( mods && (key=='m' || key=='l') ) // ignore Ctrl-M and Ctrl-L
            return 0;
        case FL_RELEASE:
          if ( Fl::event_button() == FL_RIGHT_MOUSE )
            return 1;
        case FL_UNFOCUS:
          innerlabel_visible_ = true;
          redraw();
          break;
        default:
          break;
      }
      return Fl_Input::handle(event);
    }
};


/*!
* A widget which displays information about kanji.
* \todo better linespacing
* \see KanjiPopupWindow
*/
class KanjiInfo : public Fl_Group
{
  private:
    Fl_Box *kanjibox_;
    int padding_ = 5;
    bool autoresize_ = false;
    Fl_Font font_a_ = FL_HELVETICA + FL_BOLD;
    Fl_Font font_b_ = FL_HELVETICA;
    Fl_Font font_kanji_ = FONT_KANJI;
    int size_a_ = 12;
    int size_b_ = 12;
    Kanji kanji_;
    vector<string> strings_;

    /*!
    * Helper function which draws caption and label.
    * \return height of the drawn row
    */
    inline int draw_caption_label ( string a, //!< caption
                                    string b, //!< text
                                    int xx,   //!< x-position of the label
                                    int yy,   //!< y-position of the label
                                    int wa,   //!< width of the string <i>a</i>
                                    int wb,   //!< width of the string <i>b</i>
                                    int rh    //!< row height
                                   )
    {
      if ( !b.size() )
        return 0;
      strings_.push_back(a);
      strings_.push_back(b);
      Fl_Box *bx = new Fl_Box( xx, yy, wa, rh, a.c_str() );
      bx->align(FL_ALIGN_LEFT+FL_ALIGN_INSIDE);
      bx->labelfont( font_a_ );
      bx->labelsize( size_a_ );
      bx = new Fl_Box( xx+wa+5, yy, wa, rh, b.c_str() );
      bx->align(FL_ALIGN_LEFT+FL_ALIGN_INSIDE);
      bx->labelfont( font_b_ );
      bx->labelsize( size_b_ );
      return rh;
    }

    /*!
    * Helper function which draws box with big kanji.
    * \param f      font for kanji
    * \param size   size of the kanji (px)
    * \param ss     kanji to be drawn 
    * \param xx     x-position of the box
    * \param yy     y-position of the box
    * \param ww     width of the box
    * \param hh     height of the box
    * \return height of the drawn box if <i>ss</i> not empty, else 0
    */
    inline int draw_box ( Fl_Font f, int size, string ss, int xx, int yy, int ww, int hh ){
      if ( !ss.empty() ){
        strings_.push_back(ss);
        Fl_Box *b = new Fl_Box( xx, yy, ww, hh, ss.c_str() );
        b->labelfont(f);
        b->labelsize(size);
        b->align(FL_ALIGN_INSIDE+FL_ALIGN_WRAP);
        b->size(b->w(),text_height(ss.c_str(),ww));
        return b->h();
      }
      return 0;
    }

  public:
    KanjiInfo( int x, int y, int w, int h, const char *l=0): Fl_Group(x,y,w,h,l) {};
    ~KanjiInfo(){};
    //! Sets kanji to be shown.
    void set_data ( const Kanji &k );
    //! If true then the widget changes its width and height according to its content.
    inline void autoresize ( bool b ) { autoresize_=b; };
    inline bool autoresize () const { return autoresize_; };
    //! Sets "Latin" font and size for caption (i.e. bold font).
    inline void font_a ( Fl_Font f, int size ) { font_a_=f; size_a_=size; };
    //! Sets "Latin" font and size for text.
    inline void font_b ( Fl_Font f, int size ) { font_b_=f; size_b_=size; };
    //! Sets kanji font size is derived from font_a.size.
    inline void font_kanji ( Fl_Font f ) { font_kanji_=f; };
};


/*!
* Widget which displays the grid of cells. Cells can contain one character
* and can be selected by leftclick and rightclick.
*/
class ComponentView : public Fl_Widget
{
  public:
    enum CellType { 
      CELL,               //!< normal cell
      CELL_LABEL,         //!< cell with inverted fg/bg colors; can't be selected
      CELL_HIGHLIGHTED,   //!< highlighted cell (default color: yellow)
      CELL_SELECTED_1,    //!< cell selected with leftclick (default color: pastel blue)
      CELL_SELECTED_2     //!< cell selected with rightclick (default color: salmon)
    };
    //! One cell in ComponentView
    struct Cell {
      string str;     //!< string to be shown (should be one letter)  
      CellType type;
      Cell ( const string &s, CellType t=CELL): str(s), type(t){};
    };
  private:
    vector<Cell> cells_;
    int cell_size_;
    Fl_Font font_cell_ = FL_HELVETICA;
    Fl_Font font_label_ = FL_HELVETICA;
    int fsize_cell_ = 12;
    int fsize_label_ = 12;
    Fl_Color color_highlight_ = FL_YELLOW;
    Fl_Color color_select1_   = fl_rgb_color(0x64,0x95,0xED);
    Fl_Color color_select2_   = fl_rgb_color(0xe9,0x96,0x7a);
    int rows_;
    int cols_;

    void set_cellsize ();

  public:
    ComponentView ( int x, int y, int w, int h, const char *l=0 );
    ~ComponentView(){};

    int handle ( int event );
    void draw ();

    //! Set Cells to be drawn.
    void set_data ( const vector<Cell> &d={});
    //! Set Cells (by their Cell::<i>str</i>) to be highlighted.
    void set_highlight ( const string &cell_str );
    //! Set default font for cells.
    void font_cell ( Fl_Font f=FL_HELVETICA, int s=16 );
    //! Set default font for labels. \see CellType
    void font_label ( Fl_Font f=FL_HELVETICA, int s=16 );
    //! Returns  cells selected by leftclick.
    inline vector<string> selected1 () { return cells_by_type(CELL_SELECTED_1); };
    //! Returns  cells selected by rightclick.
    inline vector<string> selected2 () { return cells_by_type(CELL_SELECTED_2); };
    //! Return all cells of the specified type <i>t</i>.
    vector<string> cells_by_type ( CellType t );
};

} // namespace aoi_ui
#endif //_GUI_WIDGETS_HXX
