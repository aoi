#ifndef _ROMANIZATION_HXX
#define _ROMANIZATION_HXX

#include <string>
#include <vector>
#include "utils.hxx"

/*! \file romanization.hxx
* \todo refactor everything
* \note http://www.unicode.org/charts/unihangridindex.html
* \note <b>ic_cjk*()</b> functions: https://en.wikipedia.org/wiki/CJK_Unified_Ideographs#CJK_Unified_Ideographs
*/


using std::string;
using std::vector;

/*!
* Class responsible for converting romaji to hiragana/katakana.
* Uses wapuro- style romanization (currently mostly Hepburn).
* \todo kunrei-shiki and nihon-shiki
*/
class Romanization
{
  public:
    Romanization();
    ~Romanization(){};
    
    //! One element in romanization table.
    struct RmnElement {
      vector<string> romaji;  
      string hiragana;
      string katakana;
    };
    inline static bool is_cjk_base (unsigned int ucs) {return ucs>=0x4e00 && ucs<=0x9fff;}
    inline static bool is_cjk_a    (unsigned int ucs) {return ucs>=0x3400 && ucs<=0x4dbf;}
    inline static bool is_cjk_b    (unsigned int ucs) {return ucs>=0x20000 && ucs<=0x2a6df;}
    inline static bool is_cjk_c    (unsigned int ucs) {return ucs>=0x2a700 && ucs<=0x2b73f;}
    inline static bool is_cjk_d    (unsigned int ucs) {return ucs>=0x2b740 && ucs<=0x2b81f;}
    // range U+1B000 - U+1B0FF is both hiragana ans katakana 
    inline static bool is_hiragana (unsigned int ucs) {return ucs>=0x3040 && ucs<=0x309f;}
    inline static bool is_katakana (unsigned int ucs) {return ucs>=0x30a0 && ucs<=0xffef;}
    inline static bool is_kana     (unsigned int ucs) {return is_hiragana(ucs) || is_katakana(ucs);}
    inline static bool is_cjk      (unsigned int ucs) 
      {return is_cjk_base(ucs) || is_cjk_a(ucs) || is_cjk_b(ucs) || is_cjk_c(ucs) || is_cjk_d(ucs);}


    string romaji_to_kana ( const string &rmj, bool to_hiragana = true );
    inline string romaji_to_kana ( const char *rmj, bool to_hiragana = true ) 
      { return romaji_to_kana(string(rmj),to_hiragana); };

    inline string romaji_to_hiragana ( const char *s ) 
      { return romaji_to_kana(string(s),true);  };
    inline string romaji_to_hiragana ( const string &s ) 
      { return romaji_to_kana(s,true);  };

    inline string romaji_to_katakana ( const char *s ) 
      { return romaji_to_kana(string(s),false); };
    inline string romaji_to_katakana ( const string &s ) 
      { return romaji_to_kana(s,false); };

    inline string hiragana_to_katakana ( const string &s );

    string kana_to_romaji ( const string &k );
    
    //! Returns true if at least one letter in <i>s</i> is a kanji.
    static inline bool contains_kanji ( const char *s )
    { 
      for( unsigned int code: utils::utf8_to_ints(s) )
        if ( is_cjk(code) )
          return true;
      return false;
    };

    //! Returns true if all letters in <i>s</i> are kanji.
    static inline bool is_kanji ( const char *s )
    { 
      for ( unsigned int code: utils::utf8_to_ints(s) )
        if ( !is_cjk(code) )
          return false;
      return true;
    };

    static inline bool is_kanji ( int code ) { return is_cjk(code);};

  private:
    vector<RmnElement> table_;
    size_t current_romaji_;
    string tsu_h_;
    string tsu_k_;
    vector<string> tsus_;

};


#endif // _ROMANIZATION_HXX
