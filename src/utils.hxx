/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __UTILS_HXX
#define __UTILS_HXX

#include <cmath>
#include <algorithm>
#include <sstream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <FL/fl_utf8.h>
#include <exception>
#include <stdexcept>
#include <set>
#include <map>
#include <zlib.h>

/*! \file utils.hxx
* \note http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&item_id=IWS-Appendix
* \note http://home.telfort.nl/~t876506/utf8tbl.html
*/


using std::vector;
using std::string;

namespace utils {

/*! 
* Max. length of tagname in parse_markup()
* \note should be enough for now (tags without arguments)
*/
const int MAX_TAGNAME_LENGTH = 32;

class CantOpenFile : public std::exception {
  private:
    string msg_;
  public:
    CantOpenFile(const string &filename): msg_(filename) {};
    const char *what() const noexcept {return msg_.c_str();};
};

class ParsingError : public std::exception {
  private:
    string msg_;
  public:
    ParsingError(const string &filename): msg_(filename) {};
    const char *what() const noexcept {return msg_.c_str();};
};


//! Histogram. Counts inserted items. Returns items sorted by their key or counts.
template <class T>
class Histogram
{
  private:
    std::map<T,int> map_;
  public:
    Histogram () {};
    ~Histogram () {};
  
    //! Reset the histogram.
    inline void clear () { map_.clear(); }

    //! Add an item to the histogram.
    inline void add ( const T &item ){
      if ( map_.find(item) == map_.end() )
        map_[item] = 1;
      else
        map_[item]++;
    }

    //! Add vector of items to the histogram.
    inline void add ( const vector<T> &v ) {
      for ( T item: v )
        add(item);
    }

    //! Returns items sorted by the key.
    inline std::map<T,int> map () { return map_; }

    //! Returns items sorted by their count (ascending order).
    inline std::multimap<int,T> sorted () {
      std::multimap<int,T> mm;
      for ( auto mi: map_ )
        mm.insert( {mi.second,mi.first} );
      return mm;
    }
};

/*!
* Finds first digit (0-9) in input string.
* \param s input string
* \return position of first digit in string or length of the string if no digit found
*/
inline size_t find_first_digit ( const char *s )
{
  size_t pos = 0;
  while ( pos < strlen(s) ){
    if ( s[pos] > '0' && s[pos] < '9' )
      break;
    pos++;
  } 
  return pos;
}


/*!
* Groups similar string in input vector. Similar strings means strings,
* which ends with different number.
* <b>Example: </b><br>
*   { "a 1", "d", "b 3", "ddd", "a 4", "b 2" }<br>-><br>
*   { "a 1", "a 4" }, { "b 2", "b 3" }, { "d" }, { "ddd" }
* \param v input vector of strings
* \return alphabetically sorted vector of groupped strings
*/
vector<vector<string>> separate_groups ( vector<string> &v );


/*!
* Checks ZLIB error code. In case of an error throws  exception with
* description of the error.
* \exception std::runtime_error
* \param err ZLIB error code
*/
void check_zlib_error ( int err );


/*!
* Decompress gzip file, copies file if it is not gzipped.
* Throws exception if cant open file. Calss check_zlib_error().
* \exception std::runtime_error
* \param infilename input file name
* \param outfilename output file name
*/
void gzip_decompress_file( const char *infilename, const char *outfilename );


/*!
* Parse string containing integer range and returns minimum and maximum.
* Range is separated by -. 
* <b>Examples</b>
* 5+    5 or more
* 3-    3 or less
* 4-8   between 4 and 8
* Minimal acceptable value is 0. maximal 99.
* \param s string containing integer range
* \return { minimal_value, maximal_value }
*/
inline std::pair<int,int> parse_range ( const string &s )
{
  string min = "0";
  string max = "99";
  if ( !s.empty() ){
    size_t pos = s.find("-");
    if ( s.back() == '+' )
      min = s.substr( 0, s.size()-1 );
    else if ( s.back() == '-' )
      max = s.substr( 0, s.size()-1 );
    else if ( pos != string::npos ){
      min = s.substr(0, pos);
      max = s.substr( pos+1, s.size() );
    }
    else {
      min = s;
      max = s;     
    }
  }
  return {std::stoi(min),std::stoi(max)};
}


/*!
* Tries to guess the filename from an url. Filename is created as substring from url,
* beginning after position of the last /.
* \param url 
* \return guessed filename or "downloaded.file" when guess failed
*/
inline string guess_fname_from_url ( const string &url )
{
  size_t pos = url.find_last_of("/")+1;
  if ( pos != string::npos )
    return url.substr( pos );
  else
    return "downloaded.file";
}


/*!
* Convenient function for finding element in the vector.
* Equivalent to:
* <pre>return std::find( v.begin(), v.end(), elt ) != v.end();</pre>
*/
template <class T>
inline bool is_in ( const vector<T> &v, const T &elt  ){
  return std::find( v.begin(), v.end(), elt ) != v.end();
}


/*!
* Check whether string contains an positive integer.
* \return true if s contains only 0-9, false otherwise
*/
inline bool isint ( const char *s )
{
  for ( size_t i=0; i<strlen(s); i++ )
    if ( s[i] < '0' || s[i] > '9' )
      return false;
  return true;
}


/*!
* Reads the string s until character end is found (or \0 until is reached).
* Throws an exception if the string is invalid.
* \exception ParsingError
* \param s [in] input string
* \param pos [in,out] starting position in the s, pos is modified in the process 
*             (and contains position of the <b>end</b>)
* \param end the input string is read until this character is reached
* \return substring starting at input pos and ending before character <b>end</b>
*/
inline string read_until ( const char *s, size_t *pos, const char end )
{
  char buff[strlen(s)]; // should be enough
  size_t j = 0;
  size_t i = *pos;
  while ( s[i] != end && s[i] != '\0'  )
    buff[j++] = s[i++]; 
  if ( s[i] != end )
    throw ParsingError("read_until(): Invalid string: " + string(s));
  *pos = ++i;
  buff[j] = '\0';
  return buff;
}


/*!
* \sa parse_markup()
*/
struct TextTag
{
  string tag; //!< name of the tag
  int pos;    //!< position in the raw string (returned by parse_markup())
  int len;    //!< length of the tagged text
  int id;
  //! for std::sort
  bool operator< ( const TextTag &rhs ) const { return (this->pos < rhs.pos); };
};


/*!
* Parses string with simple markup (without attributes and overlapping).
* Example: This is an <tag1>example</tag1> string with <tag2>markup</tag2>.
* \sa TextTag
* \return String without tags and vector of TextTags.
*/
std::pair<string,vector<TextTag>> parse_markup ( const char *s );


//! Checks whether file path existst (i.e. can be open).
inline bool file_exists ( const char *path )
{
  std::ifstream f;
  f.open(path);
  bool ret = f.is_open();
  f.close();
  return ret;
}

inline bool file_exists ( const string &s ) { return file_exists(s.c_str()); }


/*!
* Replaces all accurences of the string <i>match</i> in <i>s</i> by <i>repl</i>.
* Modifies input string.
* \note Dont work with non-ASCII characters (at least with current libstdc++).
* \todo utf8
*/
inline void replace_all ( string &s, const string &match, const string &repl )
{
  size_t pos = 0;
  while ( (pos = s.find(match, pos)) != string::npos )
    s.replace(  pos, match.size(), repl );
}


/*!
* Strips whitespace from the start and end of the input string.
* \param s string to be stripped
* \return copy of the stripped input string
*/
inline string strip ( const char *s )
{
  size_t start  = 0;
  size_t end    = strlen(s);
  while ( isspace(s[start]) ) start++;
  while ( isspace(s[end-1]) && end > start ) end--;
  return string(s, start, end-start);
}


/*!
* Joins elements of the input vector into string with separator sep.
* <b>Example:</b>
*   vector<string> v = { "a", "b", "c" }
*   to_string(v, ":") returns "a:b:c"
* \param v input vector
* \param sep string to be used as separator between elements of v in resulting string
* \return joined string
*/
template <class T=string>
inline string to_string ( const vector<T> &v, const char *sep=", " )
{
  if ( v.empty() ) return string("");
  std::stringstream ss;
  size_t i = 0;
  for ( auto &s: v )
    ss << s << ( ( i++ < v.size()-1 ) ? sep:"" );
  return ss.str();
}


//! Joins elements of the input set into string. See above.
template <class T=string>
inline string to_string ( const std::set<T> &s, const char *sep=", " )
  { return to_string<T>( vector<T>(s.begin(),s.end()), sep );  }


/*!
* Splits string s by delimiters. Delimiters are same as in strtok().
*/
vector<string> split_string ( const string &s, const char *delimiters=" " );


//! Splits string of integers into vector. See split_string()
vector<int> split_string_int ( const string &s, const char *delimiters=" " );


/*!
* Splits string to chars. E.g. 'hello' -> 'h','e','l','l','o'
* \todo check & process errors
*/
vector<string> str_to_chars ( const char *s );


/*!
* Splits string s to chars and returns vector of their utf8 codes.
*/
inline std::vector<unsigned int> utf8_to_ints ( const char *s )
{
  std::vector<unsigned int> v;
  size_t i=0;
  while ( i < strlen(s) ){
    int len = fl_utf8len(s[i]);;
    if ( len > 0 ){
      int dlen = 0;
      unsigned int dec = fl_utf8decode( s+i, s+i+len ,&dlen);
      if ( len != dlen )
        printf("UTF8 decoding error: i: %d, len: %d, dlen: %d", i, len, dlen);
      v.push_back( dec );
      i += len;
    }
    else
      i++;
  }
  return v;
}

} // namespace utils
#endif // __UTILS_HXX
