/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _AOI_HXX
#define _AOI_HXX

/* \file aoi.hxx
* Contains App - the main class of the application.
*/

#include <cmath>
#include <vector>
#include <string>
#include <set>

#include "logger.hxx"
#include "datatypes.hxx"
#include "utils.hxx"
#include "romanization.hxx"
#include "config.hxx"
#include "sqlite3.hxx"
#include "gui.hxx"
#include "parsers.hxx"
#include "gui_dicview.hxx"
#include "gui_kanjiview.hxx"


using std::vector;
using std::string;
using std::set;

namespace aoi_ui {
  class GUI;
}

namespace aoi {

/*!
* \sa cb_kanji_search
*/
enum KANJI_SORT_MODE { 
  SORT_FREQ_ASC = 0, 
  SORT_FREQ_DESC = 1, 
  SORT_STROKES_ASC = 2, 
  SORT_STROKES_DESC = 3 
};


/*!
* One item (line) in listview (dictionary results).
* \sa cb_filter_listview()
*/
struct DicViewItem {
  int did;
  set<string> pos;
  // vector<string> flags;
  string reading;
  string kanji;
  string sense;
  DicViewItem( int id, const set<string> &p, const string &r, 
                const string &k, const string &s )
    : did(id), pos(p), reading(r), kanji(k), sense(s) {};
};



/*!
* Main class of the program. <b>Singleton</b>. Manages database, logger, 
* config and UI. Provides functions for UI's callbacks.
* \note This should be only class to call query()
* \todo when calling ui_* functions check whether the ui_ pointer is valid
*/
class App
{
  private:
    class SQLite3::SQLite3 *db_;
    class Romanization *rmn_;
    class aoi_ui::GUI *ui_;
    class aoi_config::Config *cfg_;
    class Logger logger_;
    vector<DicViewItem> listview_items_; 
    map<string,int> components_;  //!< component: stroke_count
    std::multimap<int,string> curr_components_; //!< frequency:component
    // singleton
    App();
    App& operator=(App const&) = delete;
    static App* instance_;

    /*! 
    * Returns part of the SQL query for DicView.
    * \param id database id (did) of the searched word
    * \sa parse_dic_input()
    */
    inline string q_reading ( const string &id="")
    {
      string s1 = id.empty() ? "":"(select "; 
      string s2 = id.empty() ? "":(" from d_reading where d_reading.did="+id+") ");
      return s1 + "group_concat("\
        "(case freq when 1 then '<reading_freq>'||reading||'</reading_freq>' "\
        "else '<reading>'||reading||'</reading>' end)||"\
        "'<rinf>'||inf||'</rinf>',"\
      "\"<br/>\")" + s2 + "as reading, ";
    }

    /*! 
    * Returns part of the SQL query for DicView.
    * \param id database id (did) of the searched word
    * \sa parse_dic_input()
    */
    inline string q_kanji ( const string &id="" )
    {
      string s1 = id.empty() ? "":"(select "; 
      string s2 = id.empty() ? "":(" from d_kanji where d_kanji.did="+id+") ");
      return s1 + "group_concat("\
        "(case freq when 1 then '<kanji_freq>'||kanji||'</kanji_freq>' "\
        "else '<kanji>'||kanji||'</kanji>' end)||"\
        "'<kinf>'||inf||'</kinf>',"\
      "\"<br/>\")" + s2 + "as kanji, ";
    }

    /*! 
    * Returns part of the SQL query for DicView.
    * \param id database id (did) of the searched word
    * \sa parse_dic_input()
    */
    inline string q_sense ( const string &id="" ) 
    {
      string s1 = id.empty() ? "":"(select "; 
      string s2 = id.empty() ? "":(" from d_sense where d_sense.did="+id+") ");
      return s1 + "group_concat("\
        "(case pos when '' then '' else '<pos>'||pos||'</pos>' end)||"\
        "(case misc when '' then '' else '<misc>'||misc||'</misc>' end)||"\
        "gloss||"\
        "(case field when '' then '' else '<field>'||field||'</field>' end)||"\
        "(case dial when '' then '' else '<dial>'||dial||'</dial>' end),"\
      "'<sep/>') " + s2 + " as sense ";
    }

  public:
    ~App();

    //! Returns instance of App.
    static inline App *get (){ 
      if ( !instance_) 
        instance_ = new App();
      return instance_; 
    };

    //! Returns pointer to Romanization.
    inline Romanization *rmn () const { return rmn_; };

    //! Opens the main database and attach the user database.
    void open_database ();

    //! Log message.
    inline void log   ( const string &s ) { logger_.msg(s); };
    //! Log error.
    inline void log_e ( const string &s ) { logger_.msg(s,Logger::MSG_ERROR); };
    //! Log warning.
    inline void log_w ( const string &s ) { logger_.msg(s,Logger::MSG_WARNING); };
    //! Log debug message.
    inline void log_d ( const string &s ) { logger_.msg(s,Logger::MSG_DEBUG); };
    //! Log message.
    inline void log   ( const std::stringstream &s ) { logger_.msg(s); };
    //! Log error.
    inline void log_e ( const std::stringstream &s ) 
      { logger_.msg(s,Logger::MSG_ERROR); };
    //! Log warning.
    inline void log_w ( const std::stringstream &s ) 
      { logger_.msg(s,Logger::MSG_WARNING); };
    //! Log debug message.
    inline void log_d ( const std::stringstream &s ) 
      { logger_.msg(s,Logger::MSG_DEBUG); };

    //! Returns pointer to UI.
    inline aoi_ui::GUI *ui() const { return ui_; };

    //! Call UI::run(), which calls Fl::run()
    int run ( int argc, char **argv );

    /*!
    * Performs database query q.
    * \param q SQLite query
    * \param log_query true: q will be logged as debug message
    * \param replace_separator true: parsers::SEPARATOR_SQL ('|') 
    *         will be replaced by ", "
    * \sa SQLite3::query()
    * \returns data in same format as SQLite3::query()
    */
    vector<string> query ( const char *q, bool log_query=true, 
                            bool replace_separator = true );

    //! Gets one kanji from database.
    Kanji db_get_kanji ( const string &kanji );

    //! Gets one word(record) from database
    DicWord db_get_word( int did );

    //! Returns whole config as map.
    inline std::map<string,aoi_config::Config::Value> get_config_map ()
      { return cfg_->get_map(); };
    //! Returns default config map.
    inline std::map<string,aoi_config::Config::Value> get_config_map_default ()
      { return cfg_->get_default_map(); };
    /*! 
    * Sets one key=value config pair.
    * \sa apply_config()
    */
    inline void set_config ( const string &key, const string &val ) 
      { cfg_->set( key, val ); };

    /*!
    * Overrides one config option. Overidden option cant be changed until restart
    * of the program.
    * \sa Config::set_override()
    */
    inline void config_override ( const string &key, const string &val ) { 
      try{ 
        cfg_->set_override(key,val); 
      }
      catch ( const std::exception &e ) {
        log_e("Unknown config: " + key);
      }
     };

    /*!
    * Load color from database (string 0xRRGGBB00) and converts it to Fl_Color
    * (unsigned int);
    */
    inline Fl_Color get_color ( const string &color ){
      string s = cfg_->get<string>("color/"+color);
      std::stringstream ss;
      ss << std::hex << s.substr(2);
      Fl_Color c;
      ss >> c;
      return c;
    }

    /*!
    * Applies config - sets colors, loglevel, fonts, ... Redraws UI.
    * \sa set_config()
    */
    void apply_config ();
    /*! 
    * Gets one config item
    * \exception std::runtime_error when key does not exist
    */
    template<class T=string> inline T get_config( const string &s ) 
      { return cfg_->get<T>(s); }
    /*!
    * Writes current config to database. Calls set_config() for each 
    * item in config map. After that calls init_dicview()
    * \sa get_config_map()
    * \sa set_config()
    * \sa init_dicview()
    * \todo check new keys
    */
    void save_config ( const std::map<string,aoi_config::Config::Value> &newmap={});

    /*! 
    * Loads config from database.
    * \todo Compare loaded and default config. Drop keys which exists in loaded
    *       but dont in default (i.e. keys from previous versions).
    */
    void load_config ();

    /*!
    * Initializes and sets styles in DicView.
    * \sa DicView
    * \sa TextStyle
    * \sa GUI::register_tag_dicview()
    */
    void init_dicview ();

    //! Shows alert/error window.
    void alert ( const string &msg, const string &desc="" );

    /*!
    * Checks tables in database. Creates missing.
    * \sa aoi_config::db_tables
    */
    void check_tables ();
    /*!
    * Checks indexes in database. Creates missing indexes and runs 
    * VACUUM if neccessary.
    * \sa aoi_config::db_tables
    */
    void check_indexes ();

    /*!
    * Loads database stored as SQLite script. File may be GZipped.
    */
    void load_dbscript ( const char *fname );
    inline void load_dbscript ( const string &fname ){ load_dbscript(fname.c_str());};

    /*!
    * Calls input parser. Resets GUI (pos chekboxes).
    */
    void cb_dic_input ();

//    void on_dic_selected ( int id );

    /*!
    * Shows dialog which allows editing of the currently selected word
    * (i.e. row in DicView)
    */
    void cb_edit_word ();

    /*!
    * Parses the content of dic_input_, builds and performs the query 
    * to the database.
    * \see DictionaryInputParser
    * \see q_sense(), q_reading(), q_kanji()
    */
    void parse_dic_input ( const char *str );

    /*!
    * Gets kanji from database and calls GUI::popup_kanji()
    */
    void cb_popup_kanji ( const string &kanji );
    void cb_kanji_search ();

    /*!
    * Sets contents of the listview and listview_items_.
    * \parameter v preformatted text (output from query()) to be used 
    *   in aoi_ui::DicView  
    * \todo nokanji
    */
    void set_listview ( const vector<string> &v );

    /*!
    * Shows a context menu that contains kanji which appears in the selected 
    * word and items: Examples, Notes and Edit word.
    * \see GUI::dicview_menu()
    */
    void cb_dicview_rightclick ( int did );

    /*!
    * Shows a dialog with example sentences. Sentances marked as good example
    * for the word are displayed first and in the color.
    */
    void cb_examples ();

    /*!
    * Shows a dialog that allows managing the DB and downloading one.
    */
    void cb_manage_db ();

    /*!
    * Filters visible rows in the listview according to selected checkboxes.
    */
    void cb_filter_listview ();

    /*!
    * Builds a histogram of the components in currently visible kanjis and 
    * sets the content of ComponentView.
    */
    void cb_set_components ();

    /*!
    * Downloads prebuilt (gzipped)  database from aoi.souko.cz.
    */
    void cb_download_db();

    /*!
    * Used as callback for download button in aoi_ui::ManageDBDialog::cb_download()
    * \see cb_manage_db() (here it is set as callback)
    * \see cb_download_db()
    */
    inline static void scb_download_db ( Fl_Widget *w, void *p )
      { ((App*)p)->cb_download_db(); }
    //! See cb_examples()
    inline static void scb_examples ( Fl_Widget *w, void *p )
      { ((App*)p)->cb_examples(); }
    //! See cb_dic_input()
    inline static void scb_dic_input ( Fl_Widget *w, void *p )
      { ((App*)p)->cb_dic_input(); }
    //! See cb_filter_listview()
    inline static void scb_filter_listview ( Fl_Widget *w, void *p )
      { ((App*)p)->cb_filter_listview(); }
    //! See cb_manage_db()
    inline static void scb_manage_db ( Fl_Widget *w, void *p )
      { ((App*)p)->cb_manage_db(); }
    //! See cb_kanji_search()
    inline static void scb_kanji_search ( Fl_Widget *w, void *p )
      { ((App*)p)->cb_kanji_search(); }
    /*!
    * 
    * \todo siplify and unite with GUI::popup_kanji
    * \todo change to direct kanjisearch
    */
    inline static void scb_kanjiview_select ( Fl_Widget *w, void *p ){ 
      aoi_ui::KanjiView *v = (aoi_ui::KanjiView*)w;
      aoi_ui::KanjiView::Cell *c = v->selected();
      if ( c ){
        ((App*)p)->cb_popup_kanji( c->str ); 
        // copy to selection buffer (middle mouse)
        Fl::copy(c->str.c_str(), strlen(c->str.c_str()), 0);
      }
    }
    /*!
    * Static callback for doubleclick in dicview.
    * \see cb_edit_word()
    */
    inline static void scb_dicview_doubleclick ( Fl_Widget *w, void *p ){ 
      ((App*)p)->cb_edit_word(); }
    /*!
    * \see cb_edit_word()
    */
    inline static void scb_edit_word ( Fl_Widget *w, void *p ){ 
      ((App*)p)->cb_edit_word(); }

    /*!
    * Calls back DicViews' callback for rightclick.
    * \todo simplify, clarify
    */
    inline static void scb_dicview_rightclick ( Fl_Widget *w, void *p ){ 
      aoi_ui::DicView *v = (aoi_ui::DicView*)w;
      ((App*)p)->cb_dicview_rightclick( v->selected_row_id() );
    }
    //! See cb_set_components()
    inline static void scb_set_components ( Fl_Widget *w, void *p )
      { ((App*)p)->cb_set_components(); }
};


/*!
* Parses string in the dictionary's input fiels.
* Breaks the input string into the parts ( parts_ ) and build query.
* Part = content of one {}, [] or ().
* \see App:parse_dic_input()
*/
class DictionaryInputParser
{
  public:
    enum PartType { STRING, WORD, KANJI };
  private:
    vector<std::pair<string,PartType>> parts_;
    std::stringstream buffer_;
    PartType type_ = STRING;
    bool warning_ = false;

    /*!
    * Adds content of the buffer_ to parts_ and sets its type (called after 
    * encountering the start of a new part or end of string).
    * \see parse()
    */
    inline void add () {
      if ( buffer_.str().empty() ) return;
      parts_.push_back( { buffer_.str(), type_ } );
      buffer_.str("");
    }

    /*!
    * Performs a kanji query, extracts all kanji from the result to the new set 
    * and intersects it with the set <i>current</i>. Called by find_kanji().
    * \returns result of the intersection
    */
    set<string> intersection ( const string &query, const set<string> &current );

  public:
    DictionaryInputParser(){};
   ~DictionaryInputParser(){};

    /*! 
    * Returns whether the caller should display a warning about too broad search.
    * \see check_warning(), Config::"dic/input_parser_warning"
    */
    inline bool warning () const { return warning_; };

    /*!
    * Parses the text in [] or (). Does some db queries (based on SKIP, 
    * reading of the word or on/kun).
    * \param from_words If true then [] else ().
    * \see intersect()
    * \returns string consisting from all the matching kanji
    */
    string find_kanji ( const string &s, bool from_words=true );

    /*!
    * Checks whether the flag warning_ should be set.
    * \see warning_()
    */
    void check_warning ( const string &s, const string &previous );

    /*!
    * Splits the input string into parts, identifies their types and
    * \return list of matching kanji in the curly brackets (GLOB like)
    */
    string parse ( const char *s );
};

} // namespace aoi
#endif // _AOI_HXX
