/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _KANJIVIEW_HXX
#define _KANJIVIEW_HXX

#include <FL/Fl_Table.H>
#include <FL/fl_draw.H>
#include <map>
#include <string>
#include <vector>

#include "gui_settings.hxx"

/*! \file gui_kanjiview.hxx
* PREDEFINED TAGS:
*   <br>
*   <sep>
*   <default>
*
* \warning FL_FLAT_BOX causes errors in redrawing
*/


namespace aoi_ui {

using std::vector;
using std::string;

/*!
* Widget that displays grid with cells. Each cell contains one big kanji.
* No. of columns is determined by the cells and widgets width.
* Cells are squares.
*/
class KanjiView : public Fl_Table
{
  public:
    /*!
    * One cell in KanjiView.
    */
    struct Cell {
      public:
        string str;
        bool selected = false;
        Fl_Color fgcolor;
        Fl_Color bgcolor;
        Cell ( const string &s, int fg=-1, int bg=-1 ) : str(s)
        {
          fgcolor = (fg!=-1) ?fg:FL_FOREGROUND_COLOR;
          bgcolor = (bg!=-1) ?bg:FL_BACKGROUND2_COLOR;
        }
    };

  private:
    Fl_Font font_         = FONT_KANJI;   //!< font to be used
    int font_size_        = 12;           //!< default font size
    Fl_Color selection_color_ = FL_LIGHT2;
    int cell_padding_     = 5;
    int cell_size_        = 15;  

    Fl_Callback *cb_leftclick_;
    Fl_Callback *cb_rightclick_;
    Fl_Callback *cb_select_;
    void *cb_leftclick_data_      = nullptr;
    void *cb_rightclick_data_     = nullptr;
    void *cb_select_data_         = nullptr;
    vector<Cell> data_;                 //!< all cells

    //! Clear selection and redraw.
    inline void clear_selection () { 
      for ( Cell &c: data_ ) 
        c.selected=false; 
      redraw(); 
    }
    /*! 
    * Draws the content of a single cell.
    */
    void draw_single_cell ( int R, int C, int X, int Y, int W, int H );
    /*!
    * Draws cell or adjust n. of visible columns depending on the context.
    */
    void draw_cell ( TableContext context, int R=0, int C=0, int X=0, int Y=0, 
                     int W=0, int H=0 );

  public:
    KanjiView(int x, int y, int w, int h, const char *l=0 );
    ~KanjiView(){};


    //! Clears the view and sets tha data.
    void set_data ( const vector<Cell> &d );

    //! Sets selected flag for cell <i>id</i> and redraws.
    inline void select_cell ( int id ){
      clear_selection();
      data_.at(id).selected = true;
      redraw();
    }

    //! Sets selected flag for cell <i>c</i> and redraws.
    inline void select_cell ( Cell *c ){
      clear_selection();
      c->selected = true;
      redraw();
    }

    //! Returns the cell at row <i>R</i> and column <i>C</i>
    inline Cell *cell_at_rc ( int R, int C ){
      int ret = R*cols()+C;
      if ( R == -1 || C == -1 || ret > int(data_.size())-1 )
        return nullptr;
      return &data_[ret];
    }

    //! Returns the cell at position x,y
    inline Cell *cell_at_xy ( int x, int y )
    {
      int R = -1;
      int C = -1;
      int w = 0;
      int h = 0;
      // check rows
      for ( int r=0; r < rows(); r++ ){
        int nh = h + row_height(r);
        if ( y>h && y<nh ){
          R = r;
          break;
        }
        h = nh;
      }
      // check columns
      for ( int c=0; c < cols(); c++ ){
        int nw = w + col_width(c);
        if ( x>w && x<nw ) {
          C = c;  
          break;
        }
        w = nw;
      }
      int ret = R*cols()+C;
      if ( R == -1 || C == -1 || ret > int(data_.size())-1 )
        return nullptr;
      return &data_[ret];
    }
    

    //! Return (first) selected cell or  nullptr if none selected.
    inline Cell *selected () {
      for ( Cell &c: data_ )
        if ( c.selected )
          return &c;  
      return nullptr;
    }
    
    //! Returns font size
    inline int font_size () const { return font_size_;};

    //! Retuns selection color
    inline Fl_Color selection_color () const { return selection_color_; };

    //! Returns cell padding
    inline int cell_padding () const { return cell_padding_; };

    //! Return cell size (cells are squares).
    inline int cell_size () const { return cell_size_; };
    inline int selected_id () const {
      for ( size_t i=0; i<data_.size(); i++)
        if ( data_.at(i).selected ) 
          return i;
      return -1;
    }

    //! Sets the base text size, resizes the cells accordingly.
    void font_size ( int s );

    //! Sets the selection color
    inline void selection_color ( const Fl_Color &c ) { selection_color_ = c; };

    //! Sets the cell padding
    inline void cell_padding ( int x ) { cell_padding_ = x; };

    //! Sets the size of cells
    inline void cell_size ( int w=-1 ) { 
      if ( w<1 ) return;
      cell_size_ = w;
      col_width_all(w);
      row_height_all(w);
    }
  
    int handle ( int event );

    /*! 
    * Copies the text <i>userdata</i> into clipboard and the selection
    * buffer (Unix). Userdata should be const char*.
    */
    static void scb_copy ( Fl_Widget *w, void *userdata ) {
      const char *d = (const char*)userdata;
      int len = strlen(d);
      Fl::copy( d, len, 0);
      Fl::copy( d, len, 1);
    }

    /*! 
    * Sets the callback for leftclick. <i>data</i> should be pointer
    * to the master widget (App).
    */
    inline void cb_leftclick  ( Fl_Callback *cb, void *data ) 
      { cb_leftclick_  = cb; cb_leftclick_data_ = data; }

    /*! 
    * Sets the callback for rightclick. <i>data</i> should be pointer
    * to the master widget (App).
    */
    inline void cb_rightclick ( Fl_Callback *cb, void *data ) 
      { cb_rightclick_ = cb; cb_rightclick_data_ = data; }

    /*! 
    * Sets the callback for the selection of a row. 
    * <i>data</i> should be pointer to the master widget (App).
    */
    inline void cb_select     ( Fl_Callback *cb, void *data ) 
      { cb_select_     = cb; cb_select_data_ = data; }

    //! Calls the callback for the leftclick
    inline void cb_leftclick  ( Fl_Widget *w ) const
      { cb_leftclick_(w,cb_leftclick_data_); }
    //! Calls the callback for the rightclick
    inline void cb_rightclick ( Fl_Widget *w ) const 
      { cb_rightclick_(w, cb_rightclick_data_); }
    //! Calls the selection of a row
    inline void cb_select     ( Fl_Widget *w ) const 
      { cb_select_(w,cb_select_data_); }

    //! default (empty) callback
    inline static void scb_general ( Fl_Widget *w, void *f ) {}
};

} //namespace aoi_ui
#endif // _KANJIVIEW_HXX
