/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config.hxx"
#include "logger.hxx"

namespace aoi_config {

Config::Config ()
{
  // DEFAULTS
  // logging
  data_["log/level"]            = {"message", 
    "Possible values: none, message, warning, error, debug", STRING};
  // database
  data_["db/file_main"]         = {"main.db","Name of the main database file.", STRING};
  data_["db/file_user"]         = {"user.db","Name of the user database file.", STRING};
  // fonts
  data_["font/base_size"]       = {"14",
    "Base size of the text in pixels. (Most other size is based on this value)", INT};
  data_["font/kanji"]           = {"HanaMinA",
    "This font should cover at least ASCII and JIS X 208. "\
    "Recommended: MS PMincho, MS Mincho (at least one MS font should be present in Windows XP and higher) or hanazono (variant HanaMinA, http://fonts.jp/hanazono/", FONT};
  // colors
  data_["color/foreground"]     = {"0x00000000","Text color. Default: black",COLOR};
  data_["color/background"]     = {"0xbebebe00","Background color of widgets. Default: gray",COLOR};
  data_["color/background2"]    = {"0xffffff00","Alternative background color of widgets (background of fields). Default: white.",COLOR};
  data_["color/selection"]      = {"0x00008b00","Selection color: Default: dark blue",COLOR};
  data_["color/frequent"]       = {"0x0000ff00",
    "Color of the frequent readings and kanji. Default: blue",COLOR};
  data_["color/misc"]           = {"0x0000ff00",
    "Color of the misc data (). Default: blue",COLOR};
  data_["color/field"]          = {"0x00ff0000",
    "Color of the field data (comp, ling, ...). Default: green",COLOR};
  data_["color/pos"]            = {"0xff000000",
    "Color of the part-of-speech data (n,adj,exp,...). Default: red",COLOR};
  //
  data_["sources/url_database"] = {"http://aoi.souko.cz/downloads/main.db.gz", 
    "URL for downloading database", STRING},
  data_["sources/url_jmdict"]   = {"http://ftp.monash.edu.au/pub/nihongo/JMdict_e.gz",
    "URL for downloading JMdict.", STRING};
  data_["sources/url_kanjidic"] = {"http://www.csse.monash.edu.au/~jwb/kanjidic2/kanjidic2.xml.gz",
    "URL for downloading kanjidic2.",STRING}; 
  data_["sources/help_index"]   = {"help/index.html",
    "Path to help index.",STRING};
  // internal variables
  data_["knj/min_compo_count"]  = {"10","INTERNAL. Components with count lower then this value will be not shown in component view.",INT};
  data_["knj/jis208_only"]      = {"1","INTERNAL. State of the 'JIS X 208 only checkbox.'",BOOL};
  data_["dic/input_parser_warning"] = {"10","INTERNAL. How much kanji in andvanced search will trigger warning.",INT};

  default_ = data_;
}

} // namespace aoi_config
