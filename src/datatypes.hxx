/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __DATATYPES_HXX
#define __DATATYPES_HXX

#include "utils.hxx"    // for utils::to_string()
#include "sqlite3.hxx"  // for SQLite3::escape()

/*! \file datatypes.hxx
* Classes used across the program.
*/


namespace aoi {

//! Separator used for joined string in database.
const char *const SEPARATOR_SQL = "|";

//! One SKIP code.
struct SKIP
{
  int s1 = 0;
  int s2 = 0;
  int s3 = 0;
  string misclass = "";
  SKIP ( const vector<int> &v, const char *m ) 
    :s1(v[0]),s2(v[1]),s3(v[2]),misclass(m){};
  SKIP ( const string &ss1, const string &ss2, const string &ss3, const string &m ) 
    :s1(std::stoi(ss1)),s2(std::stoi(ss2)),s3(std::stoi(ss3)),misclass(m){};
};


//! Kanji.
class Kanji
{
  private:
    string kanji_;
    string ucs_;
    int strokes_ = 0;
    int freq_ = 0;
    vector<string> onyomi_;
    vector<string> kunyomi_;
    // NOTE: flags: std::vector is needed, no std::set (sorting)
    vector<string> flags_;   // jis
    int rad_classic_ = 0;
    int rad_nelson_ = 0;
    vector<string> nanori_;
    vector<string> meaning_;
    vector<SKIP> skip_;
    string components_;
    int jlpt_ = 0;
    int grade_ = 0;

  public:

    Kanji (): kanji_("") {};
    Kanji ( const string &kanji ) : kanji_(kanji) {};
    
    // setters
    inline void kanji   ( const string &s ) { kanji_ = s;}
    inline void ucs     ( const string &s ) { ucs_ = s; };
    inline void onyomi  ( const string &s ) { onyomi_.push_back(s); };
    inline void onyomi  ( const vector<string> &v ) { onyomi_=v; };
    inline void kunyomi ( const string &s ) { kunyomi_.push_back(s); };
    inline void kunyomi ( const vector<string> &v ) { kunyomi_=v; };
    inline void flags   ( const string &s ) { flags_.push_back(s); };
    inline void flags   ( const vector<string> &v ) { flags_=v; };
    inline void strokes ( const char *s )   { strokes_ = atoi(s); };
    inline void strokes ( int i )   { strokes_ = i; };
    inline void freq    ( const char *s )   { freq_ = atoi(s); };
    inline void freq    ( int i )   { freq_ = i; };
    inline void meaning ( const string &s ) { meaning_.push_back(s); };
    inline void meaning ( const vector<string> &v ) { meaning_ = v; };
    inline void nanori  ( const string &s ) { nanori_.push_back(s); };
    inline void nanori  ( const vector<string> &v ) { nanori_ = v; };
    inline void rad_classic ( const char *s )   { rad_classic_ = atoi(s); };
    inline void rad_classic ( int i )   { rad_classic_ = i; };
    inline void rad_nelson  ( const char *s )   { rad_nelson_ = atoi(s); };
    inline void rad_nelson  ( int i )   { rad_nelson_ = i; };
    inline void skip ( const string &s1, const string &s2, const string &s3, 
                        const string &m="" ) {
      skip_.push_back( SKIP( s1, s2, s3, m ) );
    };
    inline void skip    ( const vector<int> &v, const char *misclass=0 ) {
      skip_.push_back( SKIP( v, misclass ? misclass:"" ) );
    };
    inline void components ( const string &s ) { components_=s; };
    inline void jlpt ( int i ) { jlpt_=i; };
    inline void grade ( int i ) { grade_=i; };

    // getters  
    inline string kanji () const { return kanji_; };
    inline string ucs () const { return ucs_; };
    inline vector<string> onyomi () const { return onyomi_; };
    inline vector<string> kunyomi () const { return kunyomi_; };
    inline vector<string> flags () const { return flags_; };
    inline vector<string> meaning () const { return meaning_; };
    inline vector<string> nanori () const { return nanori_; };
    inline int freq () const { return freq_; };
    inline int strokes () const { return strokes_; };
    inline int rad_classic () const { return rad_classic_; };
    inline int rad_nelson () const { return rad_nelson_; };
    inline int jlpt () const { return jlpt_; };
    inline int grade () const { return grade_; };
    inline vector<SKIP> skip () const { return skip_; };
    inline string components () const { return components_;};

    inline string str_onyomi () const { return utils::to_string(onyomi_,", "); };
    inline string str_kunyomi () const { return utils::to_string(kunyomi_,", "); };
    inline string str_nanori () const { return utils::to_string(nanori_,", "); };
    inline string str_meaning () const { return utils::to_string(meaning_,", "); };
    inline string str_flags () const { return utils::to_string(flags_,", "); };
    inline string str_radicals () const { 
      if (rad_classic_ && rad_nelson_ ){
        std::stringstream ss;
        ss << rad_classic_ << " (" << rad_nelson_ << ")";
        return ss.str();
      }
      if (rad_classic_)
        return std::to_string(rad_classic_);
      return ""; 
    };
};


/*!
* One kanji element.
* \see DicWord
*/
class ElementKanji
{
  private:
    int kid_;
    string kanji_;
    vector<string> inf_;
    bool freq_;
  public:
    ElementKanji( int kid, const string &kanji, const vector<string> &inf, 
                  bool freq)
      :kid_(kid), kanji_(kanji), inf_(inf), freq_(freq){};
    ~ElementKanji(){};

    //! Returns string for <u>inserting</u> this elements data <u>into</u> sqlite3 table.
    string sql ( int did, const char *sep ){
      std::stringstream ss;
      ss << "INSERT INTO d_kanji (kid,did,kanji,inf,freq) VALUES ("
        << kid_ << "," << did << ",'" << kanji_ << "','" 
        << utils::to_string(inf_,sep) << "'," << freq_ << ");\n";  
      return ss.str();
    }

    inline int kid() const {return kid_;};
    inline string kanji() const {return kanji_;};
    inline vector<string> inf() const {return inf_;};
    inline bool freq() const {return freq_;};
};


/*!
* One reading element.
* \see DicWord
*/
struct ElementReading
{
  private:
    int rid_;
    string reading_;
    bool nokanji_;
    vector<string> restr_;
    vector<string> inf_;
    bool freq_  ; 
  public:
    ElementReading( int rid, const string &reading, bool nokanji,
        const vector<string> &restr, const vector<string> &inf, bool freq)
      : rid_(rid), reading_(reading), nokanji_(nokanji), restr_(restr), 
        inf_(inf), freq_(freq) {};
    ~ElementReading(){};

    //! Returns string for <u>inserting</u> this elements data <u>into</u> sqlite3 table.
    string sql ( int did, const char *sep ){
      std::stringstream ss;
      ss << "INSERT INTO d_reading (rid,did,reading,inf,nokanji,freq) VALUES ("
        << rid_ << "," << did << ",'" << reading_ << "','" 
        << utils::to_string(inf_,sep) << "',"
        << nokanji_ << "," << freq_ << ");\n";
      // \todo: d_re_restr
      return ss.str();
    }


    inline int rid() const {return rid_;};
    inline string reading() const {return reading_;};
    inline bool nokanji() const {return nokanji_;};
    inline vector<string> inf() const {return inf_;};
    inline vector<string> restr() const {return restr_;};
    inline bool freq() const {return freq_;};
};              
                

/*!
* One sense element.
* \see DicWord
*/
struct ElementSense
{               
  private:
    int sid_;      
    vector<string> gloss_;  
    vector<string> stagk_;  
    vector<string> stagr_;  
    vector<string> pos_;  
    vector<string> xref_;  
    vector<string> ant_;  
    vector<string> field_;  
    vector<string> misc_;  
    vector<string> dial_;  
    vector<string> s_inf_;  
  public:
    ElementSense( int sid, const vector<string> &gloss, const vector<string> &stagk,  
        const vector<string> &stagr, const vector<string> &pos, 
        const vector<string> &xref, const vector<string> &ant,  
        const vector<string> &field, const vector<string> &misc,  
        const vector<string> &dial, const vector<string> &s_inf )
      : sid_(sid), gloss_(gloss), stagk_(stagk), stagr_(stagr), pos_(pos),
        xref_(xref), ant_(ant), field_(field), misc_(misc), dial_(dial),
        s_inf_(s_inf) 
    {}
    ~ElementSense(){}

    //! Returns string for <u>inserting</u> this elements data <u>into</u> sqlite3 table.
    string sql ( int did, const char *sep ){
      std::stringstream ss;
      ss << "INSERT INTO d_sense (sid,did,gloss,xref,ant,inf,pos,field,misc,dial)"
        << " VALUES (" << sid_ << "," << did << ",'"
        << SQLite3::escape(utils::to_string(gloss_,sep)) << "','"
        << utils::to_string(xref_,sep) << "','"
        << utils::to_string(ant_,sep) << "','"
        << SQLite3::escape(utils::to_string(s_inf_,sep)) << "','"
        << utils::to_string(pos_,sep) << "','"
        << utils::to_string(field_,sep) << "','"
        << utils::to_string(misc_,sep) << "','"
        << utils::to_string(dial_,sep) << "'"
      << ");\n";
      // d_stagk, d_stagr
      return ss.str();
    }

    inline int sid() const {return sid_;}
    inline vector<string> gloss() const {return gloss_;}
    inline vector<string> stagk() const {return stagk_;}
    inline vector<string> stagr() const {return stagr_;}
    inline vector<string> pos() const {return pos_;}
    inline vector<string> xref() const {return xref_;}
    inline vector<string> ant() const {return ant_;}
    inline vector<string> field() const {return field_;}
    inline vector<string> misc() const {return misc_;}
    inline vector<string> dial() const {return dial_;}
    inline vector<string> s_inf() const {return s_inf_;}
};


/*!
* One dictionary record for a word. It has 0+ KanjiElements, 1+ ReadingElements
* and 1+ SenseElements.
*/
class DicWord
{
  private:
    int did_;
    vector<ElementKanji> k_ele_;
    vector<ElementReading> r_ele_;
    vector<ElementSense> s_ele_;
  public:
    DicWord( int i=-1, const vector<ElementKanji> &k={}, 
      const vector<ElementReading> &r={}, const vector<ElementSense> &s={} ) 
      : did_(i), k_ele_(k), r_ele_(r), s_ele_(s) {}
    ~DicWord(){}

    // setters
    inline void r_ele ( const ElementReading &rele )
      { r_ele_.push_back(rele); }
    inline void k_ele ( const ElementKanji &kele )
      { k_ele_.push_back(kele); }
    inline void s_ele ( const ElementSense &sele )
      { s_ele_.push_back(sele); }

    // getters
    inline int did() const {return did_;}
    inline vector<ElementKanji> k_ele () const {return k_ele_;}
    inline vector<ElementReading> r_ele () const {return r_ele_;}
    inline vector<ElementSense> s_ele () const {return s_ele_;}

    //! Returns HTML 2.0 string with word details (for Fl_Help_View).
    string html () const {
      std::stringstream ss;
      // kanji
      if ( !k_ele_.empty() )
        ss << "&nbsp;<br>"; // to compensate kanji's size
      for ( auto &k: k_ele_ ){
        ss << "<font face=\"symbol\" size=\"7\"";
        if ( k.freq() )
          ss << " color=\"blue\"";
        ss << ">" << k.kanji() << "</font>";
        if ( !k.inf().empty() )
          ss << "[" << utils::to_string(k.inf()) << "]";
        ss << "<br>";
      }
      // reading
      for ( auto &r: r_ele_ ){
        ss << "<font face=\"symbol\" size=\"4\"";
        if ( r.freq() )
          ss << " color=\"blue\"";
        ss << ">" << r.reading() << "</font>";
        if ( !r.inf().empty() )
          ss << "[" << utils::to_string(r.inf()) << "]";
        if ( r.nokanji() )
          ss << "[NOKANJI]";
        ss << "<br>";
      }
      // sense
      for ( auto &s: s_ele_ ){
        ss << "- ";
        if ( !s.pos().empty() )
          ss << "<font color=\"red\" size=\"2\"><i>" 
            << utils::to_string( s.pos() ) << "</i></font>&nbsp;";
        if ( !s.misc().empty() )
          ss << "<font color=\"blue\" size=\"2\"><i>" 
            << utils::to_string( s.misc() ) << "</i></font>&nbsp;";
        ss << utils::to_string( s.gloss() );
        if ( !s.field().empty() )
          ss << "<font color=\"green\" size=\"2\"><i>" 
            << utils::to_string( s.field() ) << "</i></font>&nbsp;";
        if ( !s.dial().empty() )
          ss << "<font color=\"violet\" size=\"2\"><i>" 
            << utils::to_string( s.dial() ) << "</i></font>&nbsp;";
        ss << "<br>";
        if ( !s.ant().empty() )
          ss << "&nbsp;&nbsp;<b>Antonyms:</b> " << "<font face=\"symbol\">" 
            << utils::to_string( s.ant() ) << "</font><br>";
        if ( !s.xref().empty() )
          ss << "&nbsp;&nbsp;<b>See also:</b> " << "<font face=\"symbol\">" 
            << utils::to_string( s.xref() ) << "</font><br>";
        if ( !s.s_inf().empty() )
          ss << "&nbsp;&nbsp;<b>Note:</b> " << "<font face=\"symbol\">" 
            << utils::to_string( s.s_inf() ) << "</font><br>";
      }
      return ss.str();
    }
};

} // namespace aoi
#endif // __DATATYPES_HXX
