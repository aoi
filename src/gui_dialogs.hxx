/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _GUI_DIALOGS_HXX
#define _GUI_DIALOGS_HXX

/*! \file gui_dialogs.hxx
* Custom dialogs.
*/

#include <cmath>
#include <curl/curl.h>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Menu_Window.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Help_View.H>

#include "logger.hxx"
#include "gui_widgets.hxx"
#include "datatypes.hxx"
#include "config.hxx"
#include "romanization.hxx"


using aoi_config::Config;
using aoi::Kanji;

namespace aoi_ui {

/*!
* Dialog for testing and setting fonts. Shows list of all available fonts,
* labels with testing chracters ( ASCII, KANA, JISX208/212/213 ) and input
* field for custom text.
*/
class DialogFontTest : public Fl_Double_Window
{
  private:
    Fl_Hold_Browser *browser_ = nullptr;
    Fl_Box *l_fonts_  = nullptr;
    Fl_Box *l_ascii_  = nullptr;
    Fl_Box *l_jis208_ = nullptr;
    Fl_Box *l_jis212_ = nullptr;
    Fl_Box *l_jis213_ = nullptr;
    Fl_Box *l_kana_   = nullptr;
    Fl_Box *b_ascii_  = nullptr;
    Fl_Box *b_jis208_ = nullptr;
    Fl_Box *b_jis212_ = nullptr;
    Fl_Box *b_jis213_ = nullptr;
    Fl_Box *b_kana_   = nullptr;
    Fl_Button *bt_ok_ = nullptr;
    Fl_Button *bt_cancel_ = nullptr;
    Fl_Input *input_  = nullptr;
    string s_fonts_;
    Fl_Font selected_font_ = -1;

  public:
    DialogFontTest( int w=600, int h=400 ) : Fl_Double_Window(w,h) 
    {
      browser_ = new Fl_Hold_Browser( 5, 5, 230, 365 );
      browser_->callback(static_callback, (void*)this);
      l_fonts_ = new Fl_Box( 5, 370, 250, 30 );
      l_fonts_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);

      l_ascii_ = new Fl_Box( 240, 5, 110, 40, "ASCII" );
      l_ascii_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      l_ascii_->labelfont(FL_BOLD);
      b_ascii_ = new Fl_Box( 355, 5, 240, 40, "Hello world" );
      b_ascii_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      b_ascii_->labelsize(36);

      l_kana_ = new Fl_Box( 240, 50, 110, 40, "Kana" );
      l_kana_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      l_kana_->labelfont(FL_BOLD);
      b_kana_ = new Fl_Box( 355, 50, 240, 40, "ひらーカタ." );
      b_kana_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      b_kana_->labelsize(36);

      l_jis208_ = new Fl_Box( 240, 95, 110, 40, "JIS X 208" );
      l_jis208_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      l_jis208_->labelfont(FL_BOLD);
      b_jis208_ = new Fl_Box( 355, 95, 240, 40, "龠堯槇遙凜熙");
      b_jis208_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      b_jis208_->labelsize(36);

      l_jis212_ = new Fl_Box( 240, 140, 110, 40, "JIS X 212" );
      l_jis212_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      l_jis212_->labelfont(FL_BOLD);
      b_jis212_ = new Fl_Box( 355, 140, 240, 40, "龑龒龔龖龗龞");
      b_jis212_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      b_jis212_->labelsize(36);


      l_jis213_ = new Fl_Box( 240, 185, 110, 40, "JIS X 213" );
      l_jis213_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      l_jis213_->labelfont(FL_BOLD);
      b_jis213_ = new Fl_Box( 355, 185, 240, 40, "臭艹艹著贈辶");
      b_jis213_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
      b_jis213_->labelsize(36);

      input_ = new Fl_Input( 240, 270, 350, 40, "Custom text" );
      input_->align( FL_ALIGN_TOP_LEFT );
      input_->labelfont( FL_HELVETICA|FL_BOLD );

      bt_ok_ = new Fl_Button( w-70, h-35, 65, 30, "OK");
      bt_ok_->callback( static_ok, (void*)this );
      bt_cancel_ = new Fl_Button( w-140, h-35, 65, 30, "Cancel");
      bt_cancel_->callback( static_cancel, (void*)this );

      #ifdef WINDOWS
        Fl_Font n_fonts = Fl::set_fonts("*");
      #else
        Fl_Font n_fonts = Fl::set_fonts("-*-*-*-*-*-*-*-*-*-*-*-*-iso10646-1");
      #endif

      s_fonts_ = std::to_string(n_fonts) + string(" fonts found");
      l_fonts_->label(s_fonts_.c_str());

      for ( int i=0; i<n_fonts; ++i ){
        const char *name = Fl::get_font_name(i);
        browser_->add( name, (void*)i );
      }

      // hide some fonts
      browser_->hide( FL_SYMBOL+1 );  // protected for Help_View
//      browser_->hide( FONT_KANJI+1 ); // reserved for program

      label("Fonts");
      hide();
    }

    //! Callback called when selecting font
    void cb ( Fl_Font f ){
      b_ascii_->labelfont(f);
      b_kana_->labelfont(f);
      b_jis208_->labelfont(f);
      b_jis212_->labelfont(f);
      b_jis213_->labelfont(f);
      input_->textfont(f);
      redraw();
    }

    //! Callback called on 'OK' button
    void cb_ok () {
      selected_font_ = browser_->value()-1;
      hide();
    }

    //! Callback called on 'Cancel' button
    void cb_cancel () {
      selected_font_ = -1;
      hide();
    }

    /*! 
    * Runs dialog and waits for interrupt ( OK button, Cancel button, 
      ESC, Close window )
    * \return selected font on OK button, -1 otherwise.
    */
    int run (){
      show();
      while ( visible() ){
        usleep(1000);
        Fl::check();
      }
      return selected_font();
    }

    //! Returns selected font
    inline Fl_Font selected_font() const { return selected_font_; };
    //! Calls cb()
    inline static void static_callback ( Fl_Widget *w, void *p )
      { ((DialogFontTest*)p)->cb( ((Fl_Hold_Browser*)w)->value()-1 ); };
    //! Calls cb_ok()
    inline static void static_ok ( Fl_Widget *w, void *p )
      { ((DialogFontTest*)p)->cb_ok(); }
    //! Calls cb_cancel()
    inline static void static_cancel ( Fl_Widget *w, void *p )
      { ((DialogFontTest*)p)->cb_cancel(); };

};


/*!
* Download dialog that shows the progress of download and allows the change
* ot the url and output filename.
*/
class DialogDownload : public Fl_Double_Window
{
  private:
    Fl_Progress *progress_  = nullptr;
    Fl_Button *b_abort_     = nullptr;
    Fl_Button *b_url_       = nullptr;
    Fl_Button *b_fname_     = nullptr;
    Fl_Button *b_dload_     = nullptr;
    Fl_Box *l_url_          = nullptr;
    Fl_Box *l_fname_        = nullptr;
    string url_             = "";
    string fname_           = "";
    static bool abort_download_;      //!< See mycurl_progress_func()
    bool downloading_       = false;  //!< See abort() and scb_close_window()

  public:
    DialogDownload( int w=400, int h=125 );
    ~DialogDownload();

    //! Needed by curl
    static size_t mycurl_write_func(void *ptr, size_t size, size_t nmemb, FILE *stream)
      { return fwrite(ptr, size, nmemb, (FILE *)stream);  }
    //! Needed by curl
    static size_t mycurl_read_func(void *ptr, size_t size, size_t nmemb, FILE *stream)
      { return fread(ptr, size, nmemb, stream);  }

    /*! 
    * Needed by curl. Sets progress and calls Fl::check().
    * Aborts download if abort_download_=true.
    */
    static int mycurl_progress_func(void *progress_widget,
                     double dltotal, double dlnow,
                     double ultotal, double ulnow )
    {
      float perc = 100.*dlnow/dltotal;
      char buff[128];
      sprintf(buff, "%.1f%% of %.1f MB", perc, dltotal/1024/1024);
      ((Fl_Progress*)progress_widget)->label( buff );
      ((Fl_Progress*)progress_widget)->value(perc);
      Fl::check();
      // non-zero cancels download 
      return abort_download_;
    }

    //! Runs dialog and waits for interrupt.
    void run (){
      show();
      while ( visible() ){
        usleep(1000);
        Fl::check();
      }
    }

    /*!
    * Starts a CURL download.
    * \param url  URL to be downloaded
    * \param fname name od the output file
    */ 
    void download_url( const char *url, const char *fname="downloaded.file" );
    //! Sets URL and output file name. Tries to guess fname from url if fname is empty.
    void set_names ( const string &url, const string &fname="" );
    //! Aborts download. Asks for confirmation if downloading_=true.
    void abort ();
    //! Starts download.
    void cb_download ();
    //! Allows change of the URL.
    void cb_url_callback ();
    //! Allos change of the output filename.
    void cb_fname_callback ();

    //! See cb_url_callback()
    inline static void scb_url_callback ( Fl_Widget *w, void *p ){
      ((DialogDownload*)p)->cb_url_callback(); }
    //! See cb_fname_callback()
    inline static void scb_fname_callback ( Fl_Widget *w, void *p ){
      ((DialogDownload*)p)->cb_fname_callback(); }
    //! See abort()
    inline static void scb_abort ( Fl_Widget *w, void *p ){
      ((DialogDownload*)p)->abort(); }
    //! See cb_download()
    inline static void scb_download ( Fl_Widget *w, void *p ){
      ((DialogDownload*)p)->cb_download(); }
    /*!
    * Prevents closing dialog with ESC and when downloading_=true.
    */
    inline static void scb_close_window ( Fl_Widget *w, void *f ){
      // dont close main window on ESC
      if ( Fl::event() == FL_SHORTCUT && Fl::event_key() == FL_Escape )
        return;
      // dont close when download in progress
      if ( ((DialogDownload*)f)->downloading_ ) {
        fl_message("You must abort download before closing window." );
        return;
      }
      ((DialogDownload*)w)->hide();
    };
};


/*!
* Shows all known informations about word.
* \todo change to edit word dialog
*/
class DialogEditWord : public Fl_Double_Window
{
  private: 
    Fl_Button     *b_cancel_  = nullptr;
    Fl_Help_View  *view_      = nullptr;
    Label         *l_did_     = nullptr;

  public:
    DialogEditWord ( int w=400, int h=500, const char *l="Edit word" );
    ~DialogEditWord(){}

    inline void set ( const aoi::DicWord &w ){ 
      l_did_->set( std::to_string(w.did()) ); 
      view_->value( w.html().c_str() ); 
    }

    inline static void scb_cancel ( Fl_Widget *w, void *p )
      { ((DialogEditWord*)p)->hide(); }
};


/*!
* Shows Help_View with Cancel button. Stores text.
*/
class DialogHtmlView : public Fl_Double_Window
{
  private: 
    Fl_Button     *b_cancel_  = nullptr;
    Fl_Help_View  *view_      = nullptr;
    string        str_;

  public:
    DialogHtmlView ( int w=400, int h=500, const char *l=0 )
      : Fl_Double_Window(w,h,l)
    {
      view_     = new Fl_Help_View(5, 5, w-10, h-80);
      b_cancel_ = new Fl_Button( w-65, h-35, 60, 30, "Close");
      end();
      b_cancel_->callback( scb_cancel, (void*)this );
      view_->textsize(16);
      set_modal();
    }
    ~DialogHtmlView(){};
    inline void set ( const string &s ){ 
      str_ = s;
      view_->value( str_.c_str() ); 
    }
    inline static void scb_cancel ( Fl_Widget *w, void *p )
      { ((DialogHtmlView*)p)->hide(); }
};


/*!
* Dialog with database information. Allows dowloading of the new database
* and checking user.db against main.db
* \note Callback for b_dload_ is set and called by the widgets caller/creator
*   (currently aoi::App::cb_manage_db() )
*/
class ManageDBDialog : public Fl_Double_Window
{
  private:
    Fl_Button *b_close_       = nullptr;
    Fl_Button *b_dload_       = nullptr;
    Fl_Button *b_check_main_  = nullptr;
    Fl_Button *b_check_user_  = nullptr;
    Label *lver_main_         = nullptr;
    Label *l_main_created_   = nullptr;
    Label *lver_jmdict_      = nullptr;
    Label *lver_kanjidic_    = nullptr;
    Label *lver_kradfile_    = nullptr;
    Label *lver_tatoeba_     = nullptr;
    Label *l_checked_against_= nullptr;

  public:
    ManageDBDialog ( int w=400, int h=300, const char *l="Manage Database" );
    ~ManageDBDialog(){}

    // setters
    //! Sets the version of the main database.
    inline void main_version ( const string &s )
      { lver_main_->set(s); }
    //! Sets the main database creation date.
    inline void main_created ( const string &s )
      { l_main_created_->set(s); }
    //! Sets the version of the jmdict used in database.
    inline void main_ver_jmdict ( const string &s )
      { lver_jmdict_->set(s); }
    //! Sets the version of the kanjidic used in database.
    inline void main_ver_kanjidic ( const string &s )
      { lver_kanjidic_->set(s); }
    //! Sets the version of the kradfile used in database.
    inline void main_ver_kradfile ( const string &s )
      { lver_kradfile_->set(s); }
    //! Sets the version of the tatoeba data used in database.
    inline void main_ver_tatoeba ( const string &s )
      { lver_tatoeba_->set(s); }
    //! Sets the main db. version paired with current user database.
    inline void user_checked_against ( const string &s )
      { l_checked_against_->set(s); }

    //! Sets callback for Button download
    inline void cb_download( Fl_Callback c=nullptr, void *data=nullptr ) 
      { if (c && b_dload_) b_dload_->callback(c, data); }

    // static callbacks
    /*!
    * Check the user.db against main.db.
    * \todo nothing yet
    */
    inline static void scb_check_user ( Fl_Widget *w, void *p )
      { fl_alert("Nothing yet."); }
    /*!
    * Check main.db integrity and whether any newer version exists.
    * \todo nothing yet
    */
    inline static void scb_check_main ( Fl_Widget *w, void *p )
      { fl_alert("Nothing yet."); }
    //! Close dialog.
    inline static void scb_close ( Fl_Widget *w, void *p )
      { ((ManageDBDialog*)p)->hide(); }
};


/*!
*  Popup window that shows informations about kanji.
*  The window is closed with the close button in the titlebar or with ESC.
*  \see KanjiInfo
*/
class KanjiPopupWindow : public Fl_Menu_Window
{
  private:
    KanjiInfo *box_;

  public:
    KanjiPopupWindow ( int w, int h );
    ~KanjiPopupWindow (){};
  
    //! See KanjiInfo::font_b()
    inline void font_a ( Fl_Font f, int s ) { box_->font_a( f, s ); }
    //! See KanjiInfo::font_a()
    inline void font_b ( Fl_Font f, int s ) { box_->font_b( f, s ); }
    //! See KanjiInfo::set_data()
    inline void set ( const Kanji &k ) {  box_->set_data(k); }

    int handle ( int event ); 

    //! Popups the window positioned at the cursor.
    inline void popup() {
      // XXX: after clear_border() take_focus() does not work
//      clear_border();
      // position at cursor
      position(Fl::event_x_root(), Fl::event_y_root());
      size( box_->w(), box_->h());
      show();
      take_focus();
    }
};


/*
* Global progress meter.
*/
class DialogProgress : public Fl_Double_Window
{
  private:
    string message_ = "";
    Fl_Progress *progress_ = nullptr;

  public:
    DialogProgress( int w, int h ):Fl_Double_Window(w,h){
      box( FL_BORDER_BOX );
      progress_ = new Fl_Progress( 10, 10, w-20, 30 );
      progress_->minimum(0);
      progress_->maximum(1);
      set_modal();
      clear_border();
      end();
    }
    ~DialogProgress(){}

    //! Sets value to 0% and string to "".
    void init () { message_ = ""; }

    //! Sets progress in <i>percent</i> and <i>message</i> to be shown.
    void progress ( float percent, const string &message="" ){
      message_ = message;
      progress_->label(message_.c_str());
      progress_->value( percent/100. );
      Fl::check();
    }
};


/*
* Shows Fl_Browser with config data. Selection (leftclick, enter or space)
* on the even row will show description of the config value,
* selection on the odd row will allow change of the value.
* \todo odd ? even ?
*/
class DialogSettings : public Fl_Double_Window
{
  private:
    Fl_Hold_Browser *browser_     = nullptr;
    Fl_Button       *b_ok_        = nullptr;
    Fl_Button       *b_defaults_  = nullptr;
    std::map<string,Config::Value> data_;
    std::map<string,Config::Value> defaults_;

  public:
    DialogSettings( int w=400, int h=500 );
    ~DialogSettings(){};

    int handle ( int event );

    /*!
    * Runs the dialog and waits for an interrupt (OK button, Cancel button, 
    * Close window, ESC).
    * \return data_ (always)
    */
    std::map<string,Config::Value> run ();

    /*!
    * Sets config data to be shown.
    * Sets data_ to m and defaults_ to defaults (or to m if defaults is empty )
    */
    void set (  const std::map<string,Config::Value> &m, 
                const std::map<string,Config::Value> &defaults={} );

    //! Replaces data_ with defaults_
    inline void restore_defaults () { 
      data_ = defaults_; 
      hide(); 
    };

    //! Callback called after selection (click, space, enter) of an item in browser.
    void cb ( int line );
    //! See cb()
    inline static void static_callback ( Fl_Widget *w, void *p )
      { ((DialogSettings*)p)->cb( ((Fl_Hold_Browser*)w)->value()); };  
    //! Hides dialog
    inline static void static_ok ( Fl_Widget *w, void *p )
      { ((DialogSettings*)p)->hide(); }
    //! See restore_defaults()
    inline static void scb_restore_defaults ( Fl_Widget *w, void *p )
      { ((DialogSettings*)p)->restore_defaults(); };
};

} // namespace aoi_ui
#endif // _GUI_DIALOGS_HXX
