/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*! \file sqlite3.hxx
* 
*/

#ifndef _SQLITE3_HXX
#define _SQLITE3_HXX

#include "3rdparty/sqlite3.h"
#include <exception>
#include <vector>
#include <string>
#include <sstream>
#include <cstring>
#include <stdexcept>

using std::vector;
using std::string;

namespace SQLite3 {

//////////////////////////////////////////////////////////////////////////
// EXCEPTIONS

class CantOpenDatabase : public std::exception {
  private:
    string msg_;
  public:
    CantOpenDatabase(const string &filename): msg_(filename) {};
    const char *what() const noexcept {return msg_.c_str();};
};


class DatabaseError : public std::exception {
  private:  
    string msg_;
    string query_;
  public: 
    DatabaseError(const string &msg, const string &query)
      : msg_(msg.c_str()), query_(query){};
    const char *what() const noexcept {return msg_.c_str();};
    //! Returns details about executed query.
    const char *query() const {return query_.c_str();};
};


//////////////////////////////////////////////////////////////////////////
// FUNCTIONS

/*!
* Replaces ' with '' and " with \".
*/
inline string escape ( const char *s )
{
  if ( !strlen(s) )
    return "";

  std::stringstream ss;
  for ( size_t i=0; i<strlen(s); i++ ){
    ss << s[i];
    if ( s[i] == '\'' )
      ss << "'";
    else if ( s[i] == '"' )
      ss << "\"";
  }  
  return ss.str();
}

inline string escape ( const string &s ){ return escape(s.c_str()); }


//////////////////////////////////////////////////////////////////////////
// class SQLite3

class SQLite3
{
  private:
    sqlite3 *db_ = nullptr;
    vector<string> data_ = {};
    vector<string> columns_ = {};

  public:
    /*!
    * Constructor. Calls open().
    * \param filename Name of the sqlite3 database file.
    */
    SQLite3 ( const char *filename ) {  open( filename );  }

    //! Destructor. Calls close().
    ~SQLite3 () { close(); }

    /*!
    * Opens sqlite3 database.
    * \exception CantOpenDatabase
    * \param filename NAme of the sqlite3 database.
    */
	  void open( const char *filename ){
	    if( sqlite3_open(filename, &db_) != SQLITE_OK ) {
        close();
        throw CantOpenDatabase(filename);
      }
    }

    //! Closes database.
	  void close() { sqlite3_close(db_); }

    //! sqlite3_exec calls this function for each row
    static int callback(void *parent, int argc, char **argv, char **column) {
      // put column names into vector<string> columns_
      if ( ((SQLite3*)parent)->data_.empty() ) {
        for( int i=0; i<argc; i++ )
          ((SQLite3*)parent)->columns_.push_back( column[i] );
      }
      // put row data into vector<string> data_
      for( int i=0; i<argc; i++ )
        ((SQLite3*)parent)->data_.push_back( argv[i] ? argv[i] : "" );
      return 0;
    }

    //! Returns all data (same data as query() ). 
    inline vector<string> data () const { return data_; };

    //! Returns column names
    inline vector<string> columns () const { return columns_; };

    //! Returns # of returned rows ( data().size()/columns().size() )
    inline size_t result_rows () const 
      { return data_.empty() ? 0:data_.size()/columns_.size(); };

    /*!
    * Executes SQLite query.
    * Query can contain more commands separated with semicolon.
    * \exception CantOpenDatabase
    * \exception DatabaseError
    * \param query  SQLite query
    * \return Vector of strings with query results. Order of elements is: row1_col1, row1_col2, .. row1_colN, row2_col1, .. row2_colN, .. , rowM_col1, .., rowM_colN
    * \sa script()
    */
	  vector<string> query( const char *query ){
      if ( !db_ )
        throw CantOpenDatabase("SQLite3::query(): db_ is NULL");
      char *err = nullptr;
      if ( !data_.empty() )       data_.clear();
      if ( !columns_.empty() )    columns_.clear();
      int rc = sqlite3_exec( db_, query, SQLite3::callback, (void*)this, &err);
      string s(err?err:"");
      sqlite3_free(err);
      if ( rc != SQLITE_OK )
        throw DatabaseError(s,query);
      return data_;
    };

  /*!
  * Loads and executes file containing SQLite script.
  * \exception std::runtime_error
  * \param path path to script file
  * \sa query()
  */
  void script ( const char *path ){
    std::ifstream f(path);
    if ( f.is_open() ){
      f.seekg( 0, f.end );
      size_t size = f.tellg();
      f.seekg( 0, f.beg );
      if ( size > 0 ){
        char buff[ size+1 ];
        f.read( buff, size );
        f.close();
        query( buff );
      }
      else
        throw std::runtime_error("File has zero length: " + string(path));
    }
    else
      throw std::runtime_error("Can't open file: " + string(path));
  }
};

} // namespace sqlite3
#endif //_SQLITE3_HXX
