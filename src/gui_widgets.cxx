/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gui_widgets.hxx"

namespace aoi_ui{

void KanjiInfo::set_data ( const Kanji &k )
{
  kanji_ = k;

  box(FL_BORDER_BOX);
  fl_font( font_a_, size_a_ );
  int row_height = fl_height(font_a_, size_a_) + fl_descent();
  
  string knj = kanji_.kanji();
  strings_.push_back(knj);
  kanjibox_ = new Fl_Box( this->x()+padding_, this->y()+padding_, 
                          row_height*5, row_height*5, knj.c_str());
  kanjibox_->labelfont( FONT_KANJI );
  kanjibox_->labelsize( size_a_*6 );
  kanjibox_->box(FL_BORDER_BOX);

  fl_font( font_a_, size_a_ );
  int cw = fl_width("Frequency:");  // caption width: longest word in the first column
  int tw = fl_width("U+123456"); // text width: longest possible word in 2nd column
  int total_width = kanjibox_->w() + cw +tw+padding_;
    
  int xx = this->x() + padding_ + kanjibox_->h() + 5;
  int yy = this->y() + padding_;

  const int rh = row_height;

  yy += draw_caption_label( "UCS", "U+"+kanji_.ucs(), xx ,yy, cw, tw, rh );
  yy += draw_caption_label( "Strokes", std::to_string(kanji_.strokes()), xx ,yy, cw,tw,rh);
  if ( kanji_.rad_classic() )
    yy += draw_caption_label( "Radical", kanji_.str_radicals(), xx ,yy, cw,tw,rh);
  if ( kanji_.freq() )
    yy += draw_caption_label( "Frequency", std::to_string(kanji_.freq()), xx ,yy, cw,tw,rh);
  if ( kanji_.jlpt() )
    yy += draw_caption_label( "JLPT", std::to_string(kanji_.jlpt()), xx ,yy, cw,tw,rh);
  if ( kanji_.grade() )
    yy += draw_caption_label( "Grade", std::to_string(kanji_.grade()), xx ,yy, cw,tw,rh);
  bool skip_label_drawn = false;
  for ( auto skip : kanji_.skip() ){
    char buff[64];
    sprintf( buff, "%d-%d-%d %s", 
      skip.s1, skip.s2, skip.s3, skip.misclass.empty() ? "":"!" );
    yy += draw_caption_label( skip_label_drawn ? "":"SKIP", buff, xx ,yy, cw,tw,rh);
    skip_label_drawn = true;
  }


  // draw box ignores hh and sets the height as necesary
//  string ss = kanji_.components();
//  yy += draw_box(font_kanji_, size_a_+2, ss,xx,yy,cw+tw,rh);


  if ( yy < kanjibox_->x() + kanjibox_->h() + 5 )
    yy = kanjibox_->x() + kanjibox_->h() + 5;

  xx = this->x() + padding_;
  int hh = row_height*3;
  int ww = total_width - 2*padding_;

  string ss = kanji_.str_onyomi();
  int bh = draw_box(font_kanji_, size_a_+2, ss,xx,yy,ww,hh);
  yy += bh +size_b_;

  ss = kanji_.str_kunyomi();
  bh = draw_box(font_kanji_, size_a_+2, ss,xx,yy,ww,hh);
  yy += bh +size_b_;

  ss = kanji_.str_nanori();
  bh = draw_box(font_kanji_, size_a_, ss,xx,yy,ww,hh);
  yy += bh +size_b_;

  ss = kanji_.str_meaning();
  bh = draw_box(font_b_, size_b_, ss,xx,yy,ww,hh);
  yy += bh +10;

  ss = kanji_.str_flags();
  bh = draw_box(font_b_,size_b_, ss.empty() ? "FLAGS":ss,xx,yy,ww,hh);


  if ( autoresize() ){
    resizable(0);
    size(total_width,yy+row_height+padding_);
  }

  end();
}

//////////////////////////////////////////////////////////////////////////
// COMPONENTVIEW


ComponentView::ComponentView ( int x, int y, int w, int h, const char *l ) : Fl_Widget(x,y,w,h,l)
{
  box(FL_BORDER_BOX);
  font_cell();
  font_label();
}


void ComponentView::font_cell ( Fl_Font f, int s)
{
  font_cell_ = f;
  fsize_cell_ = s;
  set_cellsize();
}

void ComponentView::set_cellsize ()
{
  fl_font(font_cell_,fsize_cell_);
  int cellsize1 = fl_height() + fl_descent()/2; 
  fl_font(font_label_,fsize_label_);
  int cellsize2 = fl_height() + fl_descent()/2; 
  cell_size_ = std::max(cellsize1,cellsize2); 
}


void ComponentView::font_label ( Fl_Font f, int s)
{
  font_label_ = f;
  fsize_label_ = s;
  set_cellsize();
}

vector<string> ComponentView::cells_by_type ( CellType t )
{
  vector<string> v;
  for ( Cell &c: cells_ )
    if ( c.type == t )
      v.push_back(c.str);
  return v;
}


void ComponentView::set_data ( const vector<Cell> &d )
{
  cells_ = d;
  redraw();
}


void ComponentView::set_highlight ( const string &components )
{
  for ( auto &c: cells_ ){
    if ( c.type == CELL_HIGHLIGHTED ) // clear previous highlight
      c.type = CELL;
    if ( components.find(c.str) != string::npos )
      c.type = CELL_HIGHLIGHTED;
  }
  redraw();
}


int ComponentView::handle ( int event )
{
  switch ( event ) {
    case FL_PUSH:
      int xx = Fl::event_x() - this->x();
      int yy = Fl::event_y() - this->y();
      int c = xx/cell_size_;
      int r = yy/cell_size_;
      int cell = r*cols_+c;
      CellType t = cells_[cell].type;
      // left click
      if ( Fl::event_button() == FL_LEFT_MOUSE ){
        if ( t != CELL_LABEL && t != CELL_SELECTED_2 ){
          cells_[cell].type = (t==CELL_SELECTED_1) ? CELL:CELL_SELECTED_1;  
          do_callback();
          redraw();
          return 1;
        }
      }
      // right click
      else if ( Fl::event_button() == FL_RIGHT_MOUSE ){
        if ( t != CELL_LABEL && t != CELL_SELECTED_1 ){
          cells_[cell].type = (t==CELL_SELECTED_2) ? CELL:CELL_SELECTED_2;  
          do_callback();
          redraw();
          return 1;
        }
      }
      break;
  }
  return Fl_Widget::handle(event);
}


void ComponentView::draw ()
{
  rows_ = h()/cell_size_;
  cols_ = w()/cell_size_;

	fl_push_clip(x(), y(), w(), h());
  {
    draw_box();
    if ( !cells_.empty() ) {
      Fl_Boxtype b = box();
      int bx = Fl::box_dx(b);
      int by = Fl::box_dy(b);

      // available area of widget
      int xx = x() + bx;
      int yy = y() + by;
      int ww = cols_ * cell_size_;
      int hh = rows_ * cell_size_;

      // background
      fl_color( Fl::get_color( FL_BACKGROUND2_COLOR ) );
      fl_rectf( x()+bx, y()+by, ww, hh );

      int cx = xx;
      int cy = yy;
      for ( int i=0; i<rows_*cols_; i++ ){
        if ( i == int(cells_.size()) )
          break;
        switch ( cells_[i].type ){
          case CELL_LABEL:
            fl_color( Fl::get_color( FL_FOREGROUND_COLOR ) );
            fl_rectf( cx, cy, cell_size_, cell_size_ );
            break;
          case CELL_HIGHLIGHTED:
            fl_color( color_highlight_ );
            fl_rectf( cx, cy, cell_size_, cell_size_ );
            break;
          case CELL_SELECTED_1:
            fl_color( color_select1_ );
            fl_rectf( cx, cy, cell_size_, cell_size_ );
            break;
          case CELL_SELECTED_2:
            fl_color( color_select2_ );
            fl_rectf( cx, cy, cell_size_, cell_size_ );
            break;
          default:
            break;
        }
        if ( cells_[i].type == CELL_LABEL ){
          fl_font(font_label_,fsize_label_);
          fl_color( Fl::get_color( FL_BACKGROUND2_COLOR ) );
        }
        else{
          fl_font(font_cell_,fsize_cell_);
          fl_color( Fl::get_color( FL_FOREGROUND_COLOR ) );
        }
        fl_draw( cells_[i].str.c_str(), 
          cx+cell_size_/2-2*fl_descent(), cy+cell_size_/2+2*fl_descent() );
        cx += cell_size_;
        if ( cx > x()+cols_*cell_size_ ){
          cy += cell_size_;
          cx = xx;
        }
      }

    } // if !cells_.empty()
  }
  fl_pop_clip();
}


} // namespace aoi_ui
