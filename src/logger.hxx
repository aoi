/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*! \file logger.hxx
*/

#ifndef __LOGGER_HXX
#define __LOGGER_HXX

#include <ctime>
#include <string>
#include <vector>
#include <sstream>
#include <cstdio>
#include <fstream>

using std::string;
using std::vector;

class Logger
{
  public:
    enum MESSAGE_TYPE { MSG_NONE, MSG_MESSAGE, MSG_WARNING, MSG_ERROR, MSG_DEBUG };
    struct LogEntry {
      time_t time;
      string msg;
      MESSAGE_TYPE type;
      /*! 
      * Returns formatted message. Format is:
      *   <b><pre>[ time from the start ] prefix message</pre></b>, 
      *   where prefix is [W] - warning, [E] - error, [D] - debug or none.
      * \return formatted message
      */  
      string str() {
        const char *prefix = " ";
        switch (type){
          case MSG_WARNING: prefix = "[W] ";  break;
          case MSG_ERROR:   prefix = "[E] ";  break;
          case MSG_DEBUG:   prefix = "[D] ";  break;
          default: break;
        }
        std::stringstream ss;
        ss << "[" << time << " s]" << prefix << msg << std::endl;
        return ss.str();
      }
    };

  private:
    MESSAGE_TYPE loglevel_ = MSG_ERROR;
    vector<LogEntry> log_;
    time_t start_;
    string filename_;
    std::ofstream file_;
    

  public:
    //! Constructor.
    Logger(){
      time(&start_);  // XXX: string(ctime()) ends with '\n'
      msg(string("Logger started at ")+string(ctime(&start_))); 
      msg(string("Logger::loglevel = ")+std::to_string(loglevel_)); 
    }
    //! Destructor
    ~Logger(){
      if ( file_.is_open() ){
        time_t t;
        time(&t);
        file_ << "Logger closed at " << ctime(&t) << std::endl;
        file_.close();
      }
    };

    /*!
    * Returns current log (all log entries from the start).
    * \return All log entries from start of the logger (sorted by time).
    */
    inline vector<LogEntry> get_log () const { return log_; };

    /*!
    * Logs one message. Writes message into log file if it is set.
    * Messages with t > loglevel will be not logged.
    * \param s  Message.
    * \param t  Type of the message.
    * \sa LogEntry, MESSAGE_TYPE
    */
    void msg ( const string &s, MESSAGE_TYPE t=MSG_MESSAGE ){
      if ( s.empty() )
        return;
  
      time_t timer;
      time( &timer );
      LogEntry entry =  { timer-start_, s, t };
      log_.push_back( entry );
  
      if ( entry.type > loglevel_ )
        return;
  
      printf( "%s", entry.str().c_str());

      if ( !filename_.empty() ){
        if ( !file_.is_open() )
          file_.open( filename_, std::ofstream::out|std::ofstream::app );
        file_ << entry.str();
      }
        
    }
 
    /*!
    * Returns current loglevel.
    * \return Current loglevel.
    */
    inline MESSAGE_TYPE loglevel () const { return loglevel_; }  
    /*!
    * Sets loglevel.
    * \param level new loglevel
    */
    inline void loglevel ( MESSAGE_TYPE level=MSG_ERROR ){ 
      msg(string("Logger::loglevel set to ") + std::to_string(level) );
      loglevel_ = level; 
    }
    /*!
    * Sets loglevel from string.
    * If s is not recognized shows warning and keep current loglevel.
    * \param s  none, message, warning, error or debug
    */
    inline void loglevel ( const string &s ){
      MESSAGE_TYPE level = MSG_MESSAGE;
      if ( s == "none" )
        level = MSG_NONE;
      else if ( s == "message" )
        level = MSG_MESSAGE;
      else if ( s == "warning" )
        level = MSG_WARNING;
      else if ( s == "error" )
        level = MSG_ERROR;
      else if ( s == "debug" )
        level = MSG_DEBUG;
      else {
        msg( "Unknown loglevel: "+s, MSG_WARNING );
        return;
      }
      loglevel( level );
    }

    /*!
    * Returns name of the log file.
    * \return  name of the log file
    */
    inline string filename () const { return filename_; };
    /*!
    * Sets name (path) of the log file. The file is rewritten every start of Logger.
    * Writes whole current log to file.
    * \param path to log file
    */
    inline void filename ( const string &s ) { 
      filename_=s; 
      if ( !filename_.empty() ){
        if ( !file_.is_open() )
          file_.open( filename_, std::ofstream::out|std::ofstream::app );
        for ( auto &rec: log_ )
          file_ << rec.str();
      }

    }
    
    inline void msg ( const std::stringstream &ss, MESSAGE_TYPE t=MSG_MESSAGE )
      { msg(ss.str(),t); }

};

#endif //__LOGGER_HXX
