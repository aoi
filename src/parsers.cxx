/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "parsers.hxx"
#include "utils.hxx"
#include "logger.hxx"

#include <cstring>

namespace parsers {

using utils::to_string;

//////////////////////////////////////////////////////////////////////////
// STATIC

vector<string> BaseParser::get_elements ( xml_node<> *parent, const char *element, bool unreference )
{
  vector<string> out;
  auto *elt = parent->first_node(element);
  while ( elt != 0 ){
    string s = elt->value();
    if ( unreference ){
      size_t start = (s.front()=='&');                  // erase & from the beggining of elt 
      size_t end = s.size() - start - (s.back()==';');  // erase ; from the end of elt
      s = string(s,start,end);
    }
    if ( !s.empty() )
      out.push_back( s );
    elt = elt->next_sibling(element);
  }
  return out;
}

//////////////////////////////////////////////////////////////////////////

BaseParser::BaseParser ( const char *filename )
{
  file_.open( filename ); 

  if ( !file_.is_open() )
    throw  utils::CantOpenFile(filename);

  // size of file
  file_.seekg( 0, file_.end );
  int size = file_.tellg();
  file_.seekg( 0, file_.beg );
  
  if ( size == 0 ){
    std::stringstream ss;
    ss << "Source file has zero length. (" << filename << ")";
    throw utils::ParsingError( ss.str() );
  }
  
  // read file
  buffer_ = new char[size+1];
  file_.read( buffer_, size );
  int read = file_.gcount();
  
  if ( read != size )
    throw utils::ParsingError("Only " + std::to_string(read) + " bytes was read.");
  
  // DOCTYPE node contains ENTITY definitions
  doc_.parse<rapidxml::parse_doctype_node>( buffer_ );
}


BaseParser::~BaseParser ()
{
  file_.close();
  delete[] buffer_;
}


map<string,string> BaseParser::get_entities () 
{
  map<string,string> m;
  auto *node = doc_.first_node();
  while ( node ){
    if ( node->type() == rapidxml::node_doctype ){
      char *pos = strstr( node->value(), "<!ENTITY " );
      while ( pos ){
        char buff[1024];
        size_t i = 0;
        pos += 9; // strlen("<!ENTITY ")
        // entity name
        while ( *pos != ' ' ) buff[i++] = *pos++;
        buff[i] = '\0';
        string abbr(buff);
        // skip spaces
        while ( *pos == ' ' ) pos++;
        // entity description
        i = 0;
        while ( *pos != '>' ) buff[i++] = *pos++;
        buff[i] = '\0';
        m[abbr] = string(buff);
        pos = strstr( pos, "<!ENTITY" );
      }
    }
    node = node->next_sibling();
  }
  return m;
}

//////////////////////////////////////////////////////////////////////////
// JmdictParser

string JmdictParser::get_version ()
{
  int n = 0;
  string s = "";
  file_.seekg( 0, file_.beg );
  while ( file_.good() && n < 1000  ){
    file_ >> s;
    if ( s == "Rev" ){
      string ver;
      file_ >> ver;
      return s + " " + ver; 
     }
    n++;
  }
  return "NONE";
}


DicWord JmdictParser::get_entry ()
{

  if ( !entry_ ){
    return DicWord();
  }

  int did = -1;
  vector<ElementKanji> vk_ele;
  vector<ElementReading> vr_ele;
  vector<ElementSense> vs_ele;

  // dictionary id
  did = std::stoi( entry_->first_node("ent_seq")->value() );

  // KANJI (element k_ele)
  // Elements of interest (name, count, description):
  //    keb       1   kanji
  //    ke_inf    0+  informations about keb
  //    ke_pri    0+  if not empty: keb can be considered "common" or "frequent"
  auto k_ele = entry_->first_node("k_ele");
  while ( k_ele != 0 ){
    auto keb = k_ele->first_node("keb");
    auto ke_inf = get_elements( k_ele, "ke_inf", true );
    bool freq = ( k_ele->first_node("ke_pri") != 0 );
    vk_ele.push_back( { n_kanji_++, keb->value(), ke_inf, freq } );
    k_ele = k_ele->next_sibling("k_ele");
  }

  // READING (element r_ele)
  // Elements of interest (name, count, description):
  //    reb         1     reading
  //    re_nokanji  0-1   if 1: reb cannot be regarded as a true reading of the kanji
  //    re_restr    0+    reb only applies for this keb 
  //    re_inf      0+    informations about reb
  //    re_pri      0+    if not empty: reb can be considered "common" or "frequent"
  auto r_ele = entry_->first_node("r_ele");
  while ( r_ele != 0 ){
    auto reb = r_ele->first_node("reb");
    auto re_restr = get_elements( r_ele, "re_restr" );
    auto re_inf = get_elements( r_ele, "re_inf", true );
    bool re_nokanji = ( r_ele->first_node("re_nokanji") != 0 );
    bool freq = ( r_ele->first_node("re_pri") != 0 );
    vr_ele.push_back( {n_reading_++, reb->value(), re_nokanji, re_restr, re_inf, freq } );
    r_ele = r_ele->next_sibling("r_ele");
  }

  // SENSE (element r_ele)
  // Elements of interest (name, count, description):
  //    gloss   0+  
  //    stagk   0+  
  //    stagr   0+  
  //    pos     0+  
  //    xref    0+  
  //    ant     0+  
  //    field   0+  
  //    misc    0+  
  //    dial    0+  
  //    s_inf   0+  
  auto *sense = entry_->first_node("sense");
  while ( sense != 0 ){
    auto gloss  = get_elements( sense, "gloss" );
    auto stagk  = get_elements( sense, "stagk" );
    auto stagr  = get_elements( sense, "stagr" );
    auto pos    = get_elements( sense, "pos", true );
    auto xref   = get_elements( sense, "xref" );
    auto ant    = get_elements( sense, "ant" );
    auto field  = get_elements( sense, "field", true );
    auto misc   = get_elements( sense, "misc", true );
    auto dial   = get_elements( sense, "dial", true );
    auto s_inf  = get_elements( sense, "s_inf" );
    vs_ele.push_back( {n_sense_++,gloss,stagk,stagr,pos,xref,ant,field,misc,dial,s_inf} );
    n_gloss_ += gloss.size();
    sense = sense->next_sibling("sense");
  }
  entry_ = entry_->next_sibling("entry");
  n_entries_++;

  return {did,vk_ele,vr_ele,vs_ele};
}

//////////////////////////////////////////////////////////////////////////
// KanjidicParser

string KanjidicParser::get_version ()
{
  auto header = doc_.first_node("kanjidic2")->first_node("header");
  const char *version = header->first_node("database_version")->value();
  const char *date = header->first_node("date_of_creation")->value();
  std::stringstream ss;
  ss << version << " (" << date << ")";
  return ss.str();
}


Kanji KanjidicParser::get_entry ()
{
  Kanji k;

  if ( !entry_ ){
    return k;
  }

  k.kanji ( entry_->first_node("literal")->value() );

  auto *cp_value = entry_->first_node("codepoint")->first_node("cp_value");
  while ( cp_value ){
    auto *type = cp_value->first_attribute("cp_type")->value();
    if ( !strcmp( type, "ucs" ) )
      k.ucs ( cp_value->value() );
    else if ( !strncmp( type, "jis", 3 ) )
      k.flags( type );
    cp_value = cp_value->next_sibling();
  }

  auto *rad_value = entry_->first_node("radical")->first_node("rad_value");
  while ( rad_value ){
    auto *type = rad_value->first_attribute("rad_type")->value();
    if ( !strcmp( type, "classical" ) )
      k.rad_classic( rad_value->value() );
    else if ( !strcmp( type, "nelson_c" ) )
      k.rad_nelson( rad_value->value() );
    rad_value = rad_value->next_sibling();
  }


  auto *query_code = entry_->first_node("query_code");
  if ( query_code ) {
    auto *q_code = query_code->first_node("q_code");
    while ( q_code ){
      auto *type = q_code->first_attribute("qc_type")->value();
      auto *misclass = q_code->first_attribute("skip_misclass");
      if ( !strcmp( type, "skip" ) ){
        vector<int> v = utils::split_string_int(q_code->value(),"-");
        if ( v.size() != 3 ){
          char msg[512];
          snprintf( msg, 512, "Kanjiparser: Wrong SKIP %s. (U+%s)", 
            q_code->value(), k.ucs().c_str());
          throw utils::ParsingError(msg);
        } 
        k.skip( v, misclass ? misclass->value():0 );
      }
      q_code = q_code->next_sibling();
    }
  }

  auto *misc  = entry_->first_node("misc");
  auto *freq  = misc->first_node("freq");
  auto *jlpt  = misc->first_node("jlpt");
  auto *grade = misc->first_node("grade");
  k.strokes( misc->first_node("stroke_count")->value() );
  if ( freq )  k.freq( freq->value() );
  if ( jlpt )  k.jlpt( atoi(jlpt->value()) );
  if ( grade ) k.grade( atoi(grade->value()) );

  auto *reading_meaning = entry_->first_node("reading_meaning");
  if ( reading_meaning ){
    auto *rmgroup = reading_meaning->first_node("rmgroup");
    if ( rmgroup ){
      auto *reading = rmgroup->first_node("reading");
      while( reading ){
        auto *type = reading->first_attribute("r_type");
        if ( type ){
          if ( !strcmp( type->value(), "ja_on") )
            k.onyomi( reading->value() );
          else if ( !strcmp( type->value(), "ja_kun") )
            k.kunyomi( reading->value() );
        }
        reading = reading->next_sibling();
      }
      auto *meaning = rmgroup->first_node("meaning");
      while ( meaning ){
        auto *fattr = meaning->first_attribute("m_lang");
        if ( !fattr ) // english meaning has no m_lang attribute
          k.meaning( meaning->value() );
        meaning = meaning->next_sibling();
      }
    }
    k.nanori( get_elements( reading_meaning, "nanori") ); 
  }
 

  entry_ = entry_->next_sibling("character");
  n_entries_++;

  return k;
}


} // namespace parsers
