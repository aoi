/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gui_dialogs.hxx"
#include <FL/fl_ask.H>
#include <FL/Fl_Color_Chooser.H>
#include <cstring>
#include <map>

namespace aoi_ui {

bool DialogDownload::abort_download_ = false;

//////////////////////////////////////////////////////////////////////////
// DialogDownload

DialogDownload::DialogDownload( int w, int h ) : Fl_Double_Window(w,h)
{
  l_url_    = new Fl_Box( 5, 5, 70, 25, "URL");
  l_fname_  = new Fl_Box( 5, 30, 70, 25, "Filename");
  b_url_    = new Fl_Button( 85, 5, 310, 25);
  b_fname_  = new Fl_Button( 85, 30, 310, 25);
  progress_ = new Fl_Progress( 5, 60, w-10, 30 );
  b_dload_  = new Fl_Button( 5, 95, 80, 25, "Download" );
  b_abort_  = new Fl_Button( w-65, 95, 60, 25, "Close");

  l_url_->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  l_fname_->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  b_url_->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT|FL_ALIGN_CLIP);
  b_fname_->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT|FL_ALIGN_CLIP);

  b_url_->callback( scb_url_callback, (void*)this );
  b_fname_->callback( scb_fname_callback, (void*)this );
  b_abort_->callback( scb_abort, (void*)this);
  b_dload_->callback( scb_download, (void*)this);
  progress_->minimum(0);
  progress_->maximum(100);

  end();
  label("Download file");
  set_modal();
  callback( scb_close_window, (void*)this );

  curl_global_cleanup();
  curl_global_init(CURL_GLOBAL_ALL);
}


DialogDownload::~DialogDownload()
{
  curl_global_cleanup();
}


void DialogDownload::download_url( const char *url, const char *fname )
{
  CURL *curl = nullptr;
  CURLcode res;
  FILE *outfile = nullptr;

  b_abort_->label("Abort");
  downloading_ = true;

  curl = curl_easy_init();
  if(curl)
  {
    outfile = fopen( fname, "wb");
    curl_easy_setopt(curl, CURLOPT_URL, url);
    #ifdef DEBUG
      curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    #endif
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_FILE, outfile);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, mycurl_write_func);
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, mycurl_progress_func );
    curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, (void*)progress_);


    res = curl_easy_perform(curl);
    downloading_ = false;
    b_abort_->label("Close");
    abort_download_ = false;

    if ( res != CURLE_OK ){
      if ( res == 42 )  // aborted by callback
        progress_->label("Aborted");
      else 
        fl_alert( "CURL ERROR %d: %s\n", res, curl_easy_strerror(res) );
    }
    else{
      progress_->label("Done");
      hide();
    }
    fclose(outfile);
    curl_easy_cleanup(curl);
  }
}


void DialogDownload::set_names ( const string &url, const string &fname )
{
  downloading_ = false;
  progress_->value(0.0);
  url_ = url;
  fname_ = ( fname.empty() ) ? utils::guess_fname_from_url(url_):fname;
  b_url_->label( url_.c_str() ); 
  b_fname_->label( fname_.c_str() );
}


void DialogDownload::abort (){
  if ( downloading_ ) {
    if ( !fl_choice("Abort download?", "Abort", "No", 0) ){
      abort_download_ = true;
    }
  }
  else {
    hide();
  }
}


void DialogDownload::cb_download () 
{
  if (url_.empty() || fname_.empty() ){
    fl_alert("You must set URL and filename.");
    return;
  }
  download_url( url_.c_str(), fname_.c_str() );
}


void DialogDownload::cb_url_callback ( )
{
  const char *ret = fl_input("New URL:", b_url_->label());
  if ( ret ){
    url_ = ret;
    b_url_->label(url_.c_str());
  }
  fname_ = utils::guess_fname_from_url(url_);
  b_fname_->label( fname_.c_str() );
}


void DialogDownload::cb_fname_callback ( )
{
  const char *ret = fl_input("New filename:", b_fname_->label());
  if ( ret ){
    fname_ = ret;
    b_fname_->label(fname_.c_str());
  }
}


//////////////////////////////////////////////////////////////////////////
// DialogEditWord

DialogEditWord::DialogEditWord ( int w, int h, const char *l )
  : Fl_Double_Window(w,h,l)
{
  Fl_Box *tmp1 = new Fl_Box( 5, 5, 100, 30, "JMDict id:");
  l_did_    = new Label( 105, 5, 60, 30 );
  view_     = new Fl_Help_View(5, 35, w-10, h-80);
  b_cancel_ = new Fl_Button( w-65, h-35, 60, 30, "Close");
  end();
  tmp1->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  tmp1->labelfont( FL_BOLD );
  b_cancel_->callback( scb_cancel, (void*)this );
  view_->textsize(16);
//  set_modal();
}

//////////////////////////////////////////////////////////////////////////
// ManageDBDialog

ManageDBDialog::ManageDBDialog ( int w, int h, const char *l )
  : Fl_Double_Window(w,h,l)
{
  Fl_Box *tmp1 = new Fl_Box( 5, 5, 100, 30, "Main database");
  tmp1->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  tmp1->labelfont( FL_BOLD );
  b_dload_ = new Fl_Button ( w-85, 5, 80, 20, "Download");

  Fl_Box *l_main_ = new Fl_Box( 15, 25, 100, 30, "Version" );
  l_main_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  l_main_->labelfont( FL_ITALIC );
  lver_main_ = new Label( 150, 25, 100, 30, "---");
  lver_main_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  b_check_main_ = new Fl_Button( w-65, 25, 60, 20, "Check");
  b_check_main_->callback( scb_check_main, (void*)this );
  b_check_main_->deactivate();

  Fl_Box *l_created_ = new Fl_Box( 15, 45, 100, 30, "Created");
  l_created_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  l_created_->labelfont( FL_ITALIC );
  l_main_created_ = new Label( 150, 45, 100, 30, "---");
  l_main_created_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );

  Fl_Box *l_jmdict_ = new Fl_Box( 15, 65, 100, 30, "JMdict version" );
  l_jmdict_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  l_jmdict_->labelfont( FL_ITALIC );
  lver_jmdict_ = new Label( 150, 65, 100, 30, "---");
  lver_jmdict_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );

  Fl_Box *l_kanjidic_ = new Fl_Box( 15, 85, 100, 30, "Kanjidic2 version" );
  l_kanjidic_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  l_kanjidic_->labelfont( FL_ITALIC );
  lver_kanjidic_ = new Label( 150, 85, 100, 30, "---");
  lver_kanjidic_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );

  Fl_Box *l_kradfile_ = new Fl_Box( 15, 105, 100, 30, "Kradfile version" );
  l_kradfile_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  l_kradfile_->labelfont( FL_ITALIC );
  lver_kradfile_ = new Label( 150, 105, 100, 30, "---");
  lver_kradfile_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );

  Fl_Box *l_tatoeba_ = new Fl_Box( 15, 125, 100, 30, "Tatoeba version" );
  l_tatoeba_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  l_tatoeba_->labelfont( FL_ITALIC );
  lver_tatoeba_ = new Label( 150, 125, 100, 30, "---");
  lver_tatoeba_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );

  Fl_Box *tmp2 = new Fl_Box( 5, 165, 100, 30, "User database");
  tmp2->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  tmp2->labelfont( FL_BOLD );

  Fl_Box *l_against_ = new Fl_Box( 15, 185, 100, 30, "Checked against" );
  l_against_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  l_against_->labelfont( FL_ITALIC );
  l_checked_against_ = new Label( 150, 185, 100, 30, "---" );
  l_checked_against_->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );
  b_check_user_ = new Fl_Button ( w-65, 185, 60, 20, "Check");
  b_check_user_->callback( scb_check_user, (void*)this );
  b_check_user_->deactivate();

  /*
  TITLE                 N_VALUES      EDIT_BTN
  user notes - kanji    #             edit_btn
  user notes - words    #             edit_btn
  user flags - kanji    #             edit_btn
  user flags - words    #             edit_btn
  ...
  */

  b_close_ = new Fl_Button( w-65, h-25, 60, 20, "Close" );
  b_close_->callback( scb_close, (void*)this );
  end();
  set_modal();
}


//////////////////////////////////////////////////////////////////////////
// KanjiPopupWindow

KanjiPopupWindow::KanjiPopupWindow ( int w, int h ) : Fl_Menu_Window(w,h)
{
  box_ = new KanjiInfo(0,0,w,h);
  box_->autoresize(true);
  box_->box(FL_BORDER_BOX);
  this->label("Kanji details");
}


int KanjiPopupWindow::handle ( int event )
{
  switch(event) {
    case FL_PUSH:
      return 1;
    case FL_DRAG: {
        int xr = Fl::event_x_root();
        int yr = Fl::event_y_root();
        position( xr, yr );
      }
      return 1;
    default:
      return Fl_Widget::handle(event);
  }
}


//////////////////////////////////////////////////////////////////////////
// DialogSettings

DialogSettings::DialogSettings( int w, int h ) : Fl_Double_Window(w,h)
{
  browser_  = new Fl_Hold_Browser( 5, 5, w-10, h-45);
  b_ok_     = new Fl_Button( w-65, h-35, 60, 30, "OK" );
  b_defaults_= new Fl_Button( 5, h-35, 60, 30, "Defaults");
  browser_->format_char('@');
  resizable(browser_);
  end();
  label("Settings");
  set_modal();
  browser_->callback(static_callback, (void*)this);
  b_ok_->callback( static_ok, (void*)this );
  b_defaults_->callback( scb_restore_defaults, (void*)this );
}


void DialogSettings::set ( const std::map<string,Config::Value> &m, const std::map<string,Config::Value> &defaults ){
  browser_->clear();
  data_ = m;
  defaults_ = defaults.empty() ? m:defaults;
  for ( auto mi: data_ ){
    string s = "@r@_";
    if ( mi.second.type == Config::ValueType::COLOR ){
      char buff[32];
      sprintf(buff, "%s", mi.second.val.c_str() );
      s += buff;
    }
    else
      s += mi.second.val;
    browser_->add( mi.first.c_str(), (void*)(mi.first.c_str()) );
    browser_->add( s.c_str(), (void*)(mi.first.c_str()) );
  }
}


std::map<string,Config::Value> DialogSettings::run ()
{
  show();
  while ( visible() ){
    usleep(1000);
    Fl::check();
  }
  return data_;
}


int DialogSettings::handle ( int event )
{
  if ( event==FL_KEYDOWN && (Fl::event_key()==FL_Enter || Fl::event_key()==' ') ){
    int line = browser_->value();
    if ( line > 0 ){
      cb(line);
      return 1;
    }
  }
  return Fl_Double_Window::handle( event );
}


void DialogSettings::cb ( int line )
{
  // not double click nor ENTER/Spacebar
  if ( Fl::event_key()!=FL_Enter && Fl::event_key()!=' ' && !Fl::event_clicks() )
    return;

  const char *var = (const char*)(browser_->data(line));
  Config::Value *val = &data_.at(var);

  fl_message_title(var);

  // value clicked, display appropriate dialog
  if ( line % 2 == 0 ){
    switch ( val->type ){
      case Config::ValueType::BOOL:
      case Config::ValueType::INT:
      {
        const char *res = fl_input( var, val->val.c_str());
        if ( !res )
          break;
        if ( !utils::isint(res) ){
          fl_alert("Only integers allowed.");
          break;
        };
        val->val = res;
        char buff[32];
        snprintf( buff, 32, "@r@_%s", res);
        browser_->text( line, buff );
        break;
      }
      case Config::ValueType::FONT:
      {
        DialogFontTest df;
        df.set_modal();
        int f = df.run();
        if ( f == -1 )
          break;
        val->val = Fl::get_font_name(f);
        char buff[128];
        snprintf( buff, 128, "@r@_%s", val->val.c_str());
        browser_->text( line, buff );
        break;
      }
      case Config::ValueType::COLOR:
      { 
        uchar r = 0;
        uchar g = 0;
        uchar b = 0;
        std::stringstream ss;
        ss << std::hex << val->val.substr(2);
        Fl_Color c;
        ss >> c;
        Fl::get_color( c, r, g, b);
        if ( fl_color_chooser("Select color", r, g, b ) ){
          char buff[32];
          int col = fl_rgb_color(r,g,b);
          snprintf( buff, 32, "0x%08x", col);
          val->val = buff;
          snprintf( buff, 32, "@r@_0x%08x", col);
          browser_->text( line, buff );
        }
        break;
      }
      default:
      {
        const char *res = fl_input( var, val->val.c_str());
        if ( res ){
          val->val = res;
          char buff[256];
          snprintf( buff, 256, "@r@_%s", res);
          browser_->text( line, buff );
        }
        break;
      }
    }
  }
  // variable name clicked - show description
  else {
    fl_message("%s",val->desc.c_str());
  }
}

} // namespace aoi_ui
