/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gui.hxx"
#include <sstream>
#include "utils.hxx"


namespace aoi_ui {

const char *LABEL_DICTIONARY  = "D";
const char *LABEL_KANJI       = "K";

//////////////////////////////////////////////////////////////////////////
// DialogRomanizationTest

DialogRomanizationTest::DialogRomanizationTest( int w, int h ) 
  : Fl_Double_Window(w,h){
  choice_ = new Fl_Choice( 5, 5, w-10, 25 );
  choice_->add("Hiragana");
  choice_->add("Katakana");
  choice_->value(0);
  choice_->callback( scb_input, (void*)this );
  input_  = new Fl_Input( 5, 35, w-10, 30 );
  input_->callback( scb_input, (void*)this );
  input_->when(FL_WHEN_CHANGED);
  input_->take_focus();
  output_ = new Fl_Input( 5, 75, w-10, h-105 );
  output_->align( FL_ALIGN_INSIDE|FL_ALIGN_WRAP);
  output_->textfont( FONT_KANJI );
  output_->textsize( 2*input_->labelsize() );
  output_->box(FL_BORDER_BOX);
  label("Romanization test");
  b_close_ = new Fl_Button( w-65, h-25, 60, 20, "Close");
  b_close_->callback( scb_close, (void*)this );
  end();
  set_modal();
}


void DialogRomanizationTest::cb_input (){
  const char *s = input_->value();
  if ( choice_->value() == 0 )
    buff_ = App::get()->rmn()->romaji_to_hiragana(s).c_str();
  else
    buff_ = App::get()->rmn()->romaji_to_katakana(s).c_str();
  output_->value( buff_.c_str() );
}


MainWindow::MainWindow ( GUI *parent, int w, int h ): Fl_Double_Window(w,h) 
{
  parent_ = parent;

  // DICTIONARY
  grp_dictionary_ = new Fl_Group(0,0,w,h);  
  {
    Fl_Group *grp = new Fl_Group(60,5,200,30);
    dic_input_ = new Entry( 60, 5, 200, 30, "Insert romaji/kanji" );
    grp->resizable(0);
    grp->end();
  }
  {
    dic_view_ = new DicView( 5, 40, 700, 555 );
    dic_view_->col_width(0,200);
    dic_view_->col_width(1,200);
    // width of the last column is determined dynamically
    dic_view_->end();
    cb_noun_ = new Fl_Check_Button(710, 40, 50, 20, "noun");
    cb_verb_ = new Fl_Check_Button(710, 65, 50, 20, "verb");
    cb_adj_ = new Fl_Check_Button(710, 90, 50, 20, "adj");
    cb_expr_ = new Fl_Check_Button(710, 115, 50, 20, "expr");
  }
  grp_dictionary_->end();

  // KANJIDIC
  grp_kanjidic_ = new Fl_Group( 0, 0, WIN_WIDTH, WIN_HEIGHT );
  {
    Fl_Group *grp   = new Fl_Group(60,5,WIN_WIDTH-60-60-2*5,30);
    e_skip          = new Entry( 60, 5, 120, 30, "SKIP" );
    e_strokes       = new Entry( 185, 5, 70, 30, "#" );
    e_jlpt          = new Entry( 260, 5, 40, 30, "JLPT");
    e_grade         = new Entry( 305, 5, 40, 30, "Grd.");
    knj_orderby_    = new Fl_Choice( 350, 5, 150, 30 );
    cb_knj_jis208   = new Fl_Check_Button( 505, 5, 100, 30, "JIS 208 only" );
    b_knj_search_   = new Fl_Button( 615, 5, 60, 30, "Search");
    b_knj_clear_    = new Fl_Button( 680, 5, 60, 30, "Clear");
    l_knj_results   = new Label( 555, 565, 240, 30 );
    grp->resizable(0);
    grp->end();
  }
  {
    knj_view_ = new KanjiView( 5, 40, 545, 555 );
    cb_knj_components = new Fl_Check_Button( 555, 40, 250, 25, "Sort by strokes");
    compo_view_ = new ComponentView( 555, 70, 240, 300); 
  }
  grp_kanjidic_->end();
  grp_kanjidic_->hide();

  // ALWAYS VISIBLE BUTTONS
  {
    int b_width = 10+fl_width( LABEL_DICTIONARY );
    if ( b_width < 30 )
      b_width = 30;
    b_toggle_view_  = new Fl_Button( w-b_width-5, 5, b_width, 30, LABEL_DICTIONARY );
    Fl_Group *g     = new Fl_Group( 5, 5, 50, 30 );
    b_menu_         = new Fl_Menu_Button( 5, 5, 50, 30, "M");
    g->end();
    g->resizable(0);
  }

  resizable( dic_view_ );
  resizable( knj_view_ );
  end();
}


int MainWindow::handle ( int event )
{
  unsigned int mods = Fl::event_state() & (FL_META|FL_CTRL|FL_ALT);
  if ( event == FL_KEYDOWN && mods==FL_CTRL ){
    switch ( Fl::event_key() ){
      // Ctrl-L
      case 'l':
        if ( grp_dictionary_->visible() )
          dic_input_->take_focus();  
        else
          e_skip->take_focus();
        return 1;
      // Ctrl-Q
      case 'q':
        parent_->cb_toggle_group(); 
        return 1;
      // Ctrl-S
      case 's':
        if ( grp_dictionary_->visible() )
          dic_input_->callback()(dic_input_,(void*)App::get());
        else
          b_knj_search_->callback()(dic_input_,(void*)App::get());
        return 1;
      // Ctrl-M
      case 'm':
        b_menu_->popup();
        return 1;
      default:
        break;
    }
  }
  return Fl_Double_Window::handle(event);
}


GUI::GUI ( aoi::App *parent ):parent_(parent)
{
  Fl::visual(FL_DOUBLE|FL_INDEX);
  init_colors();

  dlg_fonts_      = new DialogFontTest();
  dlg_progress_   = new DialogProgress(300,50);
  dlg_help_       = new Fl_Help_Dialog();
  dlg_edit_word_  = new DialogEditWord();
  dlg_manage_db_  = new ManageDBDialog();
  dlg_download_   = new DialogDownload(400,125);

  main_window_  = new MainWindow( this, WIN_WIDTH, WIN_HEIGHT );
  main_window_->label( WIN_TITLE );
  main_window_->callback(scb_main_window_);

  // populate 
  // should be in same order as aoi::KANJI_SORT_MODE
  main_window_->knj_orderby_->add("Freq (ASC)");
  main_window_->knj_orderby_->add("Freq (DESC)");
  main_window_->knj_orderby_->add("Strokes (ASC)");
  main_window_->knj_orderby_->add("Strokes (DESC)");
  main_window_->knj_orderby_->value(0);

  main_window_->cb_knj_jis208->value( parent_->get_config<bool>("knj/jis208_only") );

  // populate menu
  main_window_->b_menu_->add("Manage database", 0, App::scb_manage_db, (void*)parent_ );
  main_window_->b_menu_->add("Settings", 0, scb_show_settings, (void*)this);
  main_window_->b_menu_->add("Tools/Test romanization", 0, 
    scb_test_romanization, (void*)this);
  main_window_->b_menu_->add("Tools/Test fonts", 0, 
    scb_test_fonts, (void*)this);
  main_window_->b_menu_->add("Help && About", 0, scb_show_help, (void*)this);
  main_window_->b_menu_->add("Quit", 0, quit_fltk, (void*)main_window_ );

  // callbacks
  main_window_->dic_input_->callback( App::scb_dic_input, (void*)parent_ );
  main_window_->dic_view_->cb_doubleclick( App::scb_dicview_doubleclick, (void*)parent_ );
  main_window_->dic_view_->cb_rightclick( App::scb_dicview_rightclick, (void*)parent_ );
  main_window_->cb_noun_->callback( App::scb_filter_listview, (void*)parent_ );
  main_window_->cb_verb_->callback( App::scb_filter_listview, (void*)parent_ );
  main_window_->cb_adj_->callback( App::scb_filter_listview, (void*)parent_ );
  main_window_->cb_expr_->callback( App::scb_filter_listview, (void*)parent_ );
  main_window_->e_skip->callback( App::scb_kanji_search, (void*)parent_);
  main_window_->knj_orderby_->callback( App::scb_kanji_search, (void*)parent_);
  main_window_->b_knj_search_->callback( App::scb_kanji_search, (void*)parent_);
  main_window_->b_knj_clear_->callback( scb_knj_clear, (void*)this);
  main_window_->knj_view_->cb_leftclick( App::scb_kanjiview_select, (void*)parent_ );
  main_window_->knj_view_->cb_select( App::scb_kanjiview_select, (void*)parent_ );
  main_window_->compo_view_->callback( App::scb_kanji_search, (void*)parent_ );
  main_window_->b_toggle_view_->callback( scb_toggle_group, (void*)this );
  main_window_->e_strokes->callback( App::scb_kanji_search, (void*)parent_);
  main_window_->e_jlpt->callback( App::scb_kanji_search, (void*)parent_);
  main_window_->e_grade->callback( App::scb_kanji_search, (void*)parent_);
  main_window_->cb_knj_jis208->callback( scb_jis208_toggle, (void*)this);
  main_window_->cb_knj_components->callback( App::scb_set_components , (void*)parent_ );

  // other 
  main_window_->dic_input_->when(FL_WHEN_ENTER_KEY_ALWAYS);
  main_window_->b_menu_->labeltype(FL_NORMAL_LABEL);
  init_fonts();

  main_window_->dic_input_->take_focus();
}


GUI::~GUI ()
{
  delete dlg_download_;
  delete dlg_manage_db_;
  delete dlg_edit_word_;
  delete dlg_help_;
  delete dlg_progress_;
  delete dlg_fonts_;
}


void GUI::init_colors ()
{
  parent_->log("Initializing colors.");

  Fl_Color col = parent_->get_color("foreground");
  Fl::foreground(col>>24&0xFF, col>>16&0xFF, col>>8&0xFF);

  col = parent_->get_color("background");
  Fl::background(col>>24&0xFF, col>>16&0xFF, col>>8&0xFF);

  col = parent_->get_color("background2");
  Fl::background2(col>>24&0xFF, col>>16&0xFF, col>>8&0xFF);

  col = parent_->get_color("selection");
  Fl::free_color(FL_SELECTION_COLOR);
  Fl::set_color(FL_SELECTION_COLOR, col>>24&0xFF, col>>16&0xFF, col>>8&0xFF);

  // redraw widgets
  if ( !main_window_ )
    return;

  for ( int i=0; i < main_window_->children(); i++ )
    main_window_->child(i)->redraw();
  Fl::check();
}


void GUI::init_fonts () 
{
  Fl::set_font(FL_SYMBOL,fontname_kanji_.c_str()); // used in Fl_Help_Dialog
  Fl::set_font(FONT_KANJI, fontname_kanji_.c_str());
  if ( dlg_help_ )
    dlg_help_->textsize( font_base_size_ );
  if ( main_window_->dic_view_ ){
    main_window_->dic_view_->font_size( font_base_size_ );
    main_window_->dic_view_->redraw();
  }
  if ( main_window_->knj_view_ ) {
    main_window_->knj_view_->font_size( 3*font_base_size_ );
    main_window_->knj_view_->redraw();
  }
  if ( main_window_->compo_view_ ) {
    main_window_->compo_view_->font_cell( FONT_KANJI, font_base_size_*1.2 );
    main_window_->compo_view_->font_label( FL_HELVETICA_BOLD, font_base_size_*1.2 );
  }
  if ( main_window_->dic_input_ ){
    main_window_->dic_input_->textfont( FONT_KANJI );
    main_window_->dic_input_->textsize( font_base_size_*1.2 );
  }
}


int GUI::run ( int argc, char **argv )
{
  main_window_->show(argc,argv);
  return Fl::run();
}


void GUI::dicview_menu ( int did, const vector<string> &v, bool notes, bool examples )
{
  Fl_Menu_Button menu(Fl::event_x(), Fl::event_y(), 80, 1);
  menu.textfont(FONT_KANJI);
  menu.textsize( 1.5*font_base_size_ );
  for ( string s: v ){
    menu.add( (s+"/Copy").c_str(), 0, scb_copy, (void*)s.c_str());
    menu.add( (s+"/Show").c_str(), 0, scb_menu_show_kanji, (void*)this );
  }
  menu.add("Notes",0,0,0, notes ? 0:FL_MENU_INACTIVE);
  menu.add("Examples",0, App::scb_examples,(void*)parent_, 
                          examples ? 0:FL_MENU_INACTIVE);
  menu.add("Show", 0, App::scb_edit_word, (void*)parent_);
  menu.popup();
}


void GUI::edit_word ( const aoi::DicWord &w )
{
  dlg_edit_word_->set(w);
  dlg_edit_word_->show();
}


void GUI::cb_jis208_toggle ()
{
  parent_->set_config("knj/jis208_only", 
            std::to_string(main_window_->cb_knj_jis208->value()));
  parent_->scb_kanji_search( nullptr, (void*)parent_);
}


void GUI::cb_toggle_group ()
{
  if ( main_window_->grp_dictionary_->visible() ){
    main_window_->grp_dictionary_->hide();
    main_window_->grp_kanjidic_->show();
    main_window_->b_toggle_view_->label(LABEL_KANJI);
  }
  else {
    main_window_->grp_dictionary_->show();
    main_window_->grp_kanjidic_->hide();
    main_window_->b_toggle_view_->label(LABEL_DICTIONARY);
  }
}


void GUI::popup_kanji ( const Kanji &k )
{
  if ( popup_ )
    delete popup_;
  popup_ = new KanjiPopupWindow(100,100);
  popup_->font_a(FL_HELVETICA_BOLD, font_base_size_ );
  popup_->font_b(FL_HELVETICA, font_base_size_ );
  popup_->set(k);
  popup_->popup();
}


void GUI::popup_kanji ( const string &k )
{
  popup_kanji(parent_->db_get_kanji(k));
}


void GUI::cb_knj_clear ()
{
  main_window_->e_strokes->value("");
  main_window_->e_skip->value("");
  main_window_->e_jlpt->value("");
  main_window_->e_grade->value("");
  main_window_->compo_view_->set_data();
}


void GUI::reset_filters ()
{
  main_window_->cb_noun_->value(0);
  main_window_->cb_verb_->value(0);
  main_window_->cb_adj_->value(0);
  main_window_->cb_expr_->value(0);
}


vector<string> GUI::listview_filters ()
{
  vector<string> out;
  if ( main_window_->cb_noun_->value() )
    out.push_back("noun");
  if ( main_window_->cb_verb_->value() )
    out.push_back("verb");
  if ( main_window_->cb_adj_->value() )
    out.push_back("adj");
  if ( main_window_->cb_expr_->value() )
    out.push_back("expr");
  return out;
}


void GUI::show_settings ()
{
  DialogSettings d;
  d.set( parent_->get_config_map(), parent_->get_config_map_default() );
  parent_->save_config( d.run() );
} 

void GUI::change_cursor ( Fl_Cursor c ) 
{ 
  if ( main_window_ && main_window_->visible() )
    main_window_->cursor(c); 
  if ( dlg_manage_db_ && dlg_manage_db_->visible() )
    dlg_manage_db_->cursor(c); 
  Fl::check();
}
} // namespace
