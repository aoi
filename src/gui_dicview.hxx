/*
    Copyright 2013 Karel Matas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _DICVIEW_HXX
#define _DICVIEW_HXX

#include <FL/Fl_Table_Row.H>
#include <FL/fl_draw.H>
#include <map>
#include <string>
#include <vector>
#include <stdexcept>

/*! \file gui_dicview.hxx
* PREDEFINED TAGS:
*   <br>
*   <sep>
*   <default>
*
* \warning FL_FLAT_BOX causes errors in redrawing
*/

namespace aoi_ui {

using std::vector;
using std::map;
using std::string;

/*!
* The style of the part of the text in the DicView.
*/
struct TextStyle
{
  enum Separator {
    SEPARATOR_NONE,
    SEPARATOR_SQUARE
  };

  enum NewlineStyle {
    NEWLINE_NONE,
    NEWLINE_AFTER
  };

  Fl_Font       font;
  float         relsize;      //!< relative size of the text. See DicView::font_size_
  Fl_Color      color;
  int           offset_y;     //!< for subscripts
  Separator     separator;    //!< if not SEPARATOR_NONE draw the separator symbol instead of text
  NewlineStyle  newline;      //!< if not NEWLINE_NONE then append \n at the end of the string

  //! Generic constructor.
  TextStyle( Fl_Font fnt=FL_TIMES , float sz=1.0, Fl_Color clr=FL_BLACK, int offy=0, 
          Separator sep=SEPARATOR_NONE, NewlineStyle nl=NEWLINE_NONE)
  :font(fnt),relsize(sz),color(clr), offset_y(offy), separator(sep),newline(nl){}

  //! Constructor for separators.
  TextStyle( Separator sep ): TextStyle() { separator=sep; }
  //! Constructor for newlines.
  TextStyle( NewlineStyle nl ): TextStyle() { newline=nl; }
};


/*!
* Widget that displays table with 3 columns. Column width can be changed by 
* dragging the headers. Width of the last column is determined automatically.
* The cells are using simple markup language.
*/
class DicView : public Fl_Table_Row
{
  private:
    int font_size_        = 12;           //!< default font size. See font_size()
    int cell_padding_x_   = 5;
    int cell_padding_y_   = 5;
    map<string,TextStyle> tag_table_;     //!< tag_name:style
    vector<string> headers_;              //!< text in the headers

    /*! 
    * Helper function that sets the height <i>H</i> of the row <i>R</i> 
    * and forces its redraw. Called by draw_single_cell() when necessary
    * (i.e. if the row height changes).
    */
    void redraw_row ( int R, int H );

  protected:
    Fl_Callback *cb_leftclick_;
    Fl_Callback *cb_rightclick_;
    Fl_Callback *cb_doubleclick_;
    Fl_Callback *cb_select_;
    void *cb_leftclick_data_      = nullptr;
    void *cb_rightclick_data_     = nullptr;
    void *cb_doubleclick_data_    = nullptr;
    void *cb_select_data_         = nullptr;
    vector<int> cell_heights_;    //!< heights of the cells (size: no. of cells)
    vector<string> data_;         //!< cells strings (size: no. of cells)
    vector<int> ids_;             //!< cell ids (size: no. of cells)

    /*! 
    * Draws the content of a single cell. Parses the cells string, splits it 
    * into words, applies styles, fits the text into the cell (wraps words)
    * \see utils::parse_markup()
    * \see TextStyle
    * \return cell height
    */
    int draw_single_cell ( int R, int C, int X, int Y, int W, int H );

    /*!
    * Draws a cell or adjust the columns depending on the context
    */
    void draw_cell ( TableContext context, int R=0, int C=0, int X=0, int Y=0, 
                     int W=0, int H=0 );

  public:
    DicView(int x, int y, int w, int h, const char *l=0 );
    ~DicView(){};

    /*!
    * Resets view, sets cell data <i>d</i> and row ids <i>cell_ids</i>
    */
    void set_data ( const vector<string> &d, const vector<int> &cell_ids={} );


    /*!
    * Adds the TextStyle <i>style</i> associated with the tag <i>name</i> 
    * to the tag table.
    */
    inline void register_tag ( const string &name, const TextStyle &style ) 
      { tag_table_[name] = style; };

    //! Returns the TextStyle asociated with the tag <i>tag</i>
    inline TextStyle *get_style ( const string &tag = "default" ) const 
    { 
      auto mi = tag_table_.find(tag);
      if ( mi != tag_table_.end() )
        return const_cast<TextStyle*>(&mi->second);
      return const_cast<TextStyle*>(&tag_table_.at("default"));
    }


    /*! 
    * Copies the text <i>userdata</i> into clipboard and the selection
    * buffer (Unix). Userdata should be const char*.
    */
    static void scb_copy ( Fl_Widget *w, void *userdata ) {
      const char *d = (const char*)userdata;
      int len = strlen(d);
      Fl::copy( d, len, 0);
      Fl::copy( d, len, 1);
    }


    //! returns id of a cell in the row <i>R</i> and column <i>C</i>
    inline int cell_id ( int R, int C ) 
    { 
      int n = R*cols()+C;
      int ret;
      try {
       ret = ids_.at(n);
      } 
      catch ( std::out_of_range ) {
        ret = -1;
      }
      return ret;
    }

    //! Returns id of the selected row (id of the cell in first column)
    inline int selected_row_id () { return cell_id( selected_row(), 0 );};

    //! Returns id of the row <i>R</i> (id of the cell in first column)
    inline int row_id ( int R ) { return cell_id(R,0); };

    //! Returns cell (index) at the position x,y,.
    inline int cell_at_xy ( int x, int y )
    {
      int R = -1;
      int C = -1;
      int w = 0;
      int h = 0;
      // check rows
      for ( int r=0; r < rows(); r++ ){
        int nh = h + row_height(r);
        if ( y>h && y<nh ){
          R = r;
          break;
        }
        h = nh;
      }
      // check columns
      for ( int c=0; c < cols(); c++ ){
        int nw = w + col_width(c);
        if ( x>w && x<nw ) {
          C = c;  
          break;
        }
        w = nw;
      }
      int ret = R*cols()+C;
      if ( R == -1 || C == -1 || ret > int(data_.size())-1 )
        return -1;
      return ret;
    }


    //! Returns row at the y-coordinate.
    inline int row_at_y ( int y ){
      int R = 0;
      int C = 0;
      ResizeFlag flag;
      cursor2rowcol( R, C, flag );  
      return R;
    } 
    
    //! FLTKs handle() function.
    int handle ( int event );

    //! Returns the base size of the text.
    inline int font_size () const { return font_size_;};

    //! Returns x-padding of the cells.
    inline int cell_padding_x () const { return cell_padding_x_; };

    //! Returns y-padding of the cells.
    inline int cell_padding_y () const { return cell_padding_y_; };

    //! Return the selected row (index)
    inline int selected_row () {
      for ( int r=0; r<rows(); r++ )
        if ( row_selected(r) )  
          return r;
      return -1;
    }

    /*! 
    * Sets the base size of the text. All text sizes of the styles are derived 
    * from these value.
    * \see TextStyle::relsize
    */
    inline void font_size ( int s ) { font_size_ = s; }

    //! Sets both x-padding and y-padding to <i>x</i>.
    inline void cell_padding ( int x ) { cell_padding_x_ = x; cell_padding_y_=x;}

    //! Sets the x-padding
    inline void cell_padding_x ( int x ) { cell_padding_x_ = x; }

    //! Sets the y-padding
    inline void cell_padding_y ( int y ) { cell_padding_y_ = y; }

    //! Sets the text strings in the column headers
    inline void headers ( const vector<string> &v ) { headers_=v; }

    /*! 
    * Sets the callback for leftclick. <i>data</i> should be pointer
    * to the master widget (App).
    */
    inline void cb_leftclick  ( Fl_Callback *cb, void *data ) 
      { cb_leftclick_  = cb; cb_leftclick_data_ = data; }

    /*! 
    * Sets the callback for rightclick. <i>data</i> should be pointer
    * to the master widget (App).
    */
    inline void cb_rightclick ( Fl_Callback *cb, void *data ) 
      { cb_rightclick_ = cb; cb_rightclick_data_ = data; }

    /*! 
    * Sets the callback for doubleclick. <i>data</i> should be pointer
    * to the master widget (App).
    */
    inline void cb_doubleclick( Fl_Callback *cb, void *data ) 
      { cb_doubleclick_= cb; cb_doubleclick_data_ = data; }

    /*! 
    * Sets the callback for the selection of a row. 
    * <i>data</i> should be pointer to the master widget (App).
    */
    inline void cb_select     ( Fl_Callback *cb, void *data ) 
      { cb_select_     = cb; cb_select_data_ = data; }

    //! Calls the callback for the leftclick
    inline void cb_leftclick  ( Fl_Widget *w ) const
      { cb_leftclick_(w,cb_leftclick_data_); }
    //! Calls the callback for the rightclick
    inline void cb_rightclick ( Fl_Widget *w ) const 
      { cb_rightclick_(w, cb_rightclick_data_); }
    //! Calls the callback for the doubleclick
    inline void cb_doubleclick( Fl_Widget *w ) const 
      { cb_doubleclick_(w, cb_doubleclick_data_ ); }
    //! Calls the selection of a row
    inline void cb_select     ( Fl_Widget *w ) const 
      { cb_select_(w,cb_select_data_); }

    //! default (empty) callback
    inline static void scb_general ( Fl_Widget *w, void *f ) {}
};

} //namespace aoi_ui
#endif // _DICVIEW_HXX
