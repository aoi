# This Makefile should work under Linux and Mingw32 (in Windows)
#
# Requires C++11 and C99 (only variable length arrays)) compatible compiler 
# (tested with gcc 4.7, clang 3.2)
#
# http://tehsausage.com/mingw-to-string
#
# CLANG does not have -ggdb3

CXX               = clang
CC                = $(CXX)
DEBUG             = -DDEBUG -g -ggdb3
WARNINGS          = -Wall -Wno-vla -Wno-long-long -pedantic

OPT               = -O2
LIBS              = -L/usr/local/lib -L/lib
INCLUDE           = -I/include
CURL_CFLAGS       = $(shell curl-config --cflags)
CURL_LIBS         = $(shell curl-config --libs)
CURL_STATICLIBS   = $(shell curl-config --libs)
FLTK_CXXFLAGS     = $(shell fltk-config --cxxflags) 
FLTK_LIBS         = $(shell fltk-config --use-images --ldflags --libs)
FLTK_STATICLIBS   = $(shell fltk-config --use-images --ldstaticflags --libs)

CFLAGS            = ${WARNINGS}
CXXFLAGS          = ${CURL_CFLAGS} ${INCLUDE} -std=c++11 ${WARNINGS} ${FLTK_CXXFLAGS}
LDFLAGS           = ${LIBS} ${FLTK_LIBS} ${FLTK_LIBS} -lstdc++ -lz ${CURL_LIBS}
LDSTATICFLAGS     = ${LIBS} ${FLTK_STATICLIBS} ${CURL_STATICLIBS} -static-libgcc -static-libstdc++ 

NAME              = aoi 

# OS specific settings
ifeq ($(shell uname), Linux)
else # Windows
  NAME=aoi.exe
  CXX=g++ -DWINDOWS
  CC=gcc 
  # -mwindows: dont use console for stdout/stderr
  # -mconsole
  LDFLAGS=${LDSTATICFLAGS} -mwindows
endif


OBJ=\
  src/3rdparty/sqlite3.o\
  src/config.o\
  src/romanization.o\
  src/aoi.o\
  src/utils.o\
  src/parsers.o\
  src/main.o\
  src/gui.o\
  src/gui_widgets.o\
  src/gui_dialogs.o\
  src/gui_dicview.o\
  src/gui_kanjiview.o

SRC=\
  src/3rdparty/sqlite3.c\
  src/config.cxx\
  src/romanization.cxx\
  src/aoi.cxx\
  src/utils.cxx\
  src/parsers.cxx\
  src/main.cxx\
  src/gui.cxx\
  src/gui_widgets.cxx\
  src/gui_dialogs.cxx\
  src/gui_dicview.cxx\
  src/gui_kanjiview.cxx

HEAD=\
  src/3rdparty/sqlite3.h\
  src/3rdparty/sqlite3ext.h\
  src/3rdparty/rapidxml.hpp\
  src/logger.hxx\
  src/sqlite3.hxx\
  src/config.hxx\
  src/datatypes.hxx\
  src/romanization.hxx\
  src/utils.hxx\
  src/parsers.hxx\
  src/aoi.hxx\
  src/gui_settings.hxx\
  src/gui_widgets.hxx\
  src/gui_dialogs.hxx\
  src/gui.hxx\
  src/gui_dicview.hxx\
  src/gui_kanjiview.hxx

.PHONY: build
.PHONY: debug
.PHONY: distro
.PHONY: clean

build: ${NAME}

debug: OPT=-00
debug: CXXFLAGS += ${DEBUG}
ifneq ($(shell uname),Linux) # show console in Windows
debug: LDFLAGS += -mconsole
endif
debug: build

%.o: %.c
	${CC} ${CFLAGS} ${OPT} -c $< -o $@

%.o: %.cxx
	${CXX} ${CXXFLAGS}  ${OPT} -c $< -o $@

${NAME}: ${OBJ}
	${CXX} ${CXXFLAGS}  ${OPT} ${OBJ} -o ${NAME} ${LDFLAGS}

clean:
	rm -f src/*.o src/3rdparty/*.o src/gui/*.o ${NAME} 2>/dev/null

${OBJ}: ${HEAD}

distro: clean build
	strip -s ${NAME}
