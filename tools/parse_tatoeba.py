#!/usr/bin/python3
#
# Script for converting example sentences from tatoeba project ( tatoeba.org )
# from CSV to SQLite
#
# Notes: 
#   Needed files: sentences.csv, jpn_indices.csv
#   Only indices used - should be trustworthy. 
#   For other examples of the word usage is better to use alc.co.jp or google.

import re,sys,time

SCRIPT_NAME     = 'script.tatoeba.sql'
VERSION         = 'build: ' +time.strftime("%Y-%m-%d")

FILE_INDICES    = 'jpn_indices.csv'   # indices: sentence_id [tab] meaning_id [tab] text 
FILE_SENTENCES  = 'sentences.csv'     # sentence_id [tab] translation_id 


def load_indices ():
  print("Loading INDICES from file '%s'" %FILE_INDICES)
  lines = [
    "DROP TABLE IF EXISTS indices;", 
    "CREATE TABLE indices (sid INT, mid INT, headword TEXT, good_example INT, reading TEXT, form TEXT, sense_no INT);"
  ]
  with open(FILE_INDICES) as f:
    for line in f:
      sid, mid, *elts = line.strip().split()
      # elements are separated by one space
      # order of elements: headword()[]{}~
      for e in elts:              
        try:
          # start of the element as it appears in JMdict
          m = re.match(r'[^([{]+',e)
          headword      = m.group(0)
          # ~
          good_example  = 0 
          if '~' in e:
            good_example = 1
            headword = headword.replace('~','')
          # in ()
          m = re.search(r'\((.+)\)',e)
          reading       = m.group(1) if m else None
          # number in [], if the word has multiple senses in JMdict
          m = re.search(r'\[([0-9]+)\]',e)
          sense_no      = int(m.group(1)) if m else 0
          # in {} 
          m = re.search(r'\{(.+)\}',e)
          form          = m.group(1) if m else None    
          # | followed by a digit can be ignored
          headword = re.sub( r'\|.+', '', headword )
          lines.append( 
            "INSERT INTO indices "\
              "(sid,mid,headword,good_example,reading,form,sense_no) VALUES "\
              "(%d,%d,'%s',%d, %s, %s,%d);\n" 
            %( int(sid), int(mid), headword, good_example, 
              "'%s'" %reading if reading else 'NULL', 
              "'%s'" %form if form else 'NULL', 
              sense_no 
            )
          )
        except AttributeError:
          print(sys.exc_info())
          print(sid,mid,elts)
          print("Probably misplaced space in source.")
          sys.exit()
  print("%d indices loaded" %len(lines))
  return ''.join(lines)


def load_sentences ():
  print("Loading SENTENCES from file '%s'" %FILE_SENTENCES)
  n_eng = 0
  n_jpn = 0
  lines = [
    "DROP TABLE IF EXISTS sentences;",
    "CREATE TABLE sentences ( id INT, lang TEXT, text TEXT );"
  ]
  with  open ( FILE_SENTENCES ) as f:
    for line in f:
      id, lang, *text = line.strip().split()
      if lang  == 'eng':
        n_eng += 1
      elif lang == 'jpn':
        n_jpn += 1
      else:
        continue
      lines.append("INSERT INTO sentences (id,lang,text) VALUES (%d,'%s','%s');\n"
        %(int(id),lang,' '.join(text).replace("'","''"))   )
  print("%d sentences loaded. %d eng, %d jpn" %(len(lines), n_eng, n_jpn))
  return ''.join(lines)


if __name__ == '__main__':

  res = input("Tatoeba version [%s]: " %VERSION)
  if res:
    VERSION = res

  with open( SCRIPT_NAME, 'w' ) as f:
    f.write( "BEGIN TRANSACTION;\n" );
    f.write( "REPLACE INTO aoi (key,val) VALUES('tatoeba_version','%s');\n" %VERSION)
    f.write( load_indices() )
    f.write( load_sentences() )
    f.write( "END TRANSACTION;\n" );
