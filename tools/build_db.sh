#!/bin/sh

DB_VERSION="0"

DIR_WRK="./wrk"

FILE_AOI="../aoi"
FILE_OUT="dbscript.sql"
FILE_SENTENCES="../data/sentences.csv.xz"
FILE_INDICES="../data/jpn_indices.csv.xz"
FILE_JMDICT="../data/JMdict_e.gz"
FILE_KANJIDIC="../data/kanjidic2.xml.gz"
FILE_KRAD="../data/kradfile-u.gz"

# colors
WHITE="\033[1;37m";
BLUE_BG="\033[0;44m";
END_COL="\033[m"

# colored message
msg ()
{
  echo -e "$WHITE $BLUE_BG $1 $END_COL"
}

# $1 - file, $2 - tgt dir
copy_and_extract ()
{
  cp -v "$1" "$2"
  pushd "$2"
  f=$(basename "$1")
  case "$f" in
    *.xz)
      unxz "$f" ;;
    *.gz)
      gunzip "$f" ;;
  esac
  popd
}

mkdir "$DIR_WRK"

msg "Copying and extracting files"
copy_and_extract "$FILE_SENTENCES" "$DIR_WRK/"
copy_and_extract "$FILE_INDICES" "$DIR_WRK/"
copy_and_extract "$FILE_JMDICT" "$DIR_WRK/"
copy_and_extract "$FILE_KRAD" "$DIR_WRK/"
copy_and_extract "$FILE_KANJIDIC" "$DIR_WRK/"
copy_and_extract "$FILE_AOI" "$DIR_WRK/"

pushd "$DIR_WRK"


msg "Parsing files"
./aoi -parse jmdict
./aoi -parse kanjidic
echo | ../parse_tatoeba.py
echo | ../parse_kradfile.py


# jmdict must be first (create stable aoi)
# componnets must be after kanjidic (updates table k_kanji)
cat script.jmdict.sql \
    script.kanjidic.sql \
    script.components.sql \
    script.tatoeba.sql  \
  > "$FILE_OUT"

echo "REPLACE INTO aoi (key,val) VALUES('db_version','$DB_VERSION');">> "$FILE_OUT"
echo "REPLACE INTO aoi (key,val) VALUES('db_created','$(date +%Y-%m-%d)');">> "$FILE_OUT"


msg "File '$FILE_OUT' prepared."

popd

mv "$DIR_WRK/$FILE_OUT" .
