#!/usr/bin/env python3

# Script for converting kradfile-u to SQL script.
# Uses custom list of strokes count (FILE_STROKES).

# Check for components withou strokes (aoi will treat them as with strokes=0)
#   select * from components where ifnull(strokes ,0)=0

import time

FILE_KRAD     = 'kradfile-u'
FILE_STROKES  = 'components.strokes'
SCRIPT_NAME   = 'script.components.sql'
VERSION       = 'build: ' + time.strftime("%Y-%m-%d")

data = {}           # kanji:components
set_comps = set()   # all used components
components = {}     # component:strokes


def load_kradfile():
  with open( FILE_KRAD ) as f:
    for line in f:
      line = line.strip()
      if ( line[0] == '#' ):
        continue
      line = line.replace( ' ', '' )
      kanji, comps = line.split(':')
      for c in comps:
        set_comps.add(c)
      data[kanji] = comps
  print("%s kanji, %s radicals" %(len(data),len(set_comps)));


def load_strokes_file():
  with open( FILE_STROKES ) as f:
    for line in f:
      line = line.strip()
      component, strokes = line.split(':')
      components[component] = strokes
  print("%s components" %len(components))


if __name__ == '__main__':

  res = input("Kradfile-u version [%s]: " %VERSION)
  if res:
    VERSION = res

  load_kradfile()
#  load_strokes_file()

  with open( SCRIPT_NAME, 'w' ) as f:
    f.write( "begin transaction;\n" )
    f.write("drop table if exists components;\n")
    f.write("create table components ( component TEXT, strokes INT );\n")
    f.write( "REPLACE INTO aoi (key,val) VALUES('kradfile_version','%s');\n" %VERSION)
    for c in set_comps:
      f.write("insert into components (component,strokes) values ('%s',NULL);\n" %(c))
    for d in data:
      f.write("update k_kanji set components='%s' where kanji='%s';\n" %(data[d],d));
    f.write( "end transaction;\n" ) 
    # add stroke count
    f.write( "begin transaction;\n" )
    f.write( "update components set strokes=(select strokes from k_kanji where kanji=component);\n" )
    # manual fix for kradfile-u
    f.write("update components set strokes=4 where component='⺹';\n")
    f.write("update components set strokes=3 where component='⺾';\n")
    f.write("update components set strokes=3 where component='⻖';\n")
    f.write("update components set strokes=2 where component='マ';\n")
    f.write("update components set strokes=1 where component='ノ';\n")
    f.write("update components set strokes=2 where component='ユ';\n")
    f.write("update components set strokes=2 where component='⺅';\n")
    f.write("update components set strokes=1 where component='｜';\n")
    f.write("update components set strokes=3 where component='⺌';\n")
    f.write("update components set strokes=3 where component='⻏';\n")
    f.write("update components set strokes=3 where component='ヨ';\n")
    f.write("update components set strokes=2 where component='ハ';\n")
    f.write( "end transaction;\n" ) 
